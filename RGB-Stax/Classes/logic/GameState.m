//
//  GameState.m
//  RGB-Stax
//
//  Created by Allan De Leon on 6/13/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "GameState.h"
#import "GameKitHelper.h"
#import "Constants.h"

@implementation GameState {
    Grid *gameGrid;
    BOOL continueSaved;
}

// -----------------------------------------------------------------------

#pragma mark - Constructor/Initialization

+ (GameState *) sharedState {
    static dispatch_once_t once;
    static GameState *sharedState;
    dispatch_once(&once, ^{sharedState = [[self alloc] init]; });
    return sharedState;
}

- (id) init {
    
    if (!(self = [super init])) {
        return nil;
    }
    
    gameGrid = nil;
    continueSaved = NO;
    
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"introreset"];
    
    return self;
}

- (void) setGrid:(Grid *)grid {
    gameGrid = grid;
}

- (void) saveGameState {
    
    DLog(@"Will try to save game state!");
    
    if (gameGrid) {
        [gameGrid saveGame];
    }
    
}

- (NSUserDefaults *) getUserDefaults {
    return [NSUserDefaults standardUserDefaults];
}

- (void) addHighScore:(unsigned)highscore {
    
    if (highscore > [self highScore]) {
        NSUserDefaults *defaults = [self getUserDefaults];
        [defaults setInteger:highscore forKey:@"HIGH-SCORE"];
        [[GameKitHelper sharedGameKitHelper] submitScore:(int64_t)highscore category:GC_LEADERBOARD_ID];
    }
}

- (unsigned) highScore {
    
    NSUserDefaults *defaults = [self getUserDefaults];
    NSInteger *score = [defaults integerForKey:@"HIGH-SCORE"];
    if (score) {
        return score;
    }
    
    return 0;
}

- (BOOL) getBooleanFor:(NSString *)name {
    return [self getBooleanFor:name withDefault:NO];
}

- (BOOL) getBooleanFor:(NSString *)name withDefault:(BOOL)def {
    
    id ret = [[self getUserDefaults] objectForKey:name];
    if (ret != nil) {
        return [ret boolValue];
    }
    
    return def;
}

- (void) setBoolean:(BOOL)value for:(NSString *)name {
    [[self getUserDefaults] setBool:value forKey:name];
}

- (void) removeSavedGame {
    [[self getUserDefaults] setObject:nil  forKey:@"gameboard"];
    [[self getUserDefaults] setObject:nil  forKey:@"arsenal"];
    [[self getUserDefaults] setInteger:nil forKey:@"currentScore"];
    [[self getUserDefaults] setInteger:nil forKey:@"timerThreshold"];
    [[self getUserDefaults] setFloat:0 forKey:@"timeLimit"];
}

- (BOOL) savedGame {
    return [[self getUserDefaults] objectForKey:@"gameboard"] != nil;
}

- (void) setContinueSavedGame:(BOOL)cont {
    
    continueSaved = cont;
    if (!continueSaved) {
        [self removeSavedGame];
    }
}

- (BOOL) continueSaved {
    return continueSaved;
}
// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
    DLog(@"Dealloc'ed GameState");
    gameGrid = nil;
}

@end
