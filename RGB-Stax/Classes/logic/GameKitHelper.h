//
//  GameKitHelper.h
//  RGB-Stax
//
//  Created by Allan De Leon on 8/27/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import <GameKit/GameKit.h>

#import "cocos2d.h"
#import "cocos2d-ui.h"

@protocol GameKitHelperProtocol<NSObject>
- (void) onScoresSubmitted:(bool)success;
@end

@interface GameKitHelper : NSObject

@property (nonatomic, assign) id<GameKitHelperProtocol> delegate;
@property (nonatomic, readonly) NSError *lastError;

+ (id) sharedGameKitHelper;
- (void) authenticateLocalPlayer;
- (void) submitScore:(int64_t)score category:(NSString*)category;
- (BOOL) gameCenterEnabled;

@end
