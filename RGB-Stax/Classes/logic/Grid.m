//
//  Grid.m
//  RGB-Stax
//
//  Created by Allan De Leon on 5/21/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "GridDetail.h"
#import "Grid.h"
#import "Sounds.h"
#import "GameState.h"
#import "Bomb.h"

extern int COLOR_BOMB_THRESHHOLD;

@implementation Grid {
    
    NSArray        *columnReferences;
    NSMutableSet   *removes;
    NSMutableArray *gameboard;
    NSMutableArray *grid;
    
    int removeChildCount;
    int removeActionCounter;
    int scoreMultiplier;
    
    BOOL segmentDropped;
}

// -----------------------------------------------------------------------

#pragma mark - Saving/restoring state

- (void) saveGame {
    
    DLog(@"Saving game state!");
    
    GameState *state = [GameState sharedState];
    NSUserDefaults *defaults = [state getUserDefaults];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:gameboard];
    
    if (state && defaults && data) {
        [defaults setObject:data forKey:@"gameboard"];
        [self.scene.arsenal saveGameState];
        [self.scene saveGameState];
    }
}

- (void) restoreGameState {
    
    DLog(@"Will try to restore game state!");
    
    GameState      *state    = [GameState sharedState];
    NSUserDefaults *defaults = [state getUserDefaults];
    NSData         *data     = [defaults objectForKey:@"gameboard"];
    NSArray        *board    = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    
    if (state && defaults && data && board) {
        for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
            for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
                Segment *seg = [[board objectAtIndex:row] objectAtIndex:col];
                if (seg.value > -1) {
                    Segment *s = [self segmentAtRow:row column:col];
                    if (s) {
                        CCSprite *sprite = [self.scene createSpriteWithValue:seg.value];
                        sprite.position = s.position;
                        s.value = seg.value;
                        s.bombType = seg.bombType;
                        s.sprite = sprite;
                        [self.scene addChild:sprite z:eGamePieceLayer];
                        if (seg.bombType > -1) {
                            [self restoreBombInSegment:s ofType:seg.bombType];
                        }
                    }
                }
            }
        }
        [self.scene.arsenal restoreGameState];
        [self.scene restoreGameState];
    }
}

- (void) restoreBombInSegment:(Segment *)segment ofType:(int)type {
    CCSprite *bomb = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:[Bomb bombImage:type]]];
    bomb.position = ccp(segment.sprite.contentSize.width / 2, segment.sprite.contentSize.height / 2);
    segment.bomb = bomb;
    [segment.sprite addChild:bomb z:eReadyBombLayer];
    [self.scene.arsenal incrementBombCountForType:type];
}

// -----------------------------------------------------------------------

#pragma mark - Constructor/Initialization

- (id) initForScene:(GameBoardScene *) scene {
    
    if (!(self = [super init])) {
        return nil;
    }
    
    self->segmentDropped = NO;
    self->scoreMultiplier = 1;
    self.scene = scene;
    removes = [[NSMutableSet alloc] init];
    gameboard = [[NSMutableArray alloc] init];
    
    [self initBoardCoordinates];
    [self drawGrid];
    
    if ([[GameState sharedState] continueSaved]) {
        [self restoreGameState];
    }
    
    return self;
}

// -----------------------------------------------------------------------

#pragma mark - Grid layout logic

- (void) initBoardCoordinates {
    
    float midx = self.scene.contentSize.width / 2.0;
    float startx = (midx + (self.scene.SEGMENT_PIXEL_WIDTH / 2)) - (self.scene.SEGMENT_PIXEL_WIDTH * (self.scene.GAMEBOARD_COLUMNS / 2.0));
    
    for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
        NSMutableArray *column = [[NSMutableArray alloc] init];
        float x = startx;
        for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
            [column addObject:[[Segment alloc] initWithPosition:ccp(x, self.scene.GAMEBOARD_TOP - (self.scene.SEGMENT_PIXEL_HEIGHT * row)) value:-1 row:row+1 column:col+1]];
            x += self.scene.SEGMENT_PIXEL_WIDTH;
        }
        [gameboard addObject:column];
    }
    
    columnReferences = [gameboard objectAtIndex:0];
    [self setLastSegmentYPos:((Segment *)[[gameboard objectAtIndex:self.scene.GAMEBOARD_ROWS - 1] objectAtIndex:0]).position.y];
}

// -----------------------------------------------------------------------

#pragma mark - Segment location logic

- (Segment *) segmentAtRow:(int)row column:(int)col {
    
    if (row < 0 || row > self.scene.GAMEBOARD_ROWS || col < 0 || col > self.scene.GAMEBOARD_COLUMNS) {
        return nil;
    }
    
    return [[gameboard objectAtIndex:row] objectAtIndex:col];
}

- (Segment *) segmentAtPosition:(CGPoint)pos {
    
    for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
        for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
            Segment *seg = [[gameboard objectAtIndex:row] objectAtIndex:col];
            if (seg.position.x == pos.x && seg.position.y == pos.y) {
                return seg;
            }
        }
    }
    
    return nil;
}

- (int) columnForPosition:(CGPoint)pos {
    
    float halfSize = self.scene.SEGMENT_PIXEL_WIDTH / 2.0;
    for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
        Segment *seg = [columnReferences objectAtIndex:col];
        if (seg.position.x - halfSize < pos.x && seg.position.x + halfSize > pos.x) {
            return col;
        }
    }
    
    return -1;
}

- (int) rowForPosition:(CGPoint)pos {
    
    float halfSize = self.scene.SEGMENT_PIXEL_HEIGHT / 2.0;
    for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
        Segment *seg = [self segmentAtRow:row column:0];
        if (seg.position.y - halfSize < pos.y && seg.position.y + halfSize > pos.y) {
            return row;
        }
    }
    
    return -1;
}

- (Segment *) highestOpenTopColumn:(int)col {
    
    Segment *top;
    for (int row = self.scene.GAMEBOARD_ROWS - 1; row >= 0; --row) {
        Segment *seg = nil;
        if((seg = [self segmentAtRow:row column:col]) && seg.value == -1) {
            top = seg;
        } else {
            break;
        }
    }
    
    return top;
}

// -----------------------------------------------------------------------

#pragma mark - Drop location logic

- (float) dropXPos:(CGPoint)pos {
    
    float halfSize = self.scene.SEGMENT_PIXEL_WIDTH / 2.0;
    for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
        Segment *seg = [columnReferences objectAtIndex:col];
        if (fabs(seg.position.x - pos.x) < halfSize) {
            return seg.position.x;
        }
    }
    
    return 0.0;
}

- (float) topSegmentTopForBlock:(Block *)block {
    
    switch (block.orientation) {
        case LEFT2RIGHT:
        case RIGHT2LEFT:
            return [self min:
                    [self highestOpenTopColumn:[self columnForPosition:block.segment1.sprite.position]].position.y,
                    [self highestOpenTopColumn:[self columnForPosition:block.segment2.sprite.position]].position.y,
                    [self highestOpenTopColumn:[self columnForPosition:block.segment3.sprite.position]].position.y,
                    FLT_MIN
                    ];
        case TOP2BOTTOM:
        case BOTTOM2TOP:
            return [self highestOpenTopColumn:[self columnForPosition:block.segment2.sprite.position]].position.y;
        default:
            return -1.0;
    }
}

// -----------------------------------------------------------------------

#pragma mark - Grid drawing logic

- (void) drawGrid {
    
    if (self->grid == nil) {
        self->grid = [[NSMutableArray alloc] init];
        for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
            for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
                GridDetail *detail = [[GridDetail alloc] initWithPosition:((Segment *)[self segmentAtRow:row column:col]).position row:row column:col];
                [self.scene addChild:detail.border z:eBackgroundLayer];
                [self->grid addObject:detail];
            }
        }
    }
    
    [self resetGrid];
}

// -----------------------------------------------------------------------

#pragma mark - Game over logic

- (void) gameOverAnimation:(Block *)block {
    
    [Sounds gameOver];
    
    if (block) {
        [block.segment1.sprite runAction:[CCActionFadeOut actionWithDuration:2]];
        [block.segment2.sprite runAction:[CCActionFadeOut actionWithDuration:2]];
        [block.segment3.sprite runAction:[CCActionFadeOut actionWithDuration:2]];
    }
    
    NSMutableArray *dropSegments = [[NSMutableArray alloc] init];
    if (block.orientation == BOTTOM2TOP || block.orientation == TOP2BOTTOM) {
        [self addDropSegmentColumn:[self columnForPosition:block.segment2.position] toArray:&dropSegments];
    } else {
        int col1 = [self columnForPosition:block.segment1.position];
        if (((Segment *)[[gameboard objectAtIndex:self.scene.GAMEBOARD_ROWS -1] objectAtIndex:col1]).value > -1) {
            [self addDropSegmentColumn:col1 toArray:&dropSegments];
        }
        int col2 = [self columnForPosition:block.segment2.position];
        if (((Segment *)[[gameboard objectAtIndex:self.scene.GAMEBOARD_ROWS -1] objectAtIndex:col2]).value > -1) {
            [self addDropSegmentColumn:col2 toArray:&dropSegments];
        }
        int col3 = [self columnForPosition:block.segment3.position];
        if (((Segment *)[[gameboard objectAtIndex:self.scene.GAMEBOARD_ROWS -1] objectAtIndex:col3]).value > -1) {
            [self addDropSegmentColumn:col3 toArray:&dropSegments];
        }
    }
    
    [self doDropSegments:dropSegments];
}

- (void) addDropSegmentColumn:(int)col toArray:(NSMutableArray **)array {
    
    for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
        Segment *seg = [[gameboard objectAtIndex:row] objectAtIndex:col];
        if (seg.value > -1) {
            [*array addObject:seg];
        }
        GridDetail *detail = [grid objectAtIndex:row * self.scene.GAMEBOARD_COLUMNS + col];
        [self destroyAnimationForSprite:detail.border runBreakUp:NO];
    }
}

- (void) doDropSegments:(NSArray *)array {
    
    int dropCount = (unsigned) [array count];
    
    if (dropCount <= 0) {
        [self breakUp];
        return;
    }
    
    int count = 0;
    for (Segment *seg in array) {
        if (seg.value > -1) {
            [self destroyAnimationForSprite:seg.sprite runBreakUp:++count == dropCount];
        }
    }
}

- (void) destroyAnimationForSprite:(CCSprite *)sprite runBreakUp:(BOOL)breakup {
    CCActionDelay   *delay1  = [CCActionDelay actionWithDuration:.08];
    CCActionScaleTo *grow    = [CCActionScaleTo actionWithDuration:.05 scale:1.2];
    CCActionScaleTo *gback   = [CCActionScaleTo actionWithDuration:.15 scale:0];
    CCActionScaleTo *shrink  = [CCActionScaleTo actionWithDuration:2.2 scale:.3];
    CCActionCallBlock *gray  = [CCActionCallBlock actionWithBlock:^{ sprite.color = breakup ? [CCColor grayColor] : sprite.color; }];
    CCAction *action = breakup ? [CCActionCallFunc actionWithTarget:self selector:@selector(breakUp)] : nil;
    [sprite runAction:[CCActionSequence actions:gray, shrink, grow, delay1, gback, action, nil]];
}

- (void) breakUp {
    
    removeActionCounter = 0;
    CCActionCallFunc *func = [CCActionCallFunc actionWithTarget:self selector:@selector(removeActiveSegments)];
    for (int row = self.scene.GAMEBOARD_ROWS - 1; row >= 0; --row) {
        for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
            Segment *seg = [[gameboard objectAtIndex:row] objectAtIndex:col];
            if (seg.value > -1 && seg.sprite) {
                ++removeActionCounter;
                [seg.sprite runAction:[CCActionSequence actions:[CCActionScaleTo actionWithDuration:.5 scaleX:1 scaleY:.01], func, nil]];
                if (seg.bomb != nil) {
                    [seg.sprite removeChild:seg.bomb];
                    seg.bombType = NO_BOMB;
                    seg.bomb = nil;
                }
            }
            seg.value = -1;
        }
    }
}

- (void)removeActiveSegments {
    
    if (--removeActionCounter > 0) {
        return;
    }
    
    for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
        for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
            Segment *seg = [[gameboard objectAtIndex:row] objectAtIndex:col];
            if (seg.sprite) {
                [self.scene removeChild:seg.sprite cleanup:YES];
                seg.sprite = nil;
                seg.value = -1;
            }
        }
    }
    
    [self.scene postGameOverAnimation];
    for (GridDetail *detail in [self->grid objectEnumerator]) {
        [detail.border runAction:[CCActionFadeOut actionWithDuration:.2]];
    }
}

- (void) removeSprite:(CCSprite *)sprite {
    [self.scene removeChild:sprite];
}

// -----------------------------------------------------------------------

#pragma mark - Reset logic

- (void) resetGrid {
    
    for (GridDetail *detail in [self->grid objectEnumerator]) {
        detail.border.opacity = 1;
        detail.border.scale   = 1;
    }
    
    DLog(@"Scene child count: %d", (unsigned)[[self.scene children] count]);
}

// -----------------------------------------------------------------------

#pragma mark - Game piece fade logic

- (void) fadeTo:(CGFloat)fade {
    
    for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
        for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
            Segment *seg = [[gameboard objectAtIndex:row] objectAtIndex:col];
            if (seg.value > -1 && seg.sprite) {
                seg.sprite.opacity = fade;
            }
            if (seg.bomb) {
                seg.bomb.opacity = fade / (fade < 1.0 ? 4.0 : 1.0);
            }
        }
    }
}

// -----------------------------------------------------------------------

#pragma mark - Create bomb logic

- (void) createBombsRed:(int)red green:(int)green blue:(int)blue from:(CGPoint)start {
    
    if (red >= COLOR_BOMB_THRESHHOLD) {
        int type = [self.scene.arsenal addToArsenal:RED_BOMB immediate:YES];
        [Bomb flashFrom:start inScene:self.scene type:type cleanup:YES bonusMultiplier:2];
    }
    
    if (green >= COLOR_BOMB_THRESHHOLD) {
        int type = [self.scene.arsenal addToArsenal:GREEN_BOMB immediate:YES];
        [Bomb flashFrom:start inScene:self.scene type:type cleanup:YES bonusMultiplier:2];
    }
    
    if (blue >= COLOR_BOMB_THRESHHOLD) {
        int type = [self.scene.arsenal addToArsenal:BLUE_BOMB immediate:YES];
        [Bomb flashFrom:start inScene:self.scene type:type cleanup:YES bonusMultiplier:2];
    }
}

// -----------------------------------------------------------------------

#pragma mark - Bomb explosion logic

- (void) handleBomb:(Bomb *)bomb {
    
    [removes removeAllObjects];
    [bomb addSegmentsFromGrid:self toExplosionPattern:removes];
    
    self->removeChildCount = (int) [removes count];
    if (self->removeChildCount > 0) {
        [self.scene setSkipNextBlock];
    } else {
        scoreMultiplier = 1;
        [self.scene unPauseForArsenal];
        return;
    }
    
    CCActionCallFunc *func = [CCActionCallFunc actionWithTarget:self selector:@selector(removeChildren)];
    for (Segment *seg in removes) {
        [seg.sprite runAction:[CCActionSequence actions:[CCActionRotateBy actionWithDuration:.1 angle:180.0], [CCActionScaleTo actionWithDuration:.2 scale:0], func, nil]];
        seg.value = -1;
    }
    
    if (self->removeChildCount > 0) {
        DLog(@"Multiplier: %d", scoreMultiplier);
        [self.scene addScore:(scoreMultiplier * 10 * self->removeChildCount)];
        [Sounds pop];
        return;
    } else {
        scoreMultiplier = 1;
    }
}

// -----------------------------------------------------------------------

#pragma mark - Elimination logic

- (BOOL) removeSegmentsFromGrid {
    segmentDropped = NO;
    return [self removeSegments];
}

- (void) flashMessage {
    
    switch (scoreMultiplier) {
        case 3:
            [self.scene flashMessage:@"x2 Combo!"];
            break;
        case 4:
            [self.scene flashMessage:@"x3 Combo!"];
            break;
        case 5:
            [self.scene flashMessage:@"x4 Combo!"];
            break;
        case 6:
            [self.scene flashMessage:@"x5 Combo!"];
            break;
        case 7:
            [self.scene flashMessage:@"x6 Combo!"];
            break;
        default:
            break;
    }
    
    if (scoreMultiplier > 7) {
        [self.scene flashMessage:@"Amazing!"];
    }
}

- (BOOL) removeSegments {
    
    [removes removeAllObjects];
    
    [self findRemovableBlackSegments];
    [self findRemovableSquareSegments];
    [self findRemovableHorizontalSegments];
    [self findRemovableVerticalSegments];
    
    int redRemoved   = 0;
    int greenRemoved = 0;
    int blueRemoved  = 0;
    
    CCActionCallFunc *func = [CCActionCallFunc actionWithTarget:self selector:@selector(removeChildren)];
    
    self->removeChildCount = (int) [removes count];
    for (Segment *seg in removes) {
        switch (seg.value) {
            case 0:
                ++redRemoved;
                break;
            case 1:
                ++greenRemoved;
                break;
            case 2:
                ++blueRemoved;
                break;
            default:
                break;
        }
        [seg.sprite runAction:[CCActionSequence actions:[CCActionRotateBy actionWithDuration:.1 angle:180.0], [CCActionScaleTo actionWithDuration:.2 scale:0], func, nil]];
        seg.value = -1;
    }
    
    if (self->removeChildCount > 0) {
        [self.scene addScore:(scoreMultiplier * 10 * self->removeChildCount)];
        [Sounds pop];
        [self createBombsRed:redRemoved green:greenRemoved blue:blueRemoved from:ccp(self.scene.contentSize.width / 2, (self.scene.contentSize.height / 4) * 3)];
        return YES;
    } else {
        if (scoreMultiplier > 3) {
            [self.scene.arsenal createBomb:STAR random:YES];
        }
        if (segmentDropped) {
            SuppressPerformSelectorLeakWarning([self.scene performSelector:@selector(nextBlock) withObject:self.scene]);
            segmentDropped = NO;
        }
        [self flashMessage];
        scoreMultiplier = 1;
        [self.scene createBomb];
    }
    
    return NO;
}

- (void) removeChildren {
    
    /**
     * This is only ever called if something was removed...
     */
    
    @synchronized(self) {
        if (--self->removeChildCount == 0) {
            DLog(@"Before Children count: %d", (unsigned)[[self.scene children] count]);
            for (Segment *seg in removes) {
                if (seg.bombType > NO_BOMB && seg.bomb != nil) {
                    [self.scene.arsenal addToArsenal:seg.bombType immediate:NO];
                    [Bomb flashFrom:ccp(seg.sprite.position.x, seg.sprite.position.y) inScene:self.scene type:seg.bombType cleanup:YES];
                    [seg.sprite removeChild:seg.bomb];
                    [seg setBombType:NO_BOMB];
                    [seg setBomb:nil];
                }
                if ([[seg.sprite.parent children] containsObject:seg.sprite]) {
                    [seg.sprite.parent removeChild:seg.sprite];
                }
                [seg setSprite:nil];
            }
            DLog(@"After Children count: %d", (unsigned)[[self.scene children] count]);
            [self dropAll];
        }
    }
}

// -----------------------------------------------------------------------

#pragma mark - Drop logic

- (void) delayedDrop {
    [self dropAll];
}

- (void) dropAll {
    
    bool gotDropped = NO;
    
    for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
        int openRow = -1;
        Segment *open = nil;
        for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
            Segment *s1 = [[gameboard objectAtIndex:row] objectAtIndex:col];
            if (s1.value == -1 && !open) {
                open = s1;
                openRow = row;
            } else if (s1.value > -1 && open) {
                open.sprite = s1.sprite;
                [open.sprite runAction:[CCActionMoveTo actionWithDuration:.08 position:open.position]];
                open.value = s1.value;
                if (s1.bombType > NO_BOMB) {
                    open.bombType = s1.bombType;
                    open.bomb = s1.bomb;
                }
                s1.value = -1;
                s1.sprite = nil;
                s1.bombType = NO_BOMB;
                s1.bomb = nil;
                gotDropped = YES;
                if (openRow + 1 < self.scene.GAMEBOARD_ROWS) {
                    open = [[gameboard objectAtIndex:++openRow] objectAtIndex:col];
                }
            }
        }
    }
    
    if (gotDropped) {
        ++scoreMultiplier;
        segmentDropped = segmentDropped || gotDropped;
        [self removeSegments];
    } else {
        SuppressPerformSelectorLeakWarning([self.scene performSelector:@selector(nextBlock) withObject:self.scene]);
    }
    
    // [self dump];
}

// -----------------------------------------------------------------------

#pragma mark - Removeable search logic

- (void) findRemovableBlackSegments {
    
    for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
        Segment *s = [[gameboard objectAtIndexedSubscript:0] objectAtIndexedSubscript:col];
        if (s.value == 3) {
            [removes addObject:s];
        }
    }
}

- (void) findRemovableHorizontalSegments {
    
    for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
        int start = 0;
        for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
            Segment *s1 = [[gameboard objectAtIndex:row] objectAtIndex:col];
            Segment *s2 = col + 1 < self.scene.GAMEBOARD_COLUMNS ? [[gameboard objectAtIndex:row] objectAtIndex:col + 1] : nil;
            if (s2 && s1.value > -1 && s1.value == s2.value) {
                continue;
            } else if (col - start >= 3) {
                for (int r = start; r <= col; ++r) {
                    Segment *remove = [[gameboard objectAtIndex:row] objectAtIndex:r];
                    [removes addObject:remove];
                }
            }
            start = col + 1;
        }
    }
}

- (void) findRemovableVerticalSegments {
    
    for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
        int start = 0;
        for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
            Segment *s1 = [[gameboard objectAtIndex:row] objectAtIndex:col];
            Segment *s2 = row + 1 < self.scene.GAMEBOARD_ROWS ? [[gameboard objectAtIndex:row + 1] objectAtIndex:col] : nil;
            if (s2 && s1.value > -1 && s1.value == s2.value) {
                continue;
            } else if (row - start >= 3) {
                for (int r = start; r <= row; ++r) {
                    Segment *remove = [[gameboard objectAtIndex:r] objectAtIndex:col];
                    [removes addObject:remove];
                }
            }
            start = row + 1;
        }
    }
}

- (void) findRemovableSquareSegments {
    
    for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
        for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
            Segment *s1 = [[gameboard objectAtIndex:row] objectAtIndex:col];
            if (row + 1 < self.scene.GAMEBOARD_ROWS && col + 1 < self.scene.GAMEBOARD_COLUMNS && s1.value > -1 && ![removes containsObject:s1]) {
                Segment *s2 = [[gameboard objectAtIndex:row] objectAtIndex:col + 1];
                Segment *s3 = [[gameboard objectAtIndex:row + 1] objectAtIndex:col];
                Segment *s4 = [[gameboard objectAtIndex:row + 1] objectAtIndex:col + 1];
                if (s1.value == s2.value && s1.value == s3.value && s1.value == s4.value && ![removes containsObject:s2] && ![removes containsObject:s3] && ![removes containsObject:s4]) {
                    [removes addObject:s1];
                    [removes addObject:s2];
                    [removes addObject:s3];
                    [removes addObject:s4];
                    BOOL b1 = NO, b2 = NO;
                    if (row + 2 < self.scene.GAMEBOARD_ROWS) {
                        Segment *s5 = [[gameboard objectAtIndex:row + 2] objectAtIndex:col];
                        Segment *s6 = [[gameboard objectAtIndex:row + 2] objectAtIndex:col + 1];
                        if (s1.value == s5.value && s1.value == s6.value && ![removes containsObject:s5] && ![removes containsObject:s6]) {
                            [removes addObject:s5];
                            [removes addObject:s6];
                            b1 = YES;
                        }
                    }
                    if (col + 2 < self.scene.GAMEBOARD_COLUMNS) {
                        Segment *s7 = [[gameboard objectAtIndex:row] objectAtIndex:col + 2];
                        Segment *s8 = [[gameboard objectAtIndex:row + 1] objectAtIndex:col + 2];
                        if (s1.value == s7.value && s1.value == s8.value && ![removes containsObject:s7] && ![removes containsObject:s8]) {
                            [removes addObject:s7];
                            [removes addObject:s8];
                            b2 = YES;
                        }
                    }
                    if (b1 && b2 && row + 2 < self.scene.GAMEBOARD_ROWS && col + 2 < self.scene.GAMEBOARD_COLUMNS) {
                        Segment *s9 = [[gameboard objectAtIndex:row + 2] objectAtIndex:col + 2];
                        if (s1.value == s9.value && ![removes containsObject:s9]) {
                            [removes addObject:s9];
                        }
                    }
                }
            }
        }
    }
}

- (NSArray *) gameBoard {
    return gameboard;
}

// -----------------------------------------------------------------------

#pragma mark - Warning level logic

- (int) warnLevel {
    
    int level = self.scene.GAMEBOARD_ROWS - 4;
    for (int row = self.scene.GAMEBOARD_ROWS - 1; row >= self.scene.GAMEBOARD_ROWS - 3; --row) {
        for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
            Segment *seg = [[gameboard objectAtIndex:row] objectAtIndex:col];
            if (seg.value > -1) {
                return row - level;
            }
        }
    }
    
    return 0;
}

- (BOOL) hasSegments {
    
    for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
        for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
            Segment *seg = [[gameboard objectAtIndex:row] objectAtIndex:col];
            if (seg.value > -1) {
                return YES;
            }
        }
    }
    
    return NO;
}

// -----------------------------------------------------------------------

#pragma mark - Debug dump

- (void) dump {
    
    for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
        for (int row = 0; row < self.scene.GAMEBOARD_ROWS; ++row) {
            Segment *seg = [self segmentAtRow:row column:col];
            if (seg.sprite) {
                DLog(@"Idx: (%02d,%02d) ; Value: %d ; Position: (%3.0f,%3.0f)", row, col, seg.value, seg.sprite.position.x, seg.sprite.position.y);
            }
        }
        DLog(@"--------------------");
    }
}

// -----------------------------------------------------------------------

#pragma mark - Util methods

- (double) min:(double)first, ... {
    
    va_list args;
    va_start(args, first);
    
    double arg, min = first;
    while ((arg = va_arg(args, double)) != FLT_MIN) {
        if (arg < min) {
            min = arg;
        }
    }
    va_end(args);
    
    return min;
}

// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
    DLog(@"Dealloc'ed Grid");
    self.scene = nil;
}

@end
