//
//  Sounds.m
//  RGB-Stax
//
//  Created by Allan De Leon on 7/21/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "Sounds.h"
#import "GameState.h"
#import "OALSimpleAudio.h"

bool MUSIC   = YES;
bool EFFECTS = YES;
bool BG_MUSIC_PLAYING = NO;

@implementation Sounds

+ (void) setMusic:(bool)music effects:(bool)effects {
    MUSIC   = music;
    EFFECTS = effects;
}

+ (void) updateSoundOptions {
    [Sounds setMusic:[[GameState sharedState] getBooleanFor:@"music" withDefault:YES] effects:[[GameState sharedState] getBooleanFor:@"sound-effect" withDefault:YES]];
}

+ (void) introMusic {
    if (!MUSIC || BG_MUSIC_PLAYING) {
        return;
    }
    BG_MUSIC_PLAYING = YES;
    [[OALSimpleAudio sharedInstance] playBg:@"intro-music.caf" volume:.5 pan:1 loop:YES]; // TODO: Note, the following music can't be used commercially -- replace it!
}

+ (void) gameMusic {
    
    DLog(@"Music is: %d BG is %d", MUSIC, BG_MUSIC_PLAYING);
    if (!MUSIC || BG_MUSIC_PLAYING) {
        return;
    }
    BG_MUSIC_PLAYING = YES;
    [[OALSimpleAudio sharedInstance] playBg:@"game-music.caf" volume:.5 pan:1 loop:YES];
}

+ (void) stopBackgroundMusic {
    [[OALSimpleAudio sharedInstance] stopBg];
    BG_MUSIC_PLAYING = NO;
}

+ (void) startGame {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"start-game.caf" volume:.3 pitch:1 pan:1 loop:NO];
}

+ (void) quitGame {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"quit-game.caf" volume:.3 pitch:1 pan:1 loop:NO];
}

+ (void) gameOver {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"game-over.caf" volume:.3 pitch:1 pan:1 loop:NO];
}


+ (void) shotEffect {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"shoot.caf" volume:.2 pitch:1 pan:1 loop:NO];
}

+ (void) shotComplete {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"tick.caf" volume:.4 pitch:3 pan:1.2 loop:NO];
}

+ (void) pop {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"pop.caf" volume:.5 pitch:.6 pan:.1 loop:NO];
}

+ (void) ballBounce {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"ball-bounce.caf" volume:.5 pitch:.8 pan:.2 loop:NO];
}

+ (void) panelOpen {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"panel-open.caf" volume:.8 pitch:1 pan:1 loop:NO];
}

+ (void) panelClose {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"panel-close.caf" volume:.8 pitch:1 pan:1 loop:NO];
}

+ (void) pling {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"pling.caf" volume:.4 pitch:1 pan:1 loop:NO];
}

+ (void) explode {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"explode.caf" volume:.4 pitch:.5 pan:1 loop:NO];
}

+ (void) beep {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"beep.caf" volume:.2 pitch:1 pan:1 loop:NO];
}

+ (void) warn {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"warn.caf" volume:.2 pitch:1 pan:1 loop:NO];
}

+ (void) check {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"check.caf" volume:.2 pitch:1 pan:1 loop:NO];
}

+ (void) unCheck {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"check.caf" volume:.2 pitch:.8 pan:.5 loop:NO];
}

+ (void) resetShotClock {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"clockSlowed.caf" volume:.2 pitch:1 pan:.5 loop:NO];
}

+ (void) curser {
    if (!EFFECTS) {
        return;
    }
    [[OALSimpleAudio sharedInstance] playEffect:@"curser.caf" volume:.2 pitch:1.5 pan:1 loop:NO];
}

@end
