//
//  Segment.h
//  RGB-Stax
//
//  Created by Allan De Leon on 5/20/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

@interface Segment : NSObject <NSCoding>

@property (assign) int      row;
@property (assign) int      column;
@property (assign) int      value;
@property (assign) int      bombType;
@property (assign) CGPoint  position;
@property (assign) CCSprite *sprite;
@property (assign) CCSprite *bomb;

- (id) initWithPosition:(CGPoint)pos;
- (id) initWithPosition:(CGPoint)pos value:(int)value;
- (id) initWithPosition:(CGPoint)pos value:(int)value row:(int)row column:(int)column;

- (void) setValue:(int)value sprite:(CCSprite *)sprite;

@end
