//
//  Sounds.h
//  RGB-Stax
//
//  Created by Allan De Leon on 7/21/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import <Foundation/Foundation.h>

@interface Sounds : NSObject

+ (void) setMusic:(bool)music effects:(bool)effects;

+ (void) introMusic;
+ (void) gameMusic;
+ (void) stopBackgroundMusic;
+ (void) updateSoundOptions;

+ (void) startGame;
+ (void) quitGame;
+ (void) gameOver;
+ (void) pop;
+ (void) shotEffect;
+ (void) shotComplete;
+ (void) ballBounce;
+ (void) panelOpen;
+ (void) panelClose;
+ (void) pling;
+ (void) explode;
+ (void) beep;
+ (void) warn;
+ (void) check;
+ (void) unCheck;
+ (void) resetShotClock;
+ (void) curser;
    
@end
