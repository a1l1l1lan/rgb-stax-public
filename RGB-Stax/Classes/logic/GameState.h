//
//  GameState.h
//  RGB-Stax
//
//  Created by Allan De Leon on 6/13/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import <Foundation/Foundation.h>

#import "Grid.h"

@interface GameState : NSObject

+ (GameState *) sharedState;

- (NSUserDefaults *) getUserDefaults;

- (unsigned) highScore;
- (void) addHighScore:(unsigned)highscore;
- (void) setGrid:(Grid *)grid;
- (void) saveGameState;
- (void) removeSavedGame;
- (void) setContinueSavedGame:(BOOL)cont;
- (void) setBoolean:(BOOL)value for:(NSString *)name;

- (BOOL) savedGame;
- (BOOL) continueSaved;
- (BOOL) getBooleanFor:(NSString *)name;
- (BOOL) getBooleanFor:(NSString *)name withDefault:(BOOL)def;

@end
