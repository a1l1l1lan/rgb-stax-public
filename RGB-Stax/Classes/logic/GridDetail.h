//
//  GridDetail.h
//  RGB-Stax
//
//  Created by Allan De Leon on 7/9/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

@interface GridDetail : NSObject

@property (assign) int     row;
@property (assign) int     column;
@property (assign) BOOL    isWarning; // Set if column is nearly filled (e.g. has <= 2 open spots left)
@property (atomic, strong) CCSprite *border;

- (id) initWithPosition:(CGPoint)position row:(int)row column:(int)column;

@end
