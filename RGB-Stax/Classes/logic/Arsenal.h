//
//  Arsenal.h
//  RGB-Stax
//
//  Created by Allan De Leon on 6/20/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

#import "Bomb.h"
#import "../scenes/GameBoardScene.h"

@interface Arsenal : NSObject

@property (assign) BOOL active;
@property (strong) GameBoardScene *scene;

- (id) initWithScene:(GameBoardScene *)scene;

- (int) addToArsenal:(int)type immediate:(BOOL)immediate;
- (int) createBomb:(int)type random:(BOOL)random;

- (unsigned) bombCount;

- (void) clearArsenal;
- (void) showArsenal;
- (void) hideArsenal;
- (void) incrementBombCountForType:(int)type;
- (void) saveGameState;
- (void) restoreGameState;
- (void) pulseIndicator:(int)level;

- (Bomb *) activateBomb:(CGPoint)position;

@end
