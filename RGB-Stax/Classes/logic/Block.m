//
//  Block.m
//  RGB-Stax
//
//  Created by Allan De Leon on 5/20/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "Grid.h"
#import "Block.h"
#import "Sounds.h"

extern int PREP_TOP;

@implementation Block {
    
    int  actionCount;
    int  readyCount;
    int  shotCount;
    SEL  shotCallbackSelector;
    
    GameBoardScene *gameScene;
}

// -----------------------------------------------------------------------

#pragma mark - Constructor/Initialization

- (id) initWithScene:(GameBoardScene *)scene {
    
    if (!(self = [super init])) {
        return nil;
    }
    
    self.segment1 = [[Segment alloc] init];
    self.segment2 = [[Segment alloc] init];
    self.segment3 = [[Segment alloc] init];
    
    self.orientation = 1;
    self->gameScene = scene;
    
    return self;
}

// -----------------------------------------------------------------------

#pragma mark - Ready position logic

- (void) moveToReadyPosition {
    
    self->readyCount = 0;
    
    CCActionCallFunc *func = [CCActionCallFunc actionWithTarget:self selector:@selector(readyPositionCompleted)];
    
    float size = self->gameScene.SEGMENT_PIXEL_WIDTH;
    self.segment1.position = ccp(size * 4,PREP_TOP);
    self.segment2.position = ccp(size * 5,PREP_TOP);
    self.segment3.position = ccp(size * 6,PREP_TOP);
    
    CCActionMoveTo *action = [CCActionMoveTo actionWithDuration:.1 position:self.segment1.position];
    [self.segment1.sprite runAction:[CCActionSequence actions:action, [CCActionScaleTo actionWithDuration:.05 scale:1], func, nil]];
    
    action = [CCActionMoveTo actionWithDuration:.1 position:self.segment2.position];
    [self.segment2.sprite runAction:[CCActionSequence actions:action, [CCActionScaleTo actionWithDuration:.05 scale:1], func, nil]];
    
    action = [CCActionMoveTo actionWithDuration:.1 position:self.segment3.position];
    [self.segment3.sprite runAction:[CCActionSequence actions:action, [CCActionScaleTo actionWithDuration:.05 scale:1], func, nil]];
}

- (void) readyPositionCompleted {
    if (++readyCount == 3) {
        [self->gameScene unPauseTouch];
    }
}

// -----------------------------------------------------------------------

#pragma mark - Move logic

- (void) moveTo:(CGPoint)pos {

    float halfSize = self->gameScene.SEGMENT_PIXEL_WIDTH / 2.0;
    if ((self.orientation == 1 || self.orientation == 3) && (pos.x <= (halfSize * 3) || pos.x >= (halfSize * (self->gameScene.GAMEBOARD_COLUMNS * 2 - 1)))) {
        return;
    } else if ((self.orientation == 2 || self.orientation == 4) && (pos.x <= halfSize || pos.x >= (halfSize * (self->gameScene.GAMEBOARD_COLUMNS * 2 + 1)))) {
        return;
    }
    
    CGPoint pos1;
    CGPoint pos2;
    switch (self.orientation) {
        case LEFT2RIGHT:
            pos1 = ccp(pos.x - self->gameScene.SEGMENT_PIXEL_WIDTH, self.segment1.sprite.position.y);
            pos2 = ccp(pos.x + self->gameScene.SEGMENT_PIXEL_WIDTH, self.segment1.sprite.position.y);
            break;
        case TOP2BOTTOM:
            pos1 = ccp(pos.x, self.segment1.sprite.position.y);
            pos2 = ccp(pos.x, self.segment3.sprite.position.y);
            break;
        case RIGHT2LEFT:
            pos1 = ccp(pos.x + self->gameScene.SEGMENT_PIXEL_WIDTH, self.segment1.sprite.position.y);
            pos2 = ccp(pos.x - self->gameScene.SEGMENT_PIXEL_WIDTH, self.segment1.sprite.position.y);
            break;
        case BOTTOM2TOP:
            pos1 = ccp(pos.x, self.segment1.sprite.position.y);
            pos2 = ccp(pos.x, self.segment3.sprite.position.y);
            break;
        default:
            return;
    }

    self.segment1.position = self.segment1.sprite.position = pos1;
    self.segment2.position = self.segment2.sprite.position = ccp(pos.x, self.segment2.sprite.position.y);
    self.segment3.position = self.segment3.sprite.position = pos2;
    
    [self->gameScene repositionHighlightTo:self.segment2.position.x];
}

// -----------------------------------------------------------------------

#pragma mark - Shot logic

- (void) shootAtGrid:(Grid *)grid callback:(SEL)sel {
    
    float top = [grid topSegmentTopForBlock:self], mid, bot;

    switch (self.orientation) {
        case LEFT2RIGHT:
        case RIGHT2LEFT:
            mid = bot = top;
            break;
        case TOP2BOTTOM:
            mid = top - self->gameScene.SEGMENT_PIXEL_HEIGHT;
            bot = mid - self->gameScene.SEGMENT_PIXEL_HEIGHT;
            break;
        case BOTTOM2TOP:
            bot = top;
            mid = bot - self->gameScene.SEGMENT_PIXEL_HEIGHT;
            top = mid - self->gameScene.SEGMENT_PIXEL_HEIGHT;
            break;
        default:
            return;
    }
    
    if (top < grid.lastSegmentYPos || mid < grid.lastSegmentYPos || bot < grid.lastSegmentYPos) {
        [gameScene gameOver];
        return;
    }
    
    shotCallbackSelector = sel;
    shotCount = 0;
    
    CGPoint p1 = ccp (self.segment1.sprite.position.x, top);
    CGPoint p2 = ccp (self.segment2.sprite.position.x, mid);
    CGPoint p3 = ccp (self.segment3.sprite.position.x, bot);

    [[grid segmentAtPosition:p1] setValue:self.segment1.value sprite:self.segment1.sprite];
    [[grid segmentAtPosition:p2] setValue:self.segment2.value sprite:self.segment2.sprite];
    [[grid segmentAtPosition:p3] setValue:self.segment3.value sprite:self.segment3.sprite];
    
    [Sounds shotEffect];

    CCActionCallFunc *func = [CCActionCallFunc actionWithTarget:self selector:@selector(shotComplete)];
    
    [self.segment1.sprite runAction:[CCActionSequence actions:[CCActionMoveTo actionWithDuration:.2 position:p1], func, nil]];
    [self.segment2.sprite runAction:[CCActionSequence actions:[CCActionMoveTo actionWithDuration:.2 position:p2], func, nil]];
    [self.segment3.sprite runAction:[CCActionSequence actions:[CCActionMoveTo actionWithDuration:.2 position:p3], func, nil]];
}

- (void) shotComplete {
    
    @synchronized(self) {
        if (++shotCount == 3) {
            if (![self->gameScene.grid removeSegmentsFromGrid]) {
                [gameScene addScore:10];
                [Sounds shotComplete];
                SuppressPerformSelectorLeakWarning([gameScene performSelector:shotCallbackSelector withObject:gameScene]);
            }
        }
    }
}

// -----------------------------------------------------------------------

#pragma mark - Rotation logic

- (void) rotate {
    
    CGPoint p1;
    CGPoint p2;
    
    actionCount = 0;
    
    switch (self.orientation++) {
        case LEFT2RIGHT:
            p1.x = self.segment2.sprite.position.x;
            p1.y = self.segment2.sprite.position.y + self->gameScene.SEGMENT_PIXEL_HEIGHT;
            p2.x = self.segment2.sprite.position.x;
            p2.y = self.segment2.sprite.position.y - self->gameScene.SEGMENT_PIXEL_HEIGHT;
            break;
        case TOP2BOTTOM:
            p1.x = self.segment2.sprite.position.x + self->gameScene.SEGMENT_PIXEL_WIDTH;
            p1.y = self.segment2.sprite.position.y;
            p2.x = self.segment2.sprite.position.x - self->gameScene.SEGMENT_PIXEL_WIDTH;
            p2.y = self.segment2.sprite.position.y;
            break;
        case RIGHT2LEFT:
            p1.x = self.segment2.sprite.position.x;
            p1.y = self.segment2.sprite.position.y - self->gameScene.SEGMENT_PIXEL_HEIGHT;
            p2.x = self.segment2.sprite.position.x;
            p2.y = self.segment2.sprite.position.y + self->gameScene.SEGMENT_PIXEL_HEIGHT;
            break;
        case BOTTOM2TOP:
            p1.x = self.segment2.sprite.position.x - self->gameScene.SEGMENT_PIXEL_WIDTH;
            p1.y = self.segment2.sprite.position.y;
            p2.x = self.segment2.sprite.position.x + self->gameScene.SEGMENT_PIXEL_WIDTH;
            p2.y = self.segment2.sprite.position.y;
            break;
        default:
            return;
    }
    
    if (self.orientation == 5) {
        self.orientation = 1;
    }
    
    CCActionMoveTo *move1 = [CCActionMoveTo actionWithDuration:.06 position:p1];
    CCActionMoveTo *move2 = [CCActionMoveTo actionWithDuration:.06 position:p2];
    
    [self.segment1.sprite runAction:[CCActionSequence actions:move1, [CCActionCallFunc actionWithTarget:self selector:@selector(rotationCompleted)], nil]];
    [self.segment3.sprite runAction:[CCActionSequence actions:move2, [CCActionCallFunc actionWithTarget:self selector:@selector(rotationCompleted)], nil]];
}

- (void) rotationCompleted {
    if(++actionCount == 2) {
        [self rePosition];
        [gameScene unPauseTouch];
    }
}

- (void) rePosition {
    
    if (self.orientation == 1) {
        if (self.segment1.sprite.position.x < self->gameScene.SEGMENT_PIXEL_WIDTH) {
            self.segment1.position = self.segment1.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * 1, PREP_TOP);
            self.segment2.position = self.segment2.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * 2, PREP_TOP);
            self.segment3.position = self.segment3.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * 3, PREP_TOP);
        } else if (self.segment3.sprite.position.x > self->gameScene.SEGMENT_PIXEL_WIDTH * self->gameScene.GAMEBOARD_COLUMNS) {
            self.segment1.position = self.segment1.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * (self->gameScene.GAMEBOARD_COLUMNS - 2), PREP_TOP);
            self.segment2.position = self.segment2.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * (self->gameScene.GAMEBOARD_COLUMNS - 1), PREP_TOP);
            self.segment3.position = self.segment3.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * (self->gameScene.GAMEBOARD_COLUMNS - 0), PREP_TOP);
        }
    } else if (self.orientation == 3) {
        if (self.segment1.sprite.position.x > self->gameScene.SEGMENT_PIXEL_WIDTH * self->gameScene.GAMEBOARD_COLUMNS) {
            self.segment1.position = self.segment1.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * (self->gameScene.GAMEBOARD_COLUMNS - 0), PREP_TOP);
            self.segment2.position = self.segment2.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * (self->gameScene.GAMEBOARD_COLUMNS - 1), PREP_TOP);
            self.segment3.position = self.segment3.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * (self->gameScene.GAMEBOARD_COLUMNS - 2), PREP_TOP);
        } else if (self.segment3.sprite.position.x < self->gameScene.SEGMENT_PIXEL_WIDTH) {
            self.segment1.position = self.segment1.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * 3, PREP_TOP);
            self.segment2.position = self.segment2.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * 2, PREP_TOP);
            self.segment3.position = self.segment3.sprite.position = ccp(self->gameScene.SEGMENT_PIXEL_WIDTH * 1, PREP_TOP);
        }
    }
    
    [self->gameScene repositionHighlightTo:self.segment2.position.x];
}

// -----------------------------------------------------------------------

#pragma mark - Visibility

- (void) setVisible:(BOOL)visible {
    self.segment1.sprite.visible = visible;
    self.segment2.sprite.visible = visible;
    self.segment3.sprite.visible = visible;
}

// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
#ifdef MEMTRACE
    DLog(@"Dealloc'ed Block");
#endif
    gameScene = nil;
}

@end
