//
//  GameKitHelper.m
//  RGB-Stax
//
//  Created by Allan De Leon on 8/27/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "GameKitHelper.h"

@interface GameKitHelper () <GKGameCenterControllerDelegate> {
    BOOL _gameCenterFeaturesEnabled;
}
@end

@implementation GameKitHelper

#pragma mark Singleton stuff

+ (id) sharedGameKitHelper {
    static GameKitHelper *sharedGameKitHelper;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedGameKitHelper = [[GameKitHelper alloc] init];
    });
    return sharedGameKitHelper;
}

#pragma mark Player Authentication

- (void) authenticateLocalPlayer {
    
    __weak GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    
    localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error) {
        
        [self setLastError:error];
        
        if ([CCDirector sharedDirector].isPaused) {
            [[CCDirector sharedDirector] resume];
        }
        
        if (localPlayer.authenticated) {
            _gameCenterFeaturesEnabled = YES;
        } else if(viewController) {
            [[CCDirector sharedDirector] pause];
            [self presentViewController:viewController];
        } else {
            _gameCenterFeaturesEnabled = NO;
        }
    };
}

-(void) submitScore:(int64_t)score category:(NSString*)category {
    
    if (!_gameCenterFeaturesEnabled) {
        DLog(@"Player not authenticated");
        return;
    }
    
    GKScore *gkScore = [[GKScore alloc] initWithLeaderboardIdentifier:category];
    gkScore.value = score;
    
    [GKScore reportScores:@[gkScore] withCompletionHandler: ^(NSError *error) {
        
        [self setLastError:error];
        DLog(@"Submitting score to GameCenter: %@", error);
        
        BOOL success = (error == nil);
        
        if ([_delegate respondsToSelector:@selector(onScoresSubmitted:)]) {
            [_delegate onScoresSubmitted:success];
        }
    }];
}

- (void) presentLeaderboards {
    GKGameCenterViewController* gameCenterController = [[GKGameCenterViewController alloc] init];
    gameCenterController.viewState = GKGameCenterViewControllerStateLeaderboards;
    gameCenterController.gameCenterDelegate = self;
    [[CCDirector sharedDirector] presentViewController:gameCenterController animated:YES completion:nil];
}

#pragma mark Property setters

- (void) setLastError:(NSError*)error {
    
    _lastError = [error copy];
    
    if (_lastError) {
        DLog(@"GameKitHelper ERROR: %@", [[_lastError userInfo] description]);
    }
}

- (BOOL) gameCenterEnabled {
    return _gameCenterFeaturesEnabled;
}

#pragma mark UIViewController stuff

- (UIViewController *) getRootViewController {
    return [UIApplication sharedApplication].keyWindow.rootViewController;
}

- (void) presentViewController:(UIViewController*)vc {
    UIViewController *rootVC = [self getRootViewController];
    [rootVC presentViewController:vc animated:YES completion:nil];
}

#pragma mark View controller handlers

- (void) gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController {
    DLog(@"gameCenterViewControllerDidFinish called...");
    [[CCDirector sharedDirector] dismissViewControllerAnimated:YES completion:nil];
}

@end