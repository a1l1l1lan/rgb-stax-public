//
//  Bomb.h
//  RGB-Stax
//
//  Created by Allan De Leon on 6/18/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

#import "Grid.h"
#import "Segment.h"
#import "Arsenal.h"
#import "../scenes/GameBoardScene.h"

enum { NO_BOMB = -1, CANNON_BALL, RED_BOMB, GREEN_BOMB, BLUE_BOMB, LASER, THERMONUCLEAR, BLACK_HOLE, STAR, SLOW_MOTION, RGB_BOMB};

@interface Bomb : Segment

+ (void) flashFrom:(CGPoint)from inScene:(GameBoardScene *)scene type:(int)type cleanup:(BOOL)cleanup;
+ (void) flashFrom:(CGPoint)from inScene:(GameBoardScene *)scene type:(int)type cleanup:(BOOL)cleanup bonusMultiplier:(int)mult;

- (id) init;

- (void) moveToPosition:(CGPoint)position;
- (void) explodeInScene:(GameBoardScene *)scene;
- (void) addSegmentsFromGrid:(Grid *)grid toExplosionPattern:(NSMutableSet *)explosion;

- (NSString *) bombImage;
+ (NSString *) bombImage:(int)type;

@end
