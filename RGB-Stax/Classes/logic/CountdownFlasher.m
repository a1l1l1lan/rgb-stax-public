//
//  CountdownFlasher.m
//  RGB-Stax
//
//  Created by Allan De Leon on 6/10/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "CountdownFlasher.h"
#include "Sounds.h"

@implementation CountdownFlasher {
    
    GameBoardScene  *scene;
    id              callbackTarget;
    SEL             callback;
    int             countDown;
    int             initialValue;
    
    BOOL            started;
}

// -----------------------------------------------------------------------

#pragma mark - Constructor/Initialization

- (id) initForScene:(GameBoardScene *)gameScene target:(id)target callback:(SEL)cb from:(int)from {
    
    if (!(self = [super init])) {
        return nil;
    }
    
    self->scene = gameScene;
    self->callbackTarget = target;
    self->callback = cb;
    self->started = NO;
    self->initialValue = self->countDown = from;
    
    return self;
}

// -----------------------------------------------------------------------

#pragma mark - Count down logic

- (void) start {
    
    if (started) {
        DLog(@"Already running...");
        return;
    }
    
    countDown = initialValue;
    
    [scene runAction:[CCActionSequence actions:[CCActionDelay actionWithDuration:1.5], [CCActionCallFunc actionWithTarget:self selector:@selector(countDown)], nil]];
}

- (void) countDown {
    
    if (countDown <= 0) {
        DLog(@"Countdown complete...");
        started = NO;
        [self removeNode:@"countdown-lbl"];
        SuppressPerformSelectorLeakWarning([callbackTarget performSelector:callback]);
        return;
    }
    
    [self removeNode:@"countdown-lbl"];
    
    CCLabelTTF *label = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"%d", countDown] fontName:@"cyberspace.ttf" fontSize:44];
    label.position = ccp(scene.contentSize.width / 2.0, (scene.contentSize.height / 2.0) + scene.SEGMENT_PIXEL_HEIGHT);
    label.color = [CCColor colorWithRed:.92 green:.4 blue:.4];
    label.opacity = 0;
    [scene addChild:label z:eModalLayer name:@"countdown-lbl"];
    
    [label runAction:[CCActionFadeTo actionWithDuration:.5 opacity:1]];

    // CCBSoundEffect *sound = [CCBSoundEffect actionWithSoundFile:@"beep.caf" pitch:1 pan:1 gain:.03];
    CCActionCallBlock *sound = [CCActionCallBlock actionWithBlock:^{ [Sounds beep]; }];
    CCActionScaleTo *act1 = [CCActionScaleTo actionWithDuration:.5 scale:1.5];
    CCActionDelay *act2 = [CCActionDelay actionWithDuration:.5];
    CCActionCallFunc *func = [CCActionCallFunc actionWithTarget:self selector:@selector(countDown)];
    [label runAction:[CCActionSequence actions:sound, act1, act2, func, nil]];
    
    --countDown;
}

- (void) removeNode:(NSString *)nodeName {
    
    CCNode *node = [scene getChildByName:nodeName recursively:NO];
    if (node) {
#ifdef MEMTRACE
        DLog(@"Removing child: %@", nodeName);
#endif
        [scene removeChild:node];
    }
}

// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
    DLog(@"Dealloc'ed CountdownFlasher");
    callbackTarget = nil;
    scene = nil;
}
@end
