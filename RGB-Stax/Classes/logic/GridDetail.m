//
//  GridDetail.m
//  RGB-Stax
//
//  Created by Allan De Leon on 7/9/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "GridDetail.h"

@implementation GridDetail

// -----------------------------------------------------------------------

#pragma mark - Constructor/Initialization

- (id) initWithPosition:(CGPoint)position row:(int)row column:(int)column {

    if (!(self = [super init])) {
        return nil;
    }
    
    CCSprite *sprite = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"grid-%d.png", 64]]];
    sprite.position = position;
    sprite.opacity = 0.0;
    self.border = sprite;
    self.row = row;
    self.column = column;
    
    return self;
}

// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
#ifdef MEMTRACE
    DLog(@"Dealloc'ed GridDetail");
#endif
}

@end
