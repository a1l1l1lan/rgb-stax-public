//
//  Bomb.m
//  RGB-Stax
//
//  Created by Allan De Leon on 6/18/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "Bomb.h"
#import "Sounds.h"

extern int BONUS_POINTS;
extern int COLOR_BOMB_REMOVES;

@implementation Bomb {
    CCLabelBMFont *flasher;
}

// -----------------------------------------------------------------------

#pragma mark - Constructor/Initialization

- (id) init {
    
    if (!(self = [super init])) {
        return nil;
    }
    
    flasher = nil;
    
    return self;
}

// -----------------------------------------------------------------------

#pragma mark - Add bomb logic

+ (void) flashFrom:(CGPoint)from inScene:(GameBoardScene *)scene type:(int)type cleanup:(BOOL)cleanup {
    [Bomb flashFrom:from inScene:scene type:type cleanup:cleanup bonusMultiplier:1];
}

+ (void) flashFrom:(CGPoint)from inScene:(GameBoardScene *)scene type:(int)type cleanup:(BOOL)cleanup bonusMultiplier:(int)mult {

    CGPoint to = (type != STAR) ? ccp(0,0) : ccp(0,scene.contentSize.height);
    
    CCSprite *sprite = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:[Bomb bombImage:type]]];
    sprite.position = from;
    [scene addChild:sprite z:eReadyBombLayer];
    
    Bomb *bomb = [[Bomb alloc] init];
    [bomb setBomb:sprite];
    [bomb setBombType:type];
    [bomb flashToPosition:to scene:scene remove:cleanup bonusMultipler:mult];
}

- (void) flashToPosition:(CGPoint)position scene:(GameBoardScene *)scene remove:(BOOL)remove bonusMultipler:(int)mult {
    
    if (!self.bomb) {
        return;
    }
    
    CCActionScaleBy  *grow   = [CCActionScaleBy actionWithDuration:.15 scale:4];
    CCActionScaleTo  *shrink = [CCActionScaleTo actionWithDuration:.1 scale:.75];
    CCActionMoveTo   *move   = [CCActionMoveTo actionWithDuration:.2 position:position];
    CCActionCallFunc *func   = [CCActionCallFunc actionWithTarget:self selector:@selector(removeMe)];
    
    [self.bomb runAction:[CCActionSequence actions: grow, shrink, move, (remove ? func: nil), nil]];
    
    if (self.bombType == STAR) {
        [self flashBonusPoints:scene type:self.bombType bonusMultiplier:mult];
    }
}

- (void) removeMe {
    
    if (self.bomb) {
        CCNode *parent = [self.bomb parent];
        [parent removeChild:self.bomb];
    }
}

// -----------------------------------------------------------------------

#pragma mark - Bonus points logic

- (void) flashBonusPoints:(GameBoardScene *)scene type:(int)type bonusMultiplier:(int)mult {
    
    int points = BONUS_POINTS * mult;
    
    [scene addScore:points];
    
    int xpos = scene.contentSize.width / 4;
    int ypos = arc4random_uniform(100) + 100;
    
    flasher = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"+%d", points] fntFile:@"bonus-splash.fnt"];
    flasher.anchorPoint = ccp(.5,.5);
    flasher.position = ccp(arc4random_uniform(xpos * 2) + xpos, scene.contentSize.height - ypos);
    flasher.opacity = 1;
    flasher.scale = .1;
    [scene addChild:flasher z:eModalLayer];
    
    CCActionScaleTo   *grow   = [CCActionScaleTo actionWithDuration:.2 scale:5];
    CCActionScaleTo   *shrink = [CCActionScaleTo actionWithDuration:.4 scale:0];
    CCActionDelay     *delay  = [CCActionDelay actionWithDuration:.2];
    CCActionMoveTo    *move   = [CCActionMoveTo actionWithDuration:.1 position:ccp(0,scene.contentSize.height)];
    CCActionCallBlock *pling  = [CCActionCallBlock actionWithBlock:^{ [Sounds pling]; }];
    
    [flasher runAction:[CCActionSequence actions:pling, delay, shrink, nil]];
    [flasher runAction:[CCActionSequence actions:grow, delay, move, nil]];
}

// -----------------------------------------------------------------------

#pragma mark - Positioning logic

- (void) moveToPosition:(CGPoint)position {
    self.bomb.position = ccp(position.x, position.y + 100);
}

// -----------------------------------------------------------------------

#pragma mark - Explosion logic

- (void) explodeInScene:(GameBoardScene *)scene {
    
    DLog(@"Exploding bomb...");
    
    [scene.arsenal setActive:NO];
    [scene.grid handleBomb:self];
    [scene removeChild:self.bomb];
}

- (void) addSegmentsFromGrid:(Grid *)grid toExplosionPattern:(NSMutableSet *)explosion {
    
    CGPoint position = self.bomb.position;
    
    int startRow = [grid rowForPosition:position];
    int startCol = [grid columnForPosition:position];
    
    startRow = (startRow > -1) ? startRow : position.x > grid.scene.contentSize.width / 2.0 ? 0 : grid.scene.GAMEBOARD_ROWS - 1;
    startCol = (startCol > -1) ? startCol : position.y < grid.scene.contentSize.height / 2.0 ? 0 : grid.scene.GAMEBOARD_COLUMNS - 1;

    /**
     * Dynamically calls the proper explosion pattern based on this bomb's type
     **/
    
    SEL mSelector = [self explosionPatternSelector];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[self methodSignatureForSelector:mSelector]];
    [invocation setTarget:self];
    [invocation setSelector:mSelector];
    [invocation setArgument:&explosion atIndex:2];
    [invocation setArgument:&startRow  atIndex:3];
    [invocation setArgument:&startCol  atIndex:4];
    [invocation setArgument:&grid      atIndex:5];
    [invocation invoke];
}

// -----------------------------------------------------------------------

#pragma mark - Explosion patterns

- (SEL) explosionPatternSelector {
    return NSSelectorFromString([NSString stringWithFormat:@"explodePattern_%d:startRow:startCol:toGrid:", [self bombType]]);
}

- (void) explodeColorBomb:(int)color explosion:(NSMutableSet *)explosion grid:(Grid *)grid {
    
    [Sounds explode];
    
    int limit = COLOR_BOMB_REMOVES; // Number of segments to remove
    for (int row = 0; row < grid.scene.GAMEBOARD_ROWS; ++row) {
        for (int col = 0; col < grid.scene.GAMEBOARD_COLUMNS; ++col) {
            Segment *seg = [[[grid gameBoard] objectAtIndex:row] objectAtIndex:col];
            if (seg.value == color) {
                [explosion addObject:seg];
                if (!--limit) {
                    return;
                }
            }
        }
    }
    
    if ([explosion count] == 0) {
        [grid.scene flashMessage:@"Missed!"];
    }
}

#pragma mark Pattern 0 (CANNON_BALL)

- (void) explodePattern_0:(NSMutableSet *)explosion startRow:(int)startRow startCol:(int)startCol toGrid:(Grid *)grid {
    
    [Sounds explode];
    
    Segment *seg = [grid segmentAtRow:startRow column:startCol];
    
    if (seg && seg.value > -1) {
        [explosion addObject:seg];
    }
    
    if (startRow - 1 >= 0) {
        seg = [grid segmentAtRow:startRow - 1 column:startCol];
        if (seg && seg.value > -1) {
            [explosion addObject:seg];
        }
    }
    
    if (startRow + 1 < grid.scene.GAMEBOARD_ROWS) {
        seg = [grid segmentAtRow:startRow + 1 column:startCol];
        if (seg && seg.value > -1) {
            [explosion addObject:seg];
        }
    }
    
    if (startCol - 1 >= 0) {
        seg = [grid segmentAtRow:startRow column:startCol - 1];
        if (seg && seg.value > -1) {
            [explosion addObject:seg];
        }
    }
    
    if (startCol + 1 < grid.scene.GAMEBOARD_COLUMNS) {
        seg = [grid segmentAtRow:startRow column:startCol + 1];
        if (seg && seg.value > -1) {
            [explosion addObject:seg];
        }
    }
    
    if ([explosion count] == 0) {
        [grid.scene flashMessage:@"Missed!"];
    }
}

#pragma mark Pattern 1 (RED CANNON_BALL)

- (void) explodePattern_1:(NSMutableSet *)explosion startRow:(int)startRow startCol:(int)startCol toGrid:(Grid *)grid {
    [self explodeColorBomb:0 explosion:explosion grid:grid];
}

#pragma mark Pattern 2 (GREEN CANNON_BALL)

- (void) explodePattern_2:(NSMutableSet *)explosion startRow:(int)startRow startCol:(int)startCol toGrid:(Grid *)grid {
    [self explodeColorBomb:1 explosion:explosion grid:grid];
}

#pragma mark Pattern 3 (BLUE CANNON_BALL)

- (void) explodePattern_3:(NSMutableSet *)explosion startRow:(int)startRow startCol:(int)startCol toGrid:(Grid *)grid {
    [self explodeColorBomb:2 explosion:explosion grid:grid];
}

#pragma mark Pattern 8 (SLOW SHOT CLOCK)

- (void) explodePattern_8:(NSMutableSet *)explosion startRow:(int)startRow startCol:(int)startCol toGrid:(Grid *)grid {
    [grid.scene animateTimerClock];
}

// -----------------------------------------------------------------------

#pragma mark - Bomb image name

- (NSString *) bombImage {
    return [NSString stringWithFormat:@"bomb-%d.png", [self bombType]];
}

+ (NSString *) bombImage:(int)type {
    return [NSString stringWithFormat:@"bomb-%d.png", type];
}

// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
    
    DLog(@"Dealloc'ed Bomb");
    
    if (flasher) {
        DLog(@"Removing flasher...");
        [flasher.parent removeChild:flasher];
        flasher = nil;
    }
}

@end
