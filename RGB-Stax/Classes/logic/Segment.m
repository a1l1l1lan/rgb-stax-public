//
//  Segment.m
//  RGB-Stax
//
//  Created by Allan De Leon on 5/20/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "Segment.h"
#import "Bomb.h"

@implementation Segment

// -----------------------------------------------------------------------

#pragma mark - Serialization

- (id) initWithCoder:(NSCoder *)decoder {
    
    if (!(self = [super init])) {
        return nil;
    }
    
    self.value    = [decoder decodeIntForKey:@"value"];
    self.bombType = [decoder decodeIntForKey:@"bombType"];
    
    return self;
}

- (void) encodeWithCoder:(NSCoder *)encoder {
    [encoder encodeInt:self.value forKey:@"value"];
    [encoder encodeInt:self.bombType forKey:@"bombType"];
}

// -----------------------------------------------------------------------

#pragma mark - Constructor/Initialization

- (id) initWithPosition:(CGPoint)pos {
    return [self initWithPosition:pos value:-1 row:0 column:0];
}

- (id) initWithPosition:(CGPoint)pos value:(int)value {
    return [self initWithPosition:pos value:value row:0 column:0];
}

- (id) initWithPosition:(CGPoint)pos value:(int)value row:(int)row column:(int)column {
    
    if (!(self = [super init])) {
        return nil;
    }
    
    self.bombType = NO_BOMB;
    self.position = pos;
    self.value = value;
    self.row = row;
    self.column = column;
    
    return self;
}

// -----------------------------------------------------------------------

#pragma mark - Mutators

- (void) setValue:(int)value sprite:(CCSprite *)sprite {
    self.value = value;
    self.sprite = sprite;
}

// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
#ifdef MEMTRACE
    DLog(@"Dealloc'ed Segment");
#endif
}

@end
