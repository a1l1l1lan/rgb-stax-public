//
//  CountdownFlasher.h
//  RGB-Stax
//
//  Created by Allan De Leon on 6/10/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

#import "../scenes/GameBoardScene.h"

@interface CountdownFlasher : NSObject

- (id) initForScene:(GameBoardScene *)gameScene target:(id)target callback:(SEL)cb from:(int)from;

- (void) start;

@end
