//
//  Block.h
//  RGB-Stax
//
//  Created by Allan De Leon on 5/20/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

#import "Segment.h"
#import "../scenes/GameBoardScene.h"

@class Grid;

enum {
    LEFT2RIGHT = 1,
    TOP2BOTTOM,
    RIGHT2LEFT,
    BOTTOM2TOP
};

@interface Block : NSObject

@property (strong, atomic) Segment *segment1;
@property (strong, atomic) Segment *segment2;
@property (strong, atomic) Segment *segment3;

@property (assign) int orientation;

- (id)   initWithScene:(GameBoardScene *)scene;

- (void) moveToReadyPosition;
- (void) moveTo:(CGPoint)pos;
- (void) rotate;
- (void) shootAtGrid:(Grid *)grid callback:(SEL)sel;
- (void) setVisible:(BOOL)visible;

@end
