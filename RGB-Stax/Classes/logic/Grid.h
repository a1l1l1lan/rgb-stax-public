//
//  Grid.h
//  RGB-Stax
//
//  Created by Allan De Leon on 5/21/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

#import "Arsenal.h"
#import "Block.h"
#import "Bomb.h"
#import "Segment.h"

#import "../scenes/GameBoardScene.h"

@class GameState;

@interface Grid : NSObject

@property (assign) float lastSegmentYPos;
@property (strong) GameBoardScene *scene;

- (id) initForScene:(GameBoardScene *) scene;
- (float) dropXPos:(CGPoint)pos;
- (float) topSegmentTopForBlock:(Block *)block;

- (Segment *) segmentAtPosition:(CGPoint)pos;
- (Segment *) segmentAtRow:(int)row column:(int)col;

- (NSArray *) gameBoard;

- (int) rowForPosition:(CGPoint)pos;
- (int) columnForPosition:(CGPoint)pos;
- (int) warnLevel;

- (BOOL) hasSegments;
- (BOOL) removeSegmentsFromGrid;

- (void) handleBomb:(Bomb *)bomb;
- (void) fadeTo:(CGFloat)fade;
- (void) gameOverAnimation:(Block *)block;
- (void) resetGrid;
- (void) saveGame;
- (void) dump;

@end
