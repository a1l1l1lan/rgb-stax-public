//
//  Arsenal.m
//  RGB-Stax
//
//  Created by Allan De Leon on 6/20/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "Grid.h"
#import "Arsenal.h"
#import "Sounds.h"
#import "GameState.h"

extern int  BOMB_WARN_FREQ;
extern BOOL DARK_THEME;
extern BOOL IS_IPHONE5X;

int     BOMBS_PER_ROW       = 5;
int     GRID_SIZE           = 25;
CGFloat INIT_BOMB_POSITION  = 280.0;
CGFloat BOMB_PICKER_SPACING = 6.0;

@implementation Arsenal {
    
    int                 pulseCount;
    int                 pulseChildIndex;
    int                 pulsed;
    int                 perRow;
    CGFloat             initX;
    CGFloat             nextX;
    CGFloat             nextY;
    CGFloat             spacingInc;
    
    CCLabelBMFont       *empty;
    CCSprite            *indicator;
    CCSprite            *background;
    NSMutableArray      *bombs;
    NSMutableDictionary *bombCount;
    NSMutableDictionary *arsenalCount;
    NSDictionary        *bombLimits;
}

// -----------------------------------------------------------------------

#pragma mark - Game state logic

- (void) saveGameState {
    
    GameState *state = [GameState sharedState];
    NSUserDefaults *defaults = [state getUserDefaults];
    
    if (state && defaults) {
        [defaults setObject:[self bombsToData] forKey:@"arsenal"];
    }
}

- (void) restoreGameState {
    
    GameState *state = [GameState sharedState];
    NSUserDefaults *defaults = [state getUserDefaults];
    NSData *data = [defaults objectForKey:@"arsenal"];
    
    if (state && defaults && data) {
        NSArray *barray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        for (Bomb *bomb in barray) {
            [self.scene.arsenal addToArsenal:bomb.bombType immediate:YES];
        }
    }
}

// -----------------------------------------------------------------------

#pragma mark - Constructor/Initialization

- (id) initWithScene:(GameBoardScene *)scene {
    
    if (!(self = [super init])) {
        return nil;
    }
    
    bombs = [[NSMutableArray alloc] init];
    bombCount = [[NSMutableDictionary alloc] init];
    arsenalCount = [[NSMutableDictionary alloc] init];
    
    self.active = NO;
    background  = nil;
    indicator   = nil;
    
    [self initBombLimits];
    [self setScene:scene];
    [self initArsenal];
    
    pulseCount = BOMB_WARN_FREQ;
    
    return self;
}

- (void) initArsenal {
    
    background = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"arsenal-bg.png"]];
    background.anchorPoint = ccp(0,0);
    background.position = ccp(0,-400);
    
    indicator = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"bomb-bar.png"]];
    indicator.anchorPoint = ccp(.5,0);
    indicator.position = ccp(self.scene.contentSize.width / 2.0, 1);
    indicator.visible = NO;
    
    CCButton *close = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"close-btn.png"]];
    close.anchorPoint = ccp(.5,.5);
    close.position = ccp(255,338);
    [close setTarget:self selector:@selector(doHideArsenal)];
    [background addChild:close z:eCommandLayer name:@"close-btn"];
    
    empty = [CCLabelBMFont labelWithString:@"Empty Arsenal!" fntFile:@"staxfonts-clear.fnt"];
    empty.anchorPoint = ccp(.5,.5);
    empty.color = [CCColor whiteColor];
    empty.position = ccp(background.contentSize.width / 2, background.contentSize.height / 2 - 40);
    empty.scale = 1.3;
    [background addChild:empty z:eModalLayer name:@"empty"];
    
    [self.scene addChild:background z:eBombLayer name:@"arsenal-bg"];
    [self.scene addChild:indicator z:eBombLayer name:@"indicator-bomb"];
    [self resetBombCounts];
    
    spacingInc = background.contentSize.width / BOMB_PICKER_SPACING;
    initX  = spacingInc;
    nextX  = initX;
    nextY  = INIT_BOMB_POSITION;
    perRow = 0;
    
    [self initBombGrid];
}

- (NSData *) bombsToData {
    return [NSKeyedArchiver archivedDataWithRootObject:bombs];
}

// -----------------------------------------------------------------------

#pragma mark - Bomb indicator logic

- (void) showIndicator {
    
    if ([bombs count] > 0) {
        indicator.visible = YES;
    }
}

- (void) removeIndicator {
    indicator.visible = NO;
}

- (void) pulseIndicator:(int)level {
    
    if (level <= 0 || [[indicator children] count] <= 0) {
        return;
    }
    
    if (pulseCount-- != BOMB_WARN_FREQ) {
        if (pulseCount == 0) {
            pulseCount = BOMB_WARN_FREQ;
        }
        return;
    }
    
    pulsed = (unsigned) [[indicator children] count];
    pulseChildIndex = 0;
    
    [self pulseChild];
}

- (void) pulseChild {
    
    if (pulseChildIndex >= pulsed) {
        [self resetPulsed];
        return;
    }
    
    CCSprite *sprite = [[indicator children] objectAtIndex:pulseChildIndex++];
    CCActionScaleTo  *grow  = [CCActionScaleTo actionWithDuration:.1 scale:2];
    CCActionCallFunc *func  = [CCActionCallFunc actionWithTarget:self selector:@selector(pulseChild)];
    [sprite runAction:[CCActionFadeOut actionWithDuration:.1]];
    [sprite runAction:[CCActionSequence actions:grow, func, nil]];
}

- (void) resetPulsed {
    
    [Sounds warn];
    
    for (CCSprite *sprite in [indicator children]) {
        sprite.scale = .7;
        sprite.opacity = 1;
    }
}

// -----------------------------------------------------------------------

#pragma mark - Bomb creation logic

- (int) createBomb:(int)type random:(BOOL)random {
    
    if ([self bombCountForType:type] >= [self limitForBombType:type]) {
        type = STAR;
    }
    
    if (random) {
        for (int tries = 0; tries < 3; ++tries) {
            for (int row = self.scene.GAMEBOARD_ROWS -1; row >= 0; --row) {
                for (int col = 0; col < self.scene.GAMEBOARD_COLUMNS; ++col) {
                    Segment *seg = [self.scene.grid segmentAtRow:row column:col];
                    if (seg.value > -1 && seg.bombType == NO_BOMB && arc4random_uniform(100) > 85) {
                        CCSprite *bomb = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:[Bomb bombImage:type]]];
                        bomb.position = ccp(seg.sprite.contentSize.width / 2, seg.sprite.contentSize.height / 2);
                        if (!IS_IPHONE5X) {
                            bomb.scale = .8;
                        }
                        [seg.sprite addChild:bomb z:eReadyBombLayer];
                        seg.bombType = type;
                        seg.bomb = bomb;
                        [self incrementBombCountForType:type];
                        DLog(@"Type: %d; count: %d; limit: %d", type, [self bombCountForType:type], [self limitForBombType:type]);
                        return type;
                    }
                }
            }
        }
    }

    return NO_BOMB;
}

// -----------------------------------------------------------------------

#pragma mark - Add to arsenal logic

- (int) addToArsenal:(int)type immediate:(BOOL)immediate {
    
    if ((immediate && [self bombCountForType:type] >= [self limitForBombType:type]) || type == STAR) {
        return STAR;
    }
    
    CCSprite *bsprite = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:[Bomb bombImage:type]]];
    bsprite.scale = 1.80;
    
    Bomb *bomb = [[Bomb alloc] init];
    bomb.bombType = type;
    bomb.bomb = bsprite;
    
    [bombs addObject:bomb];
    [background addChild:bomb.bomb z:eBombLayer];
    
    bomb.bomb.position = ccp (nextX, nextY);
    if (++perRow >= BOMBS_PER_ROW) {
        nextX  = initX;
        nextY -= spacingInc;
        perRow = 0;
    } else {
        nextX += spacingInc;
    }
    
    [self showIndicator];
    [self updateArsenamIndicatorForType:bomb.bombType add:YES];
    [self incrementArsenalCountForType:type];
    
    if (immediate) {
        [self incrementBombCountForType:type];
    }
    
    return  type;
}

// -----------------------------------------------------------------------

#pragma mark - Clearing arsenal logic

- (void) clearArsenal {
    
    for (Bomb *bomb in bombs) {
        [background removeChild:bomb.bomb];
    }
    
    [bombs removeAllObjects];
    [self removeIndicator];
    [self resetBombCounts];
    [indicator removeAllChildren];
    
    pulseCount = BOMB_WARN_FREQ;
    perRow     = 0;
    nextX      = initX;
    nextY      = INIT_BOMB_POSITION;
}

// -----------------------------------------------------------------------

#pragma mark - Show/hide arsenal logic

- (void) showArsenal {
    
    int bCount = [self bombCount];
    self.active = YES;
    if (bCount == 0) {
        empty.visible = YES;
        [self setGridOpacity:0];
    } else {
        empty.visible = NO;
        [self setGridOpacity:1];
    }
    [self.scene pauseForArsenal];
    CCActionCallBlock *sound = [CCActionCallBlock actionWithBlock:^{ [Sounds panelOpen]; }];
    CCActionMoveTo *move = [CCActionMoveTo actionWithDuration:.15 position:ccp(0,0)];
    CCActionDelay *delay = [CCActionDelay actionWithDuration:bCount <= 0 ? .8 : 3];
    CCActionCallBlock *close = [CCActionCallBlock actionWithBlock:^{ [self hideArsenal]; }];
    [background runAction:[CCActionSequence actions:sound, move, delay, close, nil]];
}

- (void) doHideArsenal {
    [background stopAllActions];
    [self hideArsenal];
}

- (void) hideArsenal {
    
    if (!self.active || background.position.y < 0) {
        return;
    }
    
    self.active = NO;
    CCActionCallBlock *sound = [CCActionCallBlock actionWithBlock:^{ [Sounds panelClose]; }];
    CCActionMoveTo *move = [CCActionMoveTo actionWithDuration:.15 position:ccp(0,-400)];
    [background runAction:[CCActionSequence actions:sound, move, nil]];
    [self.scene unPauseForArsenal];
}

// -----------------------------------------------------------------------

#pragma mark - Bomb selection logic

- (Bomb *) activateBomb:(CGPoint)position {
    
    Bomb *bomb = [self bombAtPosition:position];
    
    if (bomb) {
        
        self.active = YES;
        [background removeChild:bomb.bomb cleanup:NO];
        
        CCActionCallBlock *sound = [CCActionCallBlock actionWithBlock:^{ [Sounds panelClose]; }];
        CCActionMoveTo *move = [CCActionMoveTo actionWithDuration:.15 position:ccp(0,-400)];
        [background runAction:[CCActionSequence actions:sound, move, [CCActionCallBlock actionWithBlock:^{ [background stopAllActions]; }], nil]];
        [self.scene.grid fadeTo:1];
        [bombs removeObject:bomb];
        [bomb setBomb:nil];
        
        CCSprite *newBomb = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:[bomb bombImage]]];
        newBomb.position = ccp(position.x, position.y);
        newBomb.scale = 5;
        [self.scene addChild:newBomb z:eGamePieceLayer name:@"active-bomb"];
        [newBomb runAction:[CCActionSequence actions:[CCActionMoveTo actionWithDuration:.1 position:ccp(position.x, position.y + 100)], [CCActionDelay actionWithDuration:.5], nil]];
        bomb.bomb = newBomb;
        
        if (bomb.bombType == CANNON_BALL) {
            CCSprite *crosshairs = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"cross-hairs.png"]];
            crosshairs.anchorPoint = ccp(.5,.5);
            crosshairs.scale = IS_IPHONE5X ? .25 : .175;
            crosshairs.opacity = .7;
            crosshairs.position = ccp(bomb.bomb.contentSize.width / 2, bomb.bomb.contentSize.height / 2);
            [bomb.bomb addChild:crosshairs];
            if (DARK_THEME) {
                bomb.bomb.opacity = .7;
            }
        }
        
        if ([bombs count] == 0) {
            [self removeIndicator];
        }
        
        [self reOrganizeArsenal];
        [self updateArsenamIndicatorForType:bomb.bombType add:NO];
        [self decrementBombCountForType:bomb.bombType];
        [self decrementArsenalCountForType:bomb.bombType];
        
        return bomb;
    }
    
    return nil;
}

- (void) reOrganizeArsenal {

    int   c = 0;
    float x = initX, y = INIT_BOMB_POSITION;
    for (Bomb *b in bombs) {
        b.bomb.position = ccp(x,y);
        if (++c >= BOMBS_PER_ROW) {
            x = initX;
            y -= spacingInc;
            c = 0;
        } else {
            x += spacingInc;
        }
    }
    
    nextX = x;
    nextY = y;
    perRow = c;
}

- (void) initBombGrid {
    
    int i = 0;
    float x = nextX, y = nextY;
    for (int row = 1; row <= GRID_SIZE; ++row) {
        CCSprite *hiligh = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"bomb-hilight.png"]];
        hiligh.position = ccp(x,y);
        hiligh.scale = 1.8;
        [background addChild:hiligh];
        if (++i >= BOMBS_PER_ROW) {
            x = initX;
            y -= spacingInc;
            i = 0;
        } else {
            x += spacingInc;
        }
    }
}

- (void) setGridOpacity:(float)opacity {
    
    for (CCSprite *sprite in [background children]) {
        if (![[sprite name] isEqual:@"empty"]) {
            sprite.opacity = opacity;
        }
    }
}

- (Bomb *) bombAtPosition:(CGPoint)position {
    
    for (Bomb *b in bombs) {
        if (CGRectContainsPoint([b.bomb boundingBox], position)) {
            return b;
        }
    }
    
    return nil;
}

// -----------------------------------------------------------------------

#pragma mark - Bomb count logic

- (void) initBombLimits {
    
    bombLimits = @{
                   @0: @4  , // normal bombs:        5
                   @1: @2  , // red bombs:           2
                   @2: @2  , // green bombs:         2
                   @3: @2  , // blue bombs:          2
                   @4: @3  , // lasers:              5
                   @5: @1  , // thermonuclear bombs: 1
                   @6: @1  , // black holes:         1
                   @7: @108, // bonus stars - no limit
                   @8: @2  , // Slow motion          2
                   @9: @1    // RGB BOMB! The name-sake bomb!
                   };
}

- (void) resetBombCounts {
    
    for (int i = CANNON_BALL; i < RGB_BOMB; ++i) {
        [bombCount setObject:[NSNumber numberWithInt:0] forKey:[NSNumber numberWithInt:i]];
    }
}

- (int) bombCountForType:(int)type {
    
    NSNumber *key = [NSNumber numberWithInt:type];
    NSNumber *count = [bombCount objectForKey:key];
    if (count != nil) {
        return [count intValue];
    }
    
    return 0;
}

- (void) decrementBombCountForType:(int)type {
    
    NSNumber *key = [NSNumber numberWithInt:type];
    NSNumber *count = [bombCount objectForKey:key];
    if ([count intValue] > 0) {
        [bombCount setObject:[NSNumber numberWithInt:[count intValue] - 1] forKey:key];
    }
}

- (void) incrementBombCountForType:(int)type {
    
    NSNumber *key = [NSNumber numberWithInt:type];
    NSNumber *count = [bombCount objectForKey:key];
    [bombCount setObject:[NSNumber numberWithInt:[count intValue] + 1] forKey:key];
}

- (int) limitForBombType:(int)type {
    
    NSNumber *limit = [bombLimits objectForKey:[NSNumber numberWithInt:type]];
    if (limit != nil) {
        return [limit intValue];
    }
    
    return 0;
}

// -----------------------------------------------------------------------

#pragma mark - Arsenal count logic

- (void) updateArsenamIndicatorForType:(int)type add:(BOOL)add {
    
    if (add) {
        if ([self arsenalCountForType:type] > 0) {
            return;
        } else {
            CCSprite *bomb = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:[Bomb bombImage:type]]];
            bomb.anchorPoint = ccp(0,.5);
            bomb.position = ccp(24 * type, indicator.contentSize.height / 2.0);
            bomb.scale = .7;
            [indicator addChild:bomb z:eBombLayer name:[NSString stringWithFormat:@"%d", type]];
        }
    } else {
        if ([self arsenalCountForType:type] > 1) {
            return;
        } else {
            [indicator removeChildByName:[NSString stringWithFormat:@"%d", type]];
        }
    }
}

- (int) arsenalCountForType:(int)type {
    
    NSNumber *key = [NSNumber numberWithInt:type];
    NSNumber *count = [arsenalCount objectForKey:key];
    if (count != nil) {
        return [count intValue];
    }
    
    return 0;
}

- (void) decrementArsenalCountForType:(int)type {
    
    NSNumber *key = [NSNumber numberWithInt:type];
    NSNumber *count = [arsenalCount objectForKey:key];
    if ([count intValue] > 0) {
        [arsenalCount setObject:[NSNumber numberWithInt:[count intValue] - 1] forKey:key];
    }
}

- (void) incrementArsenalCountForType:(int)type {
    
    NSNumber *key = [NSNumber numberWithInt:type];
    NSNumber *count = [arsenalCount objectForKey:key];
    [arsenalCount setObject:[NSNumber numberWithInt:[count intValue] + 1] forKey:key];
}

// -----------------------------------------------------------------------

#pragma mark - Mutators/Accessors

- (unsigned) bombCount {
    return (unsigned) [bombs count];
}

// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
    
    DLog(@"Dealloc'ed Arsenal");
    
    if (background && [[self.scene children] containsObject:background]) {
        [self.scene removeChild:background cleanup:YES];
    }
    
    if (indicator && [[self.scene children] containsObject:indicator]) {
        [self.scene removeChild:indicator cleanup:YES];
    }
    
    background = nil;
    indicator  = nil;
    self.scene = nil;
}

@end
