//
//  Option.h
//  RGB-Stax
//
//  Created by Allan De Leon on 7/22/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//


#import "cocos2d.h"
#import "cocos2d-ui.h"

@interface Option : NSObject

@property (strong, atomic) NSString *label;
@property (strong, atomic) NSString *name;
@property (assign) BOOL             booleanValue;
@property (assign) SEL              handler;
@property (assign) float            ypos;
@property (assign) CCButton         *button;

- (id) initWithName:(NSString *)name label:(NSString *)label booleanValue:(BOOL)value selector:(SEL)selector;

@end
