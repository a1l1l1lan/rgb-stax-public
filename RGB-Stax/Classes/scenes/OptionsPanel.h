//
//  OptionsPanel.h
//  RGB-Stax
//
//  Created by Allan De Leon on 7/17/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

@interface OptionsPanel : NSObject

- (id) initWithParent:(CCScene *)parent;

- (void) show;
- (void) hide;

@end
