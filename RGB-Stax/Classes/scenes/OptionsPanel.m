//
//  OptionsPanel.m
//  RGB-Stax
//
//  Created by Allan De Leon on 7/17/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "CCTextureCache.h"
#import "OptionsPanel.h"
#import "GameBoardScene.h"
#import "Option.h"
#import "../logic/GameState.h"
#import "../logic/Sounds.h"

extern float OPTION_TOP;
extern float OPTION_DEC;

extern BOOL  IS_IPHONE5X;

@implementation OptionsPanel {
    
    CCSprite       *panel;
    CCScene        *parentNode;
    NSMutableArray *options;
    
    BOOL           creditsShown;
}

- (id) initWithParent:(CCScene *)parent {
    
    if (!(self = [super init])) {
        return nil;
    }
    
    self->parentNode = parent;
    panel = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"options-panel.png"]];
    panel.anchorPoint = ccp(0,0);
    panel.position = ccp(0, self->parentNode.contentSize.height);
    
    CCButton *close = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"close-btn.png"]];
    close.anchorPoint = ccp(.5,.5);
    close.position = ccp(247, IS_IPHONE5X ? 114 : 62);
    [close setTarget:self selector:@selector(hide)];
    [panel addChild:close z:eCommandLayer name:@"close-btn"];
    
    options = [[NSMutableArray alloc] init];
    
    CCButton *creditsButton = [CCButton buttonWithTitle:@"Credits" fontName:@"cyberspace.ttf" fontSize:28.0f];
    creditsButton.anchorPoint = ccp(1,0);
    creditsButton.position = ccp(panel.contentSize.width - 20, 20);
    creditsButton.scale = .5;
    [creditsButton setTarget:self selector:@selector(showCredits)];
    [panel addChild:creditsButton z:eBackgroundLayer name:@"credits-openbtn"];
    
    [self createOptions];
    creditsShown = NO;
    
    return self;
}

- (void) showCredits {
    
    creditsShown = YES;
    [Sounds panelOpen];
    
    CCSprite *credits = [CCSprite spriteWithImageNamed:@"credits.png"];
    credits.anchorPoint = ccp(0,0);
    credits.position = ccp(0,0);
    [panel addChild:credits z:eCommandLayer name:@"credits-panel"];
    
    CCButton *closeButton = [CCButton buttonWithTitle:@"Close" fontName:@"cyberspace.ttf" fontSize:28.0f];
    closeButton.anchorPoint = ccp(1,0);
    closeButton.position = ccp(panel.contentSize.width - 20, 20);
    closeButton.scale = .5;
    [closeButton setTarget:self selector:@selector(closeCredits)];
    [credits addChild:closeButton z:eBackgroundLayer name:@"credits-closebtn"];
}

- (void) closeCredits {
    creditsShown = NO;
    [Sounds panelClose];
    [panel removeChildByName:@"credits-panel" cleanup:YES];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFrameByName:@"credits.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"credits.png"];
}

- (void) createOptions {
    
    GameState *gs = [GameState sharedState];
    
    [options addObject:[[Option alloc] initWithName:@"music" label:@"music" booleanValue:[gs getBooleanFor:@"music" withDefault:YES] selector:@selector(toggleMusic)]];
    [options addObject:[[Option alloc] initWithName:@"sound-effect" label:@"sound effects" booleanValue:[gs getBooleanFor:@"sound-effect" withDefault:YES] selector:@selector(toggleSoundEffect)]];
    // [options addObject:[[Option alloc] initWithName:@"hints" label:@"hints" booleanValue:[[GameState sharedState] getBooleanFor:@"hints" withDefault:YES] selector:@selector(toggleHints)]];
    [options addObject:[[Option alloc] initWithName:@"introreset" label:@"show intro" booleanValue:[[GameState sharedState] getBooleanFor:@"introreset" withDefault:NO] selector:@selector(resetQuickIntro)]];
    
    if (![parentNode isKindOfClass:[GameBoardScene class]]) {
        [options addObject:[[Option alloc] initWithName:@"flat-theme" label:@"flat theme" booleanValue:[[GameState sharedState] getBooleanFor:@"flat-theme" withDefault:YES] selector:@selector(toggleFlatTheme)]];
    }
    
    [self layoutOptions];
}

- (void) layoutOptions {
    
    float ypos = OPTION_TOP - (IS_IPHONE5X ? 0 : 60);
    for (Option *opt in options) {
        
        CCLabelBMFont *label = [CCLabelBMFont labelWithString:opt.label fntFile:@"staxfonts-clear.fnt"];
        label.anchorPoint = ccp(0,0);
        label.position = ccp(38, ypos);
        [panel addChild:label z:eCommandLayer name:opt.name];
        
        CCButton *mark = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:(opt.booleanValue ? @"checkmark.png" : @"xmark.png")]];
        mark.anchorPoint = ccp(0,0);
        mark.position = ccp(250,ypos - 10);
        [mark setTarget:self selector:opt.handler];
        [panel addChild:mark z:eCommandLayer];
        
        opt.button = mark;
        opt.ypos   = mark.position.y;

        ypos -= OPTION_DEC;
    }
}

- (CCButton *) buttonForOption:(Option *)option {
    CCButton *button = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:(option.booleanValue ? @"checkmark.png" : @"xmark.png")]];
    [button setTarget:self selector:option.handler];
    button.anchorPoint = ccp(0,0);
    button.position = ccp(250, option.ypos);
    [panel addChild:button z:eCommandLayer];
    return button;
}

- (void) resetQuickIntro {
    
    if (creditsShown) {
        return;
    }
    
    Option *opt = [self optionByName:@"introreset"];
    opt.booleanValue = !opt.booleanValue;
    [opt.button.parent removeChild:opt.button];
    opt.button = [self buttonForOption:opt];
    [[GameState sharedState] setBoolean:opt.booleanValue for:@"introreset"];
    [[GameState sharedState] setBoolean:!opt.booleanValue for:@"RUNQUICKINTRO"];
    opt.booleanValue ? [Sounds check] : [Sounds unCheck];
}

- (void) toggleFlatTheme {
    
    if (creditsShown) {
        return;
    }
    
    Option *opt = [self optionByName:@"flat-theme"];
    opt.booleanValue = !opt.booleanValue;
    [opt.button.parent removeChild:opt.button];
    opt.button = [self buttonForOption:opt];
    [[GameState sharedState] setBoolean:opt.booleanValue for:@"flat-theme"];
    
    CCLabelBMFont *notice = [CCLabelBMFont labelWithString:@"Requires App re-start" fntFile:@"staxfonts-clear.fnt"];
    notice.anchorPoint = ccp(0,0);
    notice.scale = .7;
    notice.color = [CCColor yellowColor];
    notice.position = ccp(38, opt.ypos - 4);
    [panel addChild:notice];
    opt.booleanValue ? [Sounds check] : [Sounds unCheck];
}

- (void) toggleMusic {
    
    if (creditsShown) {
        return;
    }
    
    Option *opt = [self optionByName:@"music"];
    opt.booleanValue = !opt.booleanValue;
    [opt.button.parent removeChild:opt.button];
    opt.button = [self buttonForOption:opt];
    [[GameState sharedState] setBoolean:opt.booleanValue for:@"music"];
    if (!opt.booleanValue) {
        [Sounds stopBackgroundMusic];
    } else if (opt.booleanValue) {
        [Sounds updateSoundOptions];
        if (![parentNode isKindOfClass:[GameBoardScene class]]) {
            [Sounds introMusic];
        } else {
            [Sounds gameMusic];
        }
    }
    [Sounds updateSoundOptions];
}

- (void) toggleSoundEffect {
    
    if (creditsShown) {
        return;
    }
    
    Option *opt = [self optionByName:@"sound-effect"];
    opt.booleanValue = !opt.booleanValue;
    [opt.button.parent removeChild:opt.button];
    opt.button = [self buttonForOption:opt];
    [[GameState sharedState] setBoolean:opt.booleanValue for:@"sound-effect"];
    [Sounds updateSoundOptions];
    if (opt.booleanValue) {
        [Sounds check];
    }
}

- (void) toggleHints {
    
    if (creditsShown) {
        return;
    }
    
    Option *opt = [self optionByName:@"hints"];
    opt.booleanValue = !opt.booleanValue;
    [opt.button.parent removeChild:opt.button];
    opt.button = [self buttonForOption:opt];
    [[GameState sharedState] setBoolean:opt.booleanValue for:@"hints"];
    opt.booleanValue ? [Sounds check] : [Sounds unCheck];
}

- (Option *) optionByName:(NSString *)name {
    
    for (Option *opt in options) {
        if ([opt.name isEqualToString:name]) {
            return opt;
        }
    }
    
    return nil;
}

- (void) show {
    [parentNode addChild:panel];
    CCActionMoveTo    *move1 = [CCActionMoveTo actionWithDuration:.2 position:ccp(0,-(parentNode.contentSize.height + 40))];
    CCActionMoveTo    *move2 = [CCActionMoveTo actionWithDuration:.2 position:ccp(0,-parentNode.contentSize.height)];
    CCActionCallBlock *sound = [CCActionCallBlock actionWithBlock:^{ [Sounds panelOpen]; }];
    [parentNode runAction:[CCActionSequence actions:sound, move1, move2, nil]];
}

- (void) hide {
    CCActionMoveTo *move =[CCActionMoveTo actionWithDuration:.2 position:ccp(0, 0)];
    CCActionCallBlock *sound = [CCActionCallBlock actionWithBlock:^{ [Sounds panelClose]; }];
    CCActionCallBlock *remove = [CCActionCallBlock actionWithBlock:^{ [self removeFromParent]; }];
    [parentNode runAction:[CCActionSequence actions:sound, move, remove, nil]];
}

- (void) removeFromParent {
    [panel runAction:[CCActionRemove action]];
}

// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
    DLog(@"Dealloc'ed OptionsPanel");
    parentNode = nil;
}

@end
