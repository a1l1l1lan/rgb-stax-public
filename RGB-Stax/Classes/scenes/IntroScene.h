//
//  IntroScene.h
//  RGB-Stax
//
//  Created by Allan De Leon on 5/20/14.
//  Copyright Allan De Leon 2014. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

@interface IntroScene : CCScene

+ (IntroScene *) scene;
- (id) init;

@end