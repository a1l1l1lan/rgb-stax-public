//
//  SceneUtil.h
//  RGB-Stax
//
//  Created by Allan De Leon on 6/20/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import <Foundation/Foundation.h>

@interface SceneUtil : NSObject

+ (BOOL) isRetina;

@end
