//
//  Tutorial.h
//  RGB-Stax
//
//  Created by Allan De Leon on 8/10/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

#import "GameBoardScene.h"

@interface Tutorial : CCScene

+ (Tutorial *) scene;
- (id) init;

@end
