//
//  GameBoardScene.h
//  RGB-Stax
//
//  Created by Allan De Leon on 5/20/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "cocos2d.h"
#import "cocos2d-ui.h"

enum {
    eBackgroundLayer = 0,
    eHiLightLayer,
    eGamePieceLayer,
    eReadyBombLayer,
    eBombLayer,
    eControlLayer,
    eModalLayer,
    eCommandLayer
};

@class Grid;
@class Bomb;
@class Arsenal;
@class CountdownFlasher;

@interface GameBoardScene : CCScene

@property (strong)   Grid     *grid;
@property (strong)   Arsenal  *arsenal;

@property (readonly) unsigned GAMEBOARD_TOP;
@property (readonly) unsigned GAMEPIECE_SIZE;
@property (readonly) unsigned GAMEBOARD_ROWS;
@property (readonly) unsigned GAMEBOARD_COLUMNS;

@property (readonly) float    SEGMENT_PIXEL_HEIGHT;
@property (readonly) float    SEGMENT_PIXEL_WIDTH;

+ (GameBoardScene *) scene;
- (id)   init;

- (void) gameOver;
- (void) createBomb;
- (void) unPauseTouch;
- (void) resetShotTimer;
- (void) addScore:(unsigned)points;
- (void) repositionHighlightTo:(int)x;
- (void) setPausedState;
- (void) setUnpausedState;
- (void) animateTimerClock;
- (void) setSkipNextBlock;
- (void) pauseForArsenal;
- (void) unPauseForArsenal;
- (void) saveGameState;
- (void) restoreGameState;
- (void) flashMessage:(NSString *)sprite;
- (void) postGameOverAnimation;

- (BOOL) pauseTouch;
- (CCSprite *) createSpriteWithValue:(int)value;
- (int) currentScore;

@end
