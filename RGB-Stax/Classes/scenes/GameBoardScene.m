//
//  GameBoardScene.m
//  RGB-Stax
//
//  Created by Allan De Leon on 5/20/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "GameBoardScene.h"
#import "IntroScene.h"
#import "SceneUtil.h"

#import "OptionsPanel.h"
#import "../logic/Bomb.h"
#import "../logic/Block.h"
#import "../logic/Grid.h"
#import "../logic/Sounds.h"
#import "../logic/Arsenal.h"
#import "../logic/GameState.h"
#import "../logic/CountdownFlasher.h"

#pragma mark - GameBoardScene

extern int PREP_TOP;

extern int GAMEPIECE_SIZE;
extern int GAMEBOARD_TOP;
extern int GAMEBOARD_ROWS;
extern int GAMEBOARD_COLUMNS;

extern float SHOTTIME_INC;
extern float INITIAL_SHOTTIME_LIMIT;
extern float SHOTTIME_DEC;
extern float SHOTTIME_MIN;

extern unsigned BLACK_THRESHHOLD_INC;
extern unsigned BOMB_THRESHHOLD_INC;
extern unsigned STAR_THRESHHOLD_INC;
extern unsigned TIMER_THRESHHOLD_INC;
extern unsigned SLOW_MO_THRESHHOLD_INC;

extern BOOL     IS_IPHONE5X;
extern BOOL     DARK_THEME;
extern NSString *THEME;

@implementation GameBoardScene {
    
    OptionsPanel   *optionsPanel;
    
    Bomb           *currentBomb;
    NSArray        *colors;
    Block          *readyBlock;
    Block          *currentBlock;
    NSDate         *curTime;
    NSTimer        *shotTimer;
    
    CCSprite       *highlight;
    CCSprite       *gridBackground;
    CCSprite       *timerBackground;
    CCSprite       *pauseModal;
    
    CCProgressNode *timerIndicator;
    CCLabelTTF     *scoreDisplay;
    CCButton       *pause;
    CGPoint        startTouch;
    
    BOOL           pauseTouch;
    BOOL           timerPaused;
    BOOL           running;
    BOOL           skipNextBlock;
    
    float          timeLimit;
    float          shotTimerElapsed;
    
    unsigned       score;
    unsigned       randomBlackAttempt;
    unsigned       blackThreshold;
    unsigned       bombThreshold;
    unsigned       starThreshold;
    unsigned       timerThreshold;
    unsigned       slowMotionThreshold;
    
    NSNumberFormatter *formatter;
}

// -----------------------------------------------------------------------

#pragma mark - Constructor/Initialization

+ (GameBoardScene *) scene {
    return [[self alloc] init];
}

- (id) init {
    
    if (!(self = [super init])) {
        return nil;
    }
    
    DLog(@"Game scene starting up; Retina: %d", [SceneUtil isRetina]);
    
    [self preloadTextureAtlas];
    [self setArsenal:[[Arsenal alloc] initWithScene:self]];
    [self setUserInteractionEnabled:YES];
    [self initOptions];
    [self initColors];
    [(formatter = [[NSNumberFormatter alloc] init]) setNumberStyle:NSNumberFormatterDecimalStyle];
    
    timeLimit      = INITIAL_SHOTTIME_LIMIT;
    blackThreshold = BLACK_THRESHHOLD_INC;
    bombThreshold  = BOMB_THRESHHOLD_INC;
    starThreshold  = STAR_THRESHHOLD_INC;
    timerThreshold = TIMER_THRESHHOLD_INC;
    randomBlackAttempt  = 0;
    slowMotionThreshold = SLOW_MO_THRESHHOLD_INC;
    
    timerPaused = NO;
    pauseTouch  = NO;
    running     = NO;
    skipNextBlock = NO;
    currentBomb = nil;
    
    return self;
}

- (void) initColors {
    colors = [NSArray arrayWithObjects:@"red", @"green", @"blue", @"black", nil];
}

- (void) initOptions {
    
    _GAMEPIECE_SIZE    = GAMEPIECE_SIZE;
    _GAMEBOARD_TOP     = GAMEBOARD_TOP - (IS_IPHONE5X ? 0 : 86);
    _GAMEBOARD_ROWS    = GAMEBOARD_ROWS;
    _GAMEBOARD_COLUMNS = GAMEBOARD_COLUMNS;
    
    CCSprite *segment  = [CCSprite spriteWithImageNamed:@"grid-64.png"];
    _SEGMENT_PIXEL_HEIGHT = segment.contentSize.height;
    _SEGMENT_PIXEL_WIDTH  = segment.contentSize.width;
}

- (void) preloadTextureAtlas {
    [self addChild:[CCSpriteBatchNode batchNodeWithFile:THEME]];
    DLog(@"Loaded theme: %@", THEME);
}

// -----------------------------------------------------------------------

#pragma mark - Transition handlers

- (void) onEnterTransitionDidFinish {
    [super onEnterTransitionDidFinish];
    [self startupSequence];
}

- (void) onExitTransitionDidStart {
    [super onExitTransitionDidStart];
}

// -----------------------------------------------------------------------

#pragma mark - Game state logic

- (void) saveGameState {
    
    GameState *state = [GameState sharedState];
    if (state) {
        NSUserDefaults *defaults = [state getUserDefaults];
        [defaults setInteger:score forKey:@"currentScore"];
        [defaults setInteger:timerThreshold forKey:@"timerThreshold"];
        [defaults setFloat:timeLimit forKey:@"timeLimit"];
        [state addHighScore:score];
    }
}

- (void) restoreGameState {
    
    GameState *state = [GameState sharedState];
    NSUserDefaults *defaults = [state getUserDefaults];
    
    if (state && defaults) {
        
        NSInteger *cScore = [defaults integerForKey:@"currentScore"];
        [self addScore:cScore];
        
        NSInteger tThreshhold = [defaults integerForKey:@"timerThreshold"];
        timerThreshold = tThreshhold == 0 ? score : (unsigned) tThreshhold;
        
        float tLimit = [defaults floatForKey:@"timeLimit"];
        timeLimit = tLimit == 0 ? INITIAL_SHOTTIME_LIMIT : tLimit;
        
        DLog(@"LIMIT: %f; Thresh: %d", timeLimit, timerThreshold);
    }
}

// -----------------------------------------------------------------------

#pragma mark - Setup game background

- (void) initBackground {
    
    // Background
    CCSprite *background = [CCNodeColor nodeWithColor:[CCColor colorWithRed:.92 green:.92 blue:.92]];
    background.anchorPoint = ccp(0,0);
    background.position = ccp(0,0);
    [self addChild:background z:eBackgroundLayer name:@"bg"];
    
    gridBackground = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"grid-bg.png"]];
    gridBackground.anchorPoint = ccp(0,0);
    gridBackground.position = ccp(0,0);
    gridBackground.scale = 0;
    [self addChild:gridBackground z:eBackgroundLayer name:@"grid-bg"];
}

// -----------------------------------------------------------------------

#pragma mark - UI control logic

- (void) initControls {
    
    // Highlight
    highlight = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"highlight.png"]];
    highlight.position = ccp(self.contentSize.width / 2.0, self.GAMEBOARD_TOP - self.GAMEBOARD_ROWS / 2.0 * self.SEGMENT_PIXEL_HEIGHT + (self.SEGMENT_PIXEL_HEIGHT / 2.0));
    highlight.opacity = 0.0;
    [self addChild:highlight z:eHiLightLayer];
    [highlight runAction:[CCActionFadeIn actionWithDuration:.3]];
    
    // Pause label
    pause = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"pause-btn.png"]];
    pause.position = ccp(self.contentSize.width - 16, self.GAMEBOARD_TOP + 34);
    pause.anchorPoint = ccp(1,.5);
    
    [pause setTarget:self selector:@selector(pause)];
    [self addChild:pause z:eGamePieceLayer];
    
    // Shot timer background
    timerBackground = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"timerbg.png"]];
    timerBackground.position = ccp(self.contentSize.width / 2.0, self.GAMEBOARD_TOP + 33);
    [self addChild:timerBackground z:eBackgroundLayer name:@"timerbg"];
    
    // Shot timer indicator
    timerIndicator = [CCProgressNode progressWithSprite:[CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"timer.png"]]];
    timerIndicator.position = ccp(self.contentSize.width / 2.0, self.GAMEBOARD_TOP + 33);
    timerIndicator.type = CCProgressNodeTypeBar;
    timerIndicator.barChangeRate = ccp(1,0);
    timerIndicator.midpoint = ccp(0,0);
    // timerIndicator.opacity = 0.75;
    timerIndicator.percentage = 0;
    [self addChild:timerIndicator z:eGamePieceLayer name:@"timer-indicator"];
}

- (void) showControls {
    
    CCButton *tryAgainButton = [CCButton buttonWithTitle:@"Try Again?" fontName:@"cyberspace.ttf" fontSize:18];
    tryAgainButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
    tryAgainButton.anchorPoint = ccp(.5,.5);
    tryAgainButton.position = ccp(self.contentSize.width / 2.0, (self.contentSize.height / 2.0) - 100);
    [tryAgainButton setTarget:self selector:@selector(tryAgainSequence)];
    [self addChild:tryAgainButton z:eModalLayer name:@"tryagain-btn"];
    
    CCButton *quitButton = [CCButton buttonWithTitle:@"Quit Game?" fontName:@"cyberspace.ttf" fontSize:18];
    quitButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
    quitButton.anchorPoint = ccp(.5,.5);
    quitButton.position = ccp(self.contentSize.width / 2.0, (self.contentSize.height / 2.0) - 160);
    [quitButton setTarget:self selector:@selector(quit)];
    [self addChild:quitButton z:eCommandLayer name:@"quit-btn"];
}

// -----------------------------------------------------------------------

#pragma mark - Startup sequence

- (void) startupSequence {

    [self initBackground];
    
    readyBlock = [self createBlock];
    [self setReadyBlockVisible:NO];

    CCLabelTTF *label = [CCLabelTTF labelWithString:@"Ready?" fontName:@"cyberspace.ttf" fontSize:44];
    label.positionType = CCPositionTypeNormalized;
    label.color = [CCColor colorWithRed:.92 green:.3 blue:.3];
    label.position = ccp(0.5f, 0.75f);
    [self addChild:label z:eCommandLayer name:@"ready-lbl"];
    
    [[[CountdownFlasher alloc] initForScene:self target:self callback:@selector(startupGo) from:3] start];
}

- (void) startupGo {
    
    [self removeChildByName:@"ready-lbl"];
    [self flashGo];
    [Sounds gameMusic];
    
    [self runAction:[CCActionSequence actions:[CCActionCallFunc actionWithTarget:self selector:@selector(startup)], nil]];
}

- (void) startup {
    
    [self flashMessage:[@[@"G'Luck!",@"Have Fun!",@"Let's Go!",@"Woohoo!"] objectAtIndex:arc4random_uniform(4)]];
    
    running = YES;
    [gridBackground runAction:[CCActionScaleTo actionWithDuration:.25 scale:1]];
    
    shotTimer = [NSTimer scheduledTimerWithTimeInterval:SHOTTIME_INC target:self selector:@selector(shotTimer:) userInfo:nil repeats:YES];

    [self initScoreDisplay];
    [self initControls];
    [self nextBlock];

    self.grid = [[Grid alloc] initForScene:self];
    
    [[GameState sharedState] setGrid:self.grid];
}

// -----------------------------------------------------------------------

#pragma mark - Pause logic

- (BOOL) pauseTouch {
    
    @synchronized(self) {
        if (self->pauseTouch) {
            return NO;
        }
        return (self->pauseTouch = YES);
    }
}

- (void) unPauseTouch {
    self->pauseTouch = NO;
}

- (void) pause {
    
    if (![self pauseTouch]) {
        return;
    }
    
    [self setPausedState];
    [self showPauseModal];
}

- (void) unPauseSequence {
    [pauseModal removeChildByName:@"options-btn"];
    [pauseModal removeChildByName:@"pause-continue"];
    // [pauseModal removeChildByName:@"quit-btn"];
    [pauseModal removeChildByName:@"savequit-btn"];
    [self showUnpauseModal];
}

- (void) unPauseGo {
    [pauseModal removeChildByName:@"ready-lbl"];
    CCActionCallBlock *sound = [CCActionCallBlock actionWithBlock:^{ [Sounds panelClose]; }]; // TODO: replace with resume sound!
    CCActionMoveTo *move = [CCActionMoveTo actionWithDuration:.4 position:ccp(0,800)];
    [pauseModal runAction:[CCActionSequence actions:sound, move, nil]];
    [self flashGo];
    [self runAction:[CCActionSequence actions:[CCActionDelay actionWithDuration:.3], [CCActionCallFunc actionWithTarget:self selector:@selector(unPause)], nil]];
}

- (void) unPause {
    [self setUnpausedState];
}

- (void) setPausedState {
    
    pauseTouch = YES;
    timerPaused = YES;
    highlight.visible = NO;
    
    [self setReadyBlockVisible:NO];
    
    if (currentBlock && currentBlock.segment2.sprite.position.y == PREP_TOP) {
        [currentBlock setVisible:NO];
    }
}

- (void) setUnpausedState {
    
    pauseTouch = NO;
    timerPaused = NO;
    highlight.visible = YES;
    timerIndicator.visible = YES;
   
    [self setReadyBlockVisible:YES];
    [self.grid fadeTo:1];
    
    if (currentBlock && !currentBlock.segment2.sprite.visible) {
        [currentBlock setVisible:YES];
    }
}

- (void) pauseForArsenal {
    pauseTouch = YES;
    timerPaused = YES;

}
- (void) unPauseForArsenal {
    pauseTouch = NO;
    timerPaused = NO;
}

- (void) showPauseModal {
    
    if (!pauseModal) {
        pauseModal = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"pause-bg.png"]];
        pauseModal.anchorPoint = ccp(0,0);
        pauseModal.position = ccp(0,800);
        [self addChild:pauseModal z:eModalLayer name:@"pause-bg"];
        
        CCLabelBMFont *text = [CCLabelBMFont labelWithString:@"Game Paused" fntFile:@"staxfonts-clear.fnt"];
        text.positionType = CCPositionTypeNormalized;
        text.position = ccp(.497, IS_IPHONE5X ? .779 : .82);
        text.scaleX = 1.06;
        text.scaleY = 1.65;
        [pauseModal addChild:text];
    }
    
    CCButton *optionsButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"btn-blue.png"]];
    optionsButton.positionType = CCPositionTypeNormalized;
    optionsButton.position = ccp(0.5f, 0.52f);
    [optionsButton setTarget:self selector:@selector(showOptions)];
    [self setLabel:@"Options" forButton:optionsButton];
    [pauseModal addChild:optionsButton z:eCommandLayer name:@"options-btn"];
    
    // CCButton *quitButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"btn-red.png"]];
    // quitButton.positionType = CCPositionTypeNormalized;
    // quitButton.position = ccp(0.5f,0.52f);
    // [quitButton setTarget:self selector:@selector(quit)];
    // [self setLabel:@"Quit" forButton:quitButton];
    // [pauseModal addChild:quitButton z:eCommandLayer name:@"quit-btn"];
    
    CCButton *saveQuitButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"btn-red.png"]];
    saveQuitButton.positionType = CCPositionTypeNormalized;
    saveQuitButton.position = ccp(0.5f,0.43f);
    [saveQuitButton setTarget:self selector:@selector(saveQuit)];
    [self setLabel:(score > 0 ? @"Save/Quit" : @"Quit") forButton:saveQuitButton];
    [pauseModal addChild:saveQuitButton z:eCommandLayer name:@"savequit-btn"];
    
    CCButton *continueButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"btn-green.png"]];
    continueButton.positionType = CCPositionTypeNormalized;
    continueButton.position = ccp(0.5f, 0.34f);
    [continueButton setTarget:self selector:@selector(unPauseSequence)];
    [self setLabel:@"Resume" forButton:continueButton];
    [pauseModal addChild:continueButton z:eCommandLayer name:@"pause-continue"];
    
    CCActionCallBlock *sound = [CCActionCallBlock actionWithBlock:^{ [Sounds panelOpen]; }]; // TODO: replace with pause sound
    CCActionMoveTo *move = [CCActionMoveTo actionWithDuration:.2 position:ccp(0,0)];
    [pauseModal runAction:[CCActionSequence actions:sound, move, nil]];
}

- (void) showOptions {
    
    if (!optionsPanel) {
        optionsPanel = [[OptionsPanel alloc] initWithParent:self];
    }
    
    [optionsPanel show];
}

- (void) setLabel:(NSString *)label forButton:(CCButton *)btn {
    CCLabelBMFont *text = [CCLabelBMFont labelWithString:label fntFile:@"staxfonts-clear.fnt"];
    text.anchorPoint = ccp(.5,.5);
    text.position = ccp(btn.contentSize.width / 2, btn.contentSize.height / 2);
    [btn addChild:text];
}

- (void) showUnpauseModal {
    
    CCLabelBMFont *label = [CCLabelBMFont labelWithString:@"Ready?" fntFile:@"staxfonts-clear.fnt"];
    label.positionType = CCPositionTypeNormalized;
    label.color = [CCColor redColor];
    label.position = ccp(0.5f, 0.58f);
    label.scale = 2;
    [pauseModal addChild:label z:eCommandLayer name:@"ready-lbl"];
    [pauseModal runAction:[CCActionSequence actions:[CCActionDelay actionWithDuration:1.5], [CCActionCallFunc actionWithTarget:self selector:@selector(unPauseGo)], nil]];
}

// -----------------------------------------------------------------------

#pragma mark - Game over logic

- (void) flashPlayerRank {
    
    if (score <= 5000) {
        [self flashMessage:@"Amateur"];
    } else if (score <= 30000) {
        [self flashMessage:@"Not Bad"];
    } else if (score <= 60000) {
        [self flashMessage:@"Good"];
    } else if (score <= 95000) {
        [self flashMessage:@"Advanced"];
    } else if (score <= 130000) {
        [self flashMessage:@"Master"];
    } else if (score > 130000) {
        [self flashMessage:@"Guru!"];
    }
}

- (void) gameOver {
    
    [Sounds stopBackgroundMusic];
    [shotTimer invalidate];
    [self flashPlayerRank];
    [[GameState sharedState] setGrid:nil];
    [[GameState sharedState] removeSavedGame];
    [self.arsenal clearArsenal];
    
    running = NO;
    highlight.visible = NO;
    ((CCControl *)pause).enabled = NO;
    blackThreshold = BLACK_THRESHHOLD_INC;
    bombThreshold  = BOMB_THRESHHOLD_INC;
    starThreshold  = STAR_THRESHHOLD_INC;
    timerThreshold = TIMER_THRESHHOLD_INC;
    timeLimit      = INITIAL_SHOTTIME_LIMIT;
    optionsPanel   = nil;
    self.arsenal   = nil;
    slowMotionThreshold = SLOW_MO_THRESHHOLD_INC;
    
    [[GameState sharedState] addHighScore:score];
    
    [self pauseTouch];
    [self setReadyBlockVisible:NO];
    [self.grid gameOverAnimation:currentBlock];
}

- (void) postGameOverAnimation {
    
    timerBackground.visible = NO;
    gridBackground.opacity = 0;
    pause.visible = NO;
    ((CCControl *)pause).enabled = YES;
    
    CCLabelTTF *gameOverLabel = [CCLabelTTF labelWithString:@"Game Over!" fontName:@"cyberspace.ttf" fontSize:28];
    gameOverLabel.visible = YES;
    gameOverLabel.color = [CCColor redColor];
    gameOverLabel.position = ccp(self.contentSize.width / 2.0, (self.contentSize.height / 2.0) + 175);
    gameOverLabel.opacity = 0.0;
    [self addChild:gameOverLabel z:eModalLayer name:@"gameover-lbl"];
    [gameOverLabel runAction:[CCActionFadeIn actionWithDuration:.5]];
    
    [self removeChild:scoreDisplay cleanup:YES];
    scoreDisplay = [CCLabelTTF labelWithString:[NSString stringWithFormat:@"Score: %@", [self score]] fontName:@"cyberspace.ttf" fontSize:22];
    scoreDisplay.color = [CCColor colorWithRed:.8 green:.3 blue:.3];
    scoreDisplay.anchorPoint = ccp(0.5,0.5);
    scoreDisplay.scaleX = .5;
    scoreDisplay.position = ccp(self.contentSize.width / 2.0, (self.contentSize.height / 2.0) + 100);
    [self addChild:scoreDisplay];
    
    [self runAction:[CCActionSequence actions:[CCActionDelay actionWithDuration:.7], [CCActionCallFunc actionWithTarget:self selector:@selector(showControls)], nil]];
    
    CCLabelTTF *highScoreLabel = [CCLabelTTF labelWithString:[self highScoreLabel] fontName:@"cyberspace.ttf" fontSize:22];
    highScoreLabel.position = ccp(self.contentSize.width / 2.0, (self.contentSize.height / 2.0) + 30);
    highScoreLabel.color = [CCColor colorWithRed:.5 green:.5 blue:.97];
    highScoreLabel.scaleX = .5;
    [self addChild:highScoreLabel z:eModalLayer name:@"highscore-lbl"];
}

- (void) quit {
    [[GameState sharedState] addHighScore:score];
    [[GameState sharedState] setGrid:nil];
    [[GameState sharedState] removeSavedGame];
    optionsPanel = nil;
    [self doQuit];
}

- (void) saveQuit {
    
    if (score <= 0) {
        [self quit];
        return;
    }
    
    [[GameState sharedState] saveGameState];
    [[GameState sharedState] setGrid:nil];
    optionsPanel = nil;
    [self doQuit];
}

- (void) doQuit {
    
    [Sounds stopBackgroundMusic];
    [Sounds quitGame];
   
    [shotTimer invalidate];
    
    pause = nil;
    colors = nil;
    curTime = nil;
    shotTimer = nil;
    formatter = nil;
    highlight = nil;
    readyBlock = nil;
    scoreDisplay = nil;
    currentBlock = nil;
    timerIndicator = nil;
    
    self.grid = nil;
    self.arsenal = nil;
    
    [[CCDirector sharedDirector] replaceScene:[IntroScene scene] withTransition:[CCTransition transitionFadeWithDuration:.9f]];
    [self removeAllChildren];
}

// -----------------------------------------------------------------------

#pragma mark - Try again logic

- (void) tryAgainSequence {
    
    [Sounds startGame];
    [[GameState sharedState] setGrid:self.grid];

    if (currentBlock) {
        [self removeChild:currentBlock.segment1.sprite];
        [self removeChild:currentBlock.segment2.sprite];
        [self removeChild:currentBlock.segment3.sprite];
        currentBlock = nil;
    }
    
    [self removeChildByName:@"gameover-lbl" cleanup:YES];
    [self removeChildByName:@"tryagain-btn" cleanup:YES];
    [self removeChildByName:@"highscore-lbl" cleanup:YES];
    [self removeChildByName:@"quit-btn" cleanup:YES];
    [self removeChild:scoreDisplay cleanup:YES];
    [self initScoreDisplay];
    [self setArsenal:[[Arsenal alloc] initWithScene:self]];

    [scoreDisplay setVisible:NO];
    
    CCLabelBMFont *label = [CCLabelBMFont labelWithString:@"Ready?" fntFile:@"staxfonts-clear.fnt"];
    label.positionType = CCPositionTypeNormalized;
    label.color = [CCColor redColor];
    label.position = ccp(0.5f, 0.75f);
    label.scale = 2;
    [self addChild:label z:eCommandLayer name:@"ready-lbl"];
    
    [[[CountdownFlasher alloc] initForScene:self target:self callback:@selector(tryAgain) from:3] start];
}

- (void) tryAgain {
    
    running = YES;
    pause.visible = YES;
    highlight.visible = YES;
    timerBackground.visible = YES;
    scoreDisplay.visible = YES;
    
    [self removeChildByName:@"ready-lbl" cleanup:YES];
    [self setReadyBlockVisible:YES];
    [self flashGo];
    
    gridBackground.opacity = 1;
    
    shotTimer = [NSTimer scheduledTimerWithTimeInterval:SHOTTIME_INC target:self selector:@selector(shotTimer:) userInfo:nil repeats:YES];

    [self.grid resetGrid];
    [self nextBlock];
    [self unPauseTouch];

    [Sounds gameMusic];
}

// -----------------------------------------------------------------------

#pragma mark - Timing logic

- (void) shotTimer:(NSTimer *)timer {
    
    if (timerPaused) {
        return;
    }
    
    timerIndicator.percentage = (shotTimerElapsed + 1) * (100.0f / timeLimit);
    // timerIndicator.opacity = timerIndicator.percentage / 100.0;
    shotTimerElapsed += SHOTTIME_INC;
    if (timerIndicator.percentage >= 100.0 && [self pauseTouch]) {
        [self executeShot];
    }
}

// -----------------------------------------------------------------------

#pragma mark - Shot execution logic

- (void) executeShot {
    timerIndicator.percentage = 0.0;
    [currentBlock shootAtGrid:self.grid callback:@selector(nextBlock)];
}

// -----------------------------------------------------------------------

#pragma mark - Block logic

- (void) setSkipNextBlock {
    skipNextBlock = YES;
}

- (void) nextBlock {
    
    [self setReadyBlockVisible:YES];
    
    if (!skipNextBlock) {
        currentBlock = readyBlock;
        readyBlock = [self createBlock];
        [currentBlock moveToReadyPosition];
        highlight.position = ccp(self.contentSize.width / 2.0, self.GAMEBOARD_TOP - self.GAMEBOARD_ROWS / 2.0 * self.SEGMENT_PIXEL_HEIGHT + (self.SEGMENT_PIXEL_HEIGHT / 2.0));
    } else {
        skipNextBlock = NO;
        [self unPauseForArsenal];
    }
    
    shotTimerElapsed = 0.0f;
    timerIndicator.percentage = 0;
    
    [self.arsenal pulseIndicator:[self.grid warnLevel]];
}

- (Block *) createBlock {
    
    CCSprite *sprite;
    Block *block = [[Block alloc] initWithScene:self];
    
    block.segment1.value = [self nextValue];
    sprite = [self createSpriteWithValue:block.segment1.value];
    sprite.position = ccp(98,PREP_TOP + 52);
    sprite.scaleY = .15;
    sprite.scaleX = .7;
    [self addChild:sprite z:eGamePieceLayer];
    block.segment1.sprite = sprite;
    
    block.segment2.value = [self nextValue];
    sprite = [self createSpriteWithValue:block.segment2.value];
    sprite.position = ccp(160,PREP_TOP + 52);
    sprite.scaleY = .15;
    sprite.scaleX = .7;
    [self addChild:sprite z:eGamePieceLayer];
    block.segment2.sprite = sprite;
    
    block.segment3.value = [self nextValue];
    sprite = [self createSpriteWithValue:block.segment3.value];
    sprite.position = ccp(222,PREP_TOP + 52);
    sprite.scaleY = .15;
    sprite.scaleX = .7;
    [self addChild:sprite z:eGamePieceLayer];
    block.segment3.sprite = sprite;
    
    return block;
}

- (void) setReadyBlockVisible:(BOOL)val {
    if (readyBlock) {
        [readyBlock setVisible:val];
    }
}

- (CCSprite *) createSpriteWithValue:(int)value {
    CCSprite *sprite = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:[NSString stringWithFormat:@"%@-%d.png", colors[value], 64]]];
    return sprite;
}

- (int) nextValue {
    
    if (score > blackThreshold) {
        ++randomBlackAttempt;
        if (arc4random_uniform(100) < (33 * randomBlackAttempt)) {
            while (blackThreshold <= score) {
                blackThreshold += BLACK_THRESHHOLD_INC;
            }
            randomBlackAttempt = 0;
            return 3;
        } else if (randomBlackAttempt > 2) {
            while (blackThreshold <= score) {
                blackThreshold += BLACK_THRESHHOLD_INC;
            }
            randomBlackAttempt = 0;
            return 3;
        }
    }
    
    return arc4random_uniform(3);
}

// -----------------------------------------------------------------------

#pragma mark - Highlight logic

- (void) repositionHighlightTo:(int)x {
    if (x > 0) {
        highlight.position = ccp(x, highlight.position.y);
    }
}

// -----------------------------------------------------------------------

#pragma mark - Go flash logic

- (void) flashGo {
    
    CCLabelTTF *label = [CCLabelTTF labelWithString:@"Go!" fontName:@"cyberspace.ttf" fontSize:44];
    label.positionType = CCPositionTypeNormalized;
    label.color = [CCColor redColor];
    label.position = ccp(0.5f, 0.61f);
    [self addChild:label z:eCommandLayer name:@"go-lbl"];
    
    [label runAction:[CCActionScaleTo actionWithDuration:.25 scale:8]];
    [label runAction:[CCActionSequence actions:[CCActionFadeOut actionWithDuration:.25], [CCActionCallFunc actionWithTarget:self selector:@selector(removeGo)], nil]];
}

- (void) removeGo {
    [self removeChildByName:@"go-lbl"];
}

// -----------------------------------------------------------------------

#pragma mark - Score logic

- (void) addScore:(unsigned)points {
    
    @synchronized(self) {
        score += points;
        [scoreDisplay setString:[self score]];
        // Speed up shot timer...
        if (timeLimit >= SHOTTIME_MIN && score > timerThreshold) {
            timerThreshold += TIMER_THRESHHOLD_INC;
            timeLimit -= SHOTTIME_DEC;
        }
    }
}

- (NSString *) score {
    return [formatter stringFromNumber:[NSNumber numberWithInteger:score]];
}

- (void) initScoreDisplay {
    
    score = 0;
    
    scoreDisplay = [CCLabelBMFont labelWithString:[self score] fntFile:(DARK_THEME ? @"score-light.fnt" : @"score-dark.fnt")];
    scoreDisplay.anchorPoint = ccp(0,.5);
    scoreDisplay.position = ccp(12, self.GAMEBOARD_TOP + 31);
    [self addChild:scoreDisplay z:eBackgroundLayer];
}

- (int) currentScore {
    return score;
}

- (id) highScoreLabel {
    return [NSString stringWithFormat:@"High Score: %@", [formatter stringFromNumber:[NSNumber numberWithInt:[[GameState sharedState] highScore]]]];
}

// -----------------------------------------------------------------------

#pragma mark - Arsenal logic

- (void) createBomb {
    
    
    if (score >= slowMotionThreshold) {
        DLog(@"Score: %d; Time Threshold: %d", score, slowMotionThreshold);
        [self.arsenal createBomb:SLOW_MOTION random:YES];
        slowMotionThreshold += SLOW_MO_THRESHHOLD_INC;
    }
    
    int type = NO_BOMB;
    if (score >= bombThreshold) {
        DLog(@"Score: %d; Bomb Threshold: %d", score, bombThreshold);
        type = [self.arsenal createBomb:CANNON_BALL random:YES];
        bombThreshold += BOMB_THRESHHOLD_INC;
    }
    
    if (score >= starThreshold) {
        DLog(@"Score: %d; Star Threshold: %d", score, starThreshold);
        if (type != STAR) {
            [self.arsenal createBomb:STAR random:YES];
        }
        starThreshold += STAR_THRESHHOLD_INC;
    }
}

// -----------------------------------------------------------------------

#pragma mark - Shot timer logic

- (void) resetShotTimer {
    DLog(@"Reset shot timer...");
    [self removeChildByName:@"c-hour" cleanup:YES];
    [self flashMessage:@"Slowed!"];
}

- (void) animateTimerClock {
    
    [Sounds resetShotClock];
    
    CCSprite *clock    = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"clock-hour.png"]];
    clock.anchorPoint  = ccp(.5,.5);
    clock.opacity      = .03;
    clock.position     = ccp(self.contentSize.width / 2, self.contentSize.height - 150);
    
    CCSprite *second   = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"clock-second.png"]];
    second.anchorPoint = ccp(.5,.5);
    second.opacity     = .3;
    second.position    = ccp(clock.contentSize.width / 2, clock.contentSize.height / 2);
    
    [self addChild:clock z:eModalLayer name:@"c-hour"];
    [clock addChild:second z:eModalLayer name:@"c-second"];
    
    CCActionCallFunc *func   = [CCActionCallFunc actionWithTarget:self selector:@selector(resetShotTimer)];
    CCActionRotateBy *rotate = [CCActionRotateBy actionWithDuration:.6 angle:-360];
    
    [clock runAction:[CCActionFadeOut actionWithDuration:.5]];
    [second runAction:[CCActionSequence actions:rotate, func, nil]];
    
    shotTimerElapsed = 0;
    timerIndicator.percentage = 0;
    timeLimit = INITIAL_SHOTTIME_LIMIT - 1;
}

// -----------------------------------------------------------------------

#pragma mark - Flasher

- (void) flashMessage:(NSString *)message {
    
    CCLabelBMFont *flash = [CCLabelBMFont labelWithString:message fntFile:@"staxfonts-flash.fnt"];
    flash.anchorPoint = ccp(.5,.5);
    flash.opacity = 0;
    flash.scale = 4;
    flash.rotation = arc4random_uniform(10) % 2 ? -5 : 5;
    flash.position = ccp(self.contentSize.width / 2, IS_IPHONE5X ? 460 : 400);
    [self addChild:flash z:eCommandLayer name:@"message-flasher"];
    
    CCActionFadeOut  *fadeOut = [CCActionFadeOut actionWithDuration:.5];
    CCActionDelay    *delay   = [CCActionDelay actionWithDuration:.5];
    CCActionFadeIn   *fadeIn  = [CCActionFadeIn actionWithDuration:.05];
    CCActionCallFunc *func    = [CCActionCallFunc actionWithTarget:self selector:@selector(removeFlasher)];
    
    @synchronized(self) {
        [flash runAction:[CCActionSequence actions:fadeIn, delay, fadeOut, func, nil]];
    }
}

- (void) removeFlasher {
    [self removeChildByName:@"message-flasher"];
    DLog(@"Removed flasher...");
}


// -----------------------------------------------------------------------

#pragma mark - Touch handlers

- (void) touchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint p = [touch locationInNode:self];
    
    if (self.arsenal && self.arsenal.active && currentBomb == nil) {
        currentBomb = [self.arsenal activateBomb:p];
        return;
    }
    
    if (self->pauseTouch) {
        return;
    }
    
    curTime = [NSDate date];
    startTouch = p;
}

- (void) touchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint p = [touch locationInNode:self];
    
    if (self.arsenal && self.arsenal.active && currentBomb) {
        [currentBomb moveToPosition:p];
        return;
    }

    if (self->pauseTouch) {
        return;
    }
    
    if (currentBlock && p.y - startTouch.y < 100) {
        [currentBlock moveTo:ccp([self.grid dropXPos:p], p.y)];
    }
}

- (void) touchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint p = [touch locationInNode:self];
    
    if (self.arsenal && self.arsenal.active && currentBomb) {
        [self pauseTouch];
        timerIndicator.percentage = 0.0;
        [currentBomb explodeInScene:self];
        currentBomb = nil;
        return;
    }
    
    if (self->pauseTouch) {
        return;
    }
    
    if (currentBlock) {
        float endTime = [curTime timeIntervalSinceNow] * -1000;
        if (startTouch.y - p.y > 80.0 && endTime < 180.0) {
            [self swipeDown]; // Show arsenal gesture
        } else if (endTime < 120.0) {
            if ([self pauseTouch]) {
                [currentBlock rotate];
            }
        } else {
            if (p.y - startTouch.y > 80.0) {
                if ([self pauseTouch]) {
                    [self executeShot];
                }
            }
        }
    }
}

// -----------------------------------------------------------------------

#pragma mark - Gesture handlers

// - (void) swipedUp {
//     if (currentBlock && [self pauseTouch]) {
//         [self executeShot];
//     }
// }

- (void) swipeDown {
    DLog(@"Bomb count: %d, pause: %d", [self.arsenal bombCount], pauseTouch);
    if (!pauseTouch) {
        [self.arsenal showArsenal];
    }
}

// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
    DLog(@"Dealloc'ed GameBoardScene");
}

@end
