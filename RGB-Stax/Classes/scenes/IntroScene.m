//
//  IntroScene.m
//  RGB-Stax
//
//  Created by Allan De Leon on 5/20/14.
//  Copyright Allan De Leon 2014. All rights reserved.
//
// -----------------------------------------------------------------------

#import "CCTextureCache.h"
#import "IntroScene.h"
#import "Tutorial.h"
#import "OptionsPanel.h"
#import "GameBoardScene.h"
#import "../logic/GameState.h"
#import "../logic/Sounds.h"
#import "../logic/GameKitHelper.h"

#pragma mark - IntroScene

extern BOOL IS_IPHONE5X;

BOOL     HAS_RAN        = NO;
BOOL     QUICK_INTO_RAN = NO;
BOOL     DARK_THEME;

NSString *THEME;

static NSString *themeLoaded = nil;

@implementation IntroScene {
    
    OptionsPanel   *optionsPanel;
    
    CCSprite *background;
    
    int      currentPageNo;
    float    touchMovedX;
    CGPoint  startTouch;
    CCSprite *currentPage;
    CCSprite *prevPage;
    CCSprite *nextPage;
    NSArray  *introPages;
    NSMutableArray *dots;
}

// -----------------------------------------------------------------------

#pragma mark - Constructor/Initialization

+ (IntroScene *) scene {
	return [[self alloc] init];
}

- (id) init {
    
    if (!(self = [super init])) {
        return nil;
    }
    
	return self;
}

- (void) preloadTextureAtlas {
    
    if (!themeLoaded) {
        if (![[GameState sharedState] getBooleanFor:@"flat-theme" withDefault:YES]) {
            THEME = themeLoaded = @"stax-sprites.pvr.ccz";
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"stax-sprites.plist"];
            DARK_THEME = YES;
        } else {
            [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"stax-sprites-flat.plist"];
            THEME = themeLoaded = @"stax-sprites-flat.pvr.ccz";
            DARK_THEME = NO;
        }
    }
    
    [self addChild:[CCSpriteBatchNode batchNodeWithFile:themeLoaded]];
}

// -----------------------------------------------------------------------

#pragma mark - Draw scene

- (void) drawScene {
    
    [self preloadTextureAtlas];
    
    background = [CCNodeColor nodeWithColor:[CCColor colorWithRed:.92 green:.92 blue:.92]];
    background.anchorPoint = ccp(0,0);
    background.position = ccp(0,0);
    [self addChild:background z:eBackgroundLayer name:@"bg"];
    
    [self animateScene];
}

- (void) animateScene {
    
    NSArray *boxes = @[[CCSprite spriteWithImageNamed:@"rbox.png"],[CCSprite spriteWithImageNamed:@"gbox.png"],[CCSprite spriteWithImageNamed:@"bbox.png"]];
    
    BOOL hasSavedGame = [[GameState sharedState] savedGame];
    
    CCButton *helpButton = [CCButton buttonWithTitle:@"" spriteFrame:[CCSpriteFrame frameWithImageNamed:@"question-mark.png"]];
    helpButton.position = ccp(30,30);
    helpButton.visible = NO;
    [helpButton setTarget:self selector:@selector(showTutorial)];
    [self addChild:helpButton z:eBackgroundLayer name:@"help"];
    
    CCSprite *url = [CCSprite spriteWithSpriteFrame:[CCSpriteFrame frameWithImageNamed:@"softlycoded.png"]];
    url.anchorPoint = ccp(1,0);
    url.position = ccp(self.contentSize.width - 13, 9);
    url.visible = NO;
    [self addChild:url z:eCommandLayer name:@"url"];
    
    CCButton *optionsButton = [CCButton buttonWithTitle:@"Settings" fontName:@"cyberspace.ttf" fontSize:28];
    optionsButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
    optionsButton.positionType = CCPositionTypeNormalized;
    optionsButton.position = ccp(0.5f, 0.38);
    optionsButton.visible = NO;
    optionsButton.scale = .55;
    [optionsButton setTarget:self selector:@selector(showOptions)];
    [self addChild:optionsButton z:eModalLayer name:@"options-btn"];
    
    CCButton *playButton = [CCButton buttonWithTitle:@"NEW GAME" fontName:@"cyberspace.ttf" fontSize:28];
    playButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
    playButton.positionType = CCPositionTypeNormalized;
    playButton.position = ccp(0.5f, hasSavedGame ? 0.18f : 0.28f);
    playButton.visible = NO;
    playButton.scale = .55;
    [playButton setTarget:self selector:@selector(onStartGameClicked:)];
    [self addChild:playButton z:eModalLayer name:@"play-btn"];
    
    CCButton *continueButton = nil;
    if (hasSavedGame) {
        continueButton = [CCButton buttonWithTitle:@"Continue Saved Game" fontName:@"cyberspace.ttf" fontSize:28];
        continueButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
        continueButton.positionType = CCPositionTypeNormalized;
        continueButton.position = ccp(0.5f, 0.28f);
        continueButton.scaleY = .58;
        continueButton.scaleX = .45;
        continueButton.visible = NO;
        [continueButton setTarget:self selector:@selector(onContinueGameClicked:)];
        [self addChild:continueButton z:eModalLayer name:@"continue-btn"];
    }
    
    CCLabelTTF *stax = [CCLabelTTF labelWithString:@"Stax" fontName:@"cyberspace.ttf" fontSize:18];
    stax.color = [CCColor colorWithRed:.75 green:.75 blue:.9];
    stax.anchorPoint = ccp(.5,.5);
    stax.scaleX = 2;
    [self addChild:stax z:eCommandLayer name:@"stax-display"];
    
    // Init box positions...
    CCSprite *rbox = [boxes objectAtIndex:0];
    CCSprite *gbox = [boxes objectAtIndex:1];
    CCSprite *bbox = [boxes objectAtIndex:2];
    
    if (!HAS_RAN) {
        gbox.position  = ccp(self.contentSize.width / 2, self.contentSize.height / 2 + 60);
        rbox.position  = ccp(gbox.position.x - 60, self.contentSize.height / 2 + 56);
        bbox.position  = ccp(gbox.position.x + 60, self.contentSize.height / 2 + 55);
        rbox.rotation = -20;
        gbox.rotation = 10;
        bbox.rotation = -10;
        stax.position = ccp(gbox.position.x, gbox.position.y - 58);
    }
    
    [self addChild:rbox];
    [self addChild:gbox];
    [self addChild:bbox];
    
    CCActionDelay    *delay1 = [CCActionDelay actionWithDuration:HAS_RAN ? 0 : 2];
    CCActionDelay    *delay2 = [CCActionDelay actionWithDuration:HAS_RAN ? 0 : 1];
    CCActionCallBlock   *ran = [CCActionCallBlock actionWithBlock:^{ if (!HAS_RAN) HAS_RAN = YES; }];
    CCActionCallBlock *block = [CCActionCallBlock actionWithBlock:
                                ^{
                                    stax.opacity = 0;
                                    stax.anchorPoint = ccp(1,.5);
                                    
                                    float inc = ((CCSprite *)[boxes objectAtIndex:0]).contentSize.width + 10;
                                    float startX = 5.0;
                                    float duration = .8;
                                    for (CCSprite *box in boxes) {
                                        box.anchorPoint = ccp(.5,.5);
                                        if (!HAS_RAN) {
                                            [box runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:duration += .5 position:ccp(startX+=inc, IS_IPHONE5X ? 430 : 380)]]];
                                            [box runAction:[CCActionEaseBackInOut actionWithAction:[CCActionRotateTo actionWithDuration:.1 angle:0]]];
                                        } else {
                                            box.position = ccp(startX+=inc, IS_IPHONE5X ? 430 : 380);
                                        }
                                    }
                                }
                                ];
    
    CCActionCallBlock *score = [CCActionCallBlock actionWithBlock:
                                ^{
                                    
                                    CCButton *leaderBoardButton = nil;
                                    if ([[GameKitHelper sharedGameKitHelper] gameCenterEnabled]) {
                                        leaderBoardButton = [CCButton buttonWithTitle:@"Leader BoArd" fontName:@"cyberspace.ttf" fontSize:28];
                                        leaderBoardButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
                                        leaderBoardButton.positionType = CCPositionTypeNormalized;
                                        leaderBoardButton.position = ccp(0.5f, 0.48);
                                        leaderBoardButton.visible = YES;
                                        leaderBoardButton.scale = .55;
                                        [leaderBoardButton setTarget:[GameKitHelper sharedGameKitHelper] selector:@selector(presentLeaderboards)];
                                        [self addChild:leaderBoardButton z:eModalLayer name:@"leaderboard-btn"];
                                    }
                                    
                                    GameState *gameState = [GameState sharedState];
                                    if ([gameState highScore] > 0) {
                                        [self displayHighScore:gameState];
                                    }
                                    
                                    url.visible           = YES;
                                    helpButton.visible    = YES;
                                    optionsButton.visible = YES;
                                    playButton.visible    = YES;
                                    if (continueButton) {
                                        continueButton.visible = YES;
                                    }
                                    
                                }];
    
    [self runAction:[CCActionSequence actions:
                     delay1,
                     block,
                     delay2,
                     score,
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          CCSprite *m = [boxes objectAtIndex:2];
                          stax.position = ccp(m.position.x + m.contentSize.width / 2 + 5, m.position.y - 48);
                          stax.scaleX = 3.3;
                      }],
                     ran,
                     [CCActionCallBlock actionWithBlock:^{ [self animateStax]; }],
                     nil
                     ]];
}

- (void) animateStax {
    
    CCLabelTTF *stax = (CCLabelTTF *) [self getChildByName:@"stax-display" recursively:NO];
    if (stax) {
        [stax runAction:[CCActionRepeatForever actionWithAction:
                         [CCActionSequence actions:
                          [CCActionFadeIn actionWithDuration:3],
                          [CCActionFadeOut actionWithDuration:5],
                          [CCActionCallBlock actionWithBlock:^{stax.color = [CCColor colorWithRed:.95 green:.3 blue:.3];}],
                          [CCActionFadeIn actionWithDuration:3],
                          [CCActionFadeOut actionWithDuration:5],
                          [CCActionCallBlock actionWithBlock:^{stax.color = [CCColor colorWithRed:.3 green:.75 blue:.3];}],
                          [CCActionFadeIn actionWithDuration:3],
                          [CCActionFadeOut actionWithDuration:5],
                          [CCActionCallBlock actionWithBlock:^{stax.color = [CCColor colorWithRed:.3 green:.3 blue:.95];}],
                          nil]
                         ]];
    }
}

// -----------------------------------------------------------------------

#pragma mark - High score logic

- (void) displayHighScore:(GameState *)gameState {
    
    NSNumberFormatter *formatter = nil;
    [(formatter = [[NSNumberFormatter alloc] init]) setNumberStyle:NSNumberFormatterDecimalStyle];
    
    NSString *label = [NSString stringWithFormat:@"High Score: %@", [formatter stringFromNumber:[NSNumber numberWithInt:[gameState highScore]]]];
    
    CCLabelTTF *highScoreLabel = [CCLabelTTF labelWithString:label fontName:@"cyberspace.ttf" fontSize:22];
    highScoreLabel.position = ccp(self.contentSize.width / 2.0, (self.contentSize.height / 2.0) + 50);
    highScoreLabel.color = [CCColor colorWithRed:.5 green:.5 blue:.97];
    highScoreLabel.scaleX = .5;
    [self addChild:highScoreLabel z:eModalLayer name:@"highscore-lbl"];
}

- (void) setLabel:(NSString *)label forButton:(CCButton *)btn {
    CCLabelBMFont *text = [CCLabelBMFont labelWithString:label fntFile:@"staxfonts-clear.fnt"];
    text.anchorPoint = ccp(.5,.5);
    text.position = ccp(btn.contentSize.width / 2, btn.contentSize.height / 2);
    if ([label length] >10) {
        text.scaleX = .5;
    }
    [btn addChild:text];
}

// -----------------------------------------------------------------------

#pragma mark - Start game sequence

- (void) onStartGameClicked:(id)sender {
    
    BOOL hasSavedGame = [[GameState sharedState] savedGame];
    
    if (hasSavedGame) {
        [self confirmStart];
    } else {
        [self newGame];
    }
}

- (void) confirmStart {
    
    CCNode *confirmPanel = [CCNodeColor nodeWithColor:[CCColor colorWithRed:0 green:0 blue:0] width:self.contentSize.width height:self.contentSize.height];
    confirmPanel.anchorPoint = ccp(0,0);
    confirmPanel.position = ccp(self.contentSize.width, 0);
    
    CCLabelTTF *msg1 = [CCLabelTTF labelWithString:@"You have a saved Game" fontName:@"cyberspace.ttf" fontSize:16];
    msg1.anchorPoint = ccp(.5,.5);
    msg1.color = [CCColor whiteColor];
    msg1.scaleX = .95;
    msg1.scaleY = 1.2;
    msg1.position = ccp(confirmPanel.contentSize.width / 2, confirmPanel.contentSize.height / 2 + 130);
    [confirmPanel addChild:msg1];
    
    CCLabelTTF *msg2 = [CCLabelTTF labelWithString:@"Abandon saved Game?" fontName:@"cyberspace.ttf" fontSize:14];
    msg2.anchorPoint = ccp(.5,.5);
    msg2.color = [CCColor whiteColor];
    msg2.scaleY = 1.1;
    msg2.position = ccp(confirmPanel.contentSize.width / 2, confirmPanel.contentSize.height / 2 + 90);
    [confirmPanel addChild:msg2];
    
    CCButton *cancelButton = [CCButton buttonWithTitle:@"Cancel" fontName:@"cyberspace.ttf" fontSize:16];
    cancelButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
    cancelButton.anchorPoint = ccp(.5,.5);
    cancelButton.position = ccp(self.contentSize.width / 2, self.contentSize.height / 2 - 80);
    [cancelButton setTarget:self selector:@selector(cancelConfirm)];
    [confirmPanel addChild:cancelButton z:eModalLayer];
    
    CCButton *confirmButton = [CCButton buttonWithTitle:@"Continue" fontName:@"cyberspace.ttf" fontSize:16];
    confirmButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
    confirmButton.anchorPoint = ccp(.5,.5);
    confirmButton.position = ccp(self.contentSize.width / 2, self.contentSize.height / 2 - 140);
    [confirmButton setTarget:self selector:@selector(newGame)];
    [confirmPanel addChild:confirmButton z:eModalLayer];
    
    [Sounds panelOpen];
    [self addChild:confirmPanel z:eModalLayer name:@"confirm-panel"];
    [self runAction:[CCActionMoveTo actionWithDuration:.1 position:ccp(-self.contentSize.width,0)]];
    
}

- (void) newGame {
    
    [self runAction:[CCActionSequence actions:
                     [CCActionMoveTo actionWithDuration:.2 position:ccp(0,0)],
                     [CCActionCallBlock actionWithBlock:^{[self removeChildByName:@"confirm-panel" cleanup:YES];}],
                     nil]];
    
    [[GameState sharedState] removeSavedGame];
    
    [self getChildByName:@"options-btn" recursively:YES].visible = NO;
    [self getChildByName:@"play-btn" recursively:YES].visible = NO;
    if ([[GameState sharedState] savedGame]) {
        [self getChildByName:@"continue-btn" recursively:YES].visible = NO;
    }
    [[GameState sharedState] removeSavedGame];
    [self doStartGame];
}

- (void) cancelConfirm {
    
    [Sounds panelClose];
    [self runAction:[CCActionMoveTo actionWithDuration:.1 position:ccp(0,0)]];
    [self removeChildByName:@"confirm-panel" cleanup:YES];
    
}

- (void) onContinueGameClicked:(id)sender {
    [self getChildByName:@"options-btn" recursively:YES].visible = NO;
    [self getChildByName:@"continue-btn" recursively:YES].visible = NO;
    [self getChildByName:@"play-btn" recursively:YES].visible = NO;
    [[GameState sharedState] setContinueSavedGame:YES];
    [self doStartGame];
}

- (void) doStartGame {
    [Sounds stopBackgroundMusic];
    CCActionCallFunc  *func  = [CCActionCallFunc actionWithTarget:self selector:@selector(startGame)];
    CCActionCallBlock *sound = [CCActionCallBlock actionWithBlock:^{ [Sounds startGame]; }];
    [self runAction:[CCActionSequence actions:sound, func, nil]];
}

- (void) startGame {
    optionsPanel = nil;
    [[CCDirector sharedDirector] replaceScene:[GameBoardScene scene] withTransition:[CCTransition transitionFadeWithDuration:1.0f]];
}

// -----------------------------------------------------------------------

#pragma mark - Options

- (void) showOptions {
    
    if (!optionsPanel) {
        optionsPanel = [[OptionsPanel alloc] initWithParent:self];
    }
    
    [optionsPanel show];
}

// -----------------------------------------------------------------------

#pragma mark - Handlers

- (void) onEnter {
    [super onEnter];
    DLog(@"Trying to login to GameCenter...");
    [[GameKitHelper sharedGameKitHelper] authenticateLocalPlayer];
}

- (void) onEnterTransitionDidFinish {
    
    [super onEnterTransitionDidFinish];
    [Sounds updateSoundOptions];
    [Sounds introMusic];
    
    if (!QUICK_INTO_RAN && ![[GameState sharedState] getBooleanFor:@"RUNQUICKINTRO" withDefault:NO]) {
        [[GameState sharedState] setBoolean:YES for:@"RUNQUICKINTRO"];
        [self startQuickIntro];
    } else {
        [self drawScene];
    }
    
    QUICK_INTO_RAN = YES;
}

- (void) onExitTransitionDidStart {
    [super onExitTransitionDidStart];
}

- (void) showTutorial {
    optionsPanel = nil;
    [[CCDirector sharedDirector] replaceScene:[Tutorial scene] withTransition:[CCTransition transitionCrossFadeWithDuration:1.2]];
}

// -----------------------------------------------------------------------

#pragma mark - Quick intro

- (void) transitionToIntro {
    
    NSArray *textures = @[@"intro-1.png", @"intro-2.png", @"intro-3.png", @"intro-4.png"];
    
    for (int i = 0; i < [introPages count]; ++i) {
        CCSprite *sprite = [introPages objectAtIndex:i];
        [self removeChild:sprite cleanup:YES];
        [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFrameByName:[textures objectAtIndex:i]];
        [[CCTextureCache sharedTextureCache] removeTextureForKey:[textures objectAtIndex:i]];
    }
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFrameByName:@"dot.png"];
    [[CCTextureCache sharedTextureCache] removeTextureForKey:@"dot.png"];
    
    [self removeChildByName:@"credits-openbtn" cleanup:YES];
    
    dots        = nil;
    introPages  = nil;
    prevPage    = nil;
    nextPage    = nil;
    currentPage = nil;
    
    [self setUserInteractionEnabled:NO];
    [self drawScene];
}

- (void) startQuickIntro {
    
    [self setUserInteractionEnabled:YES];
    
    NSArray *textures = @[@"intro-1.png", @"intro-2.png", @"intro-3.png", @"intro-4.png"];
    
    CCSprite *page1 = [CCSprite spriteWithImageNamed:@"intro-1.png"];
    CCSprite *page2 = [CCSprite spriteWithImageNamed:@"intro-2.png"];
    CCSprite *page3 = [CCSprite spriteWithImageNamed:@"intro-3.png"];
    CCSprite *page4 = [CCSprite spriteWithImageNamed:@"intro-4.png"];
    
    currentPageNo = 0;
    dots = [[NSMutableArray alloc] init];
    introPages = @[page1,page2,page3,page4];
    
    float dotX = self.contentSize.width / 2 - 32;
    for (int i = 0; i < [introPages count]; ++i) {
        CCSprite *page = [introPages objectAtIndex:i];
        page.anchorPoint = ccp(0,0);
        page.position = ccp(i == 0 ? 0 : self.contentSize.width, 0);
        [self addChild:page z:eGamePieceLayer name:[textures objectAtIndex:i]];
        
        CCSprite *dot = [CCSprite spriteWithImageNamed:@"dot.png"];
        dot.anchorPoint = ccp(.5,.5);
        dot.position = ccp(dotX, IS_IPHONE5X ? 105 : 38);
        dot.opacity = i == 0 ? 1 : .25;
        [dots addObject:dot];
        [self addChild:dot z:eCommandLayer name:[NSString stringWithFormat:@"dot%@", [textures objectAtIndex:i]]];
        dotX += 20;
    }
    
    prevPage    = nil;
    currentPage = [introPages objectAtIndex:0];
    nextPage    = [introPages objectAtIndex:1];
}

- (void) prevPage {
    
    if (prevPage) {
        [Sounds panelOpen];
        [currentPage runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(self.contentSize.width, 0)]];
        [prevPage runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(0,0)]];
        prevPage    = currentPageNo >= 2 ? [introPages objectAtIndex:currentPageNo - 2] : nil;
        currentPage = currentPageNo >= 1 ? [introPages objectAtIndex:currentPageNo - 1] : currentPage;
        nextPage    = currentPageNo >= 1 ? [introPages objectAtIndex:currentPageNo] : nextPage;
        if (currentPageNo > 0) {
            --currentPageNo;
        }
    } else {
        [currentPage runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(0, 0)]];
    }
    
    [self setDot];
}

- (void) nextPage {
    
    int count = (unsigned) [introPages count];
    
    if (nextPage) {
        [Sounds panelOpen];
        [currentPage runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(-self.contentSize.width,0)]];
        [nextPage runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(0,0)]];
        prevPage    = currentPageNo <= count - 2 ? [introPages objectAtIndex:currentPageNo] : prevPage;
        currentPage = currentPageNo <= count - 1 ? [introPages objectAtIndex:currentPageNo + 1] : currentPage;
        nextPage    = currentPageNo <= count - 3 ? [introPages objectAtIndex:currentPageNo + 2] : nil;
        if (currentPageNo < count) {
            ++currentPageNo;
        }
    } else {
        [self removeDots];
        [Sounds panelClose];
        CCActionMoveTo *move = [CCActionMoveTo actionWithDuration:.2 position:ccp(-self.contentSize.width,0)];
        CCActionCallBlock *trans = [CCActionCallBlock actionWithBlock:^{ [self transitionToIntro]; }];
        [currentPage runAction:[CCActionSequence actions:move, trans, nil]];
        return;
    }
    
    [self setDot];
}

- (void) removeDots {
    for(CCSprite *dot in dots) {
        [self removeChild:dot cleanup:YES];
    }
}

- (void) setDot {
    for (int i = 0; i < [dots count]; ++i) {
        CCSprite *dot = [dots objectAtIndex:i];
        dot.opacity = i == currentPageNo ? 1 : .25;
    }
}

// -----------------------------------------------------------------------

#pragma mark - Touch handlers

- (void) touchBegan:(UITouch *)touch withEvent:(UIEvent *)event {
    startTouch = [touch locationInNode:self];
}

- (void) touchMoved:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint p = [touch locationInNode:self];
    if (currentPage) {
        currentPage.position = ccp(-(startTouch.x - p.x), 0);
        if (nextPage) {
            nextPage.position = ccp(self.contentSize.width - (startTouch.x - p.x), 0);
        }
        if (prevPage) {
            prevPage.position = ccp(-self.contentSize.width - (startTouch.x - p.x), 0);
        }
        touchMovedX = p.x;
    }
}

- (void) touchEnded:(UITouch *)touch withEvent:(UIEvent *)event {
    
    CGPoint p = [touch locationInNode:self];
    
    if (p.x < startTouch.x && p.x <= touchMovedX) {
        [self nextPage];
    } else if (p.x > startTouch.x && p.x >= touchMovedX){
        [self prevPage];
    } else {
        [prevPage runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(-self.contentSize.width,0)]];
        [currentPage runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(0,0)]];
        [nextPage runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(self.contentSize.width,0)]];
    }
}

// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
    DLog(@"Dealloc'ed IntroScene");
}

@end