//
//  Tutorial.m
//  RGB-Stax
//
//  Created by Allan De Leon on 8/10/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "CCTextureCache.h"
#import "Tutorial.h"
#import "IntroScene.h"
#import "../logic/Sounds.h"
#import "../logic/GameState.h"


extern BOOL IS_IPHONE5X;
extern BOOL HAS_RAN;

extern int GAMEBOARD_COLUMNS;
extern int GAMEPIECE_SIZE;
extern float INITIAL_SHOTTIME_LIMIT;

const float LEFT_MAGIN       = 20.0;
const float VERTICAL_SPACING = 25.0;
const float SPACE_WIDTH      = 8.0;
const float SWITCH_SPEED     = .2;

const int   NUM_PAGES        = 9;

@implementation Tutorial {
    int            currentPageNumber;
    int            highestPage;
    BOOL           canSwipe;
    CGPoint        middlePos;
    CCNode         *currentPage;
    CCButton       *nextButton;
    CCButton       *prevButton;
    NSMutableArray *pages;
    NSMutableArray *dots;
    NSMutableArray *prevComboArray;
    
    CCSprite       *no;
    CCSprite       *spinner1;
    CCSprite       *spinner2;
    CCSprite       *spinner3;
    
    UISwipeGestureRecognizer *swipeDown;
    UISwipeGestureRecognizer *swipeLeft;
    UISwipeGestureRecognizer *swipeRight;
}

// -----------------------------------------------------------------------

#pragma mark - Constructor/Initialization

+ (Tutorial *) scene {
	return [[self alloc] init];
}

- (id) init {
    
    if (!(self = [super init])) {
        return nil;
    }
    
    pages       = [[NSMutableArray alloc] init];
    highestPage = 0;
    canSwipe    = NO;
    
	return self;
}

- (void) renderScene {
    
    spinner1 = [CCSprite spriteWithImageNamed:@"spinner1.png"];
    spinner1.anchorPoint = ccp(.5,.5);
    spinner1.position = ccp(self.contentSize.width / 2, 55);
    spinner1.opacity = 0;
    spinner1.visible = NO;
    [self addChild:spinner1 z:eCommandLayer name:@"spinner1-stat"];
    
    spinner2 = [CCSprite spriteWithImageNamed:@"spinner2.png"];
    spinner2.anchorPoint = ccp(1.4,.5);
    spinner2.position = ccp(self.contentSize.width / 2, 55);
    spinner2.opacity = 0;
    spinner2.visible = NO;
    [self addChild:spinner2 z:eCommandLayer name:@"spinner2-stat"];
    
    spinner3 = [CCSprite spriteWithImageNamed:@"spinner3.png"];
    spinner3.anchorPoint = ccp(1.4,.5);
    spinner3.position = ccp(self.contentSize.width / 2, 55);
    spinner3.opacity = 0;
    spinner3.visible = NO;
    [self addChild:spinner3 z:eCommandLayer name:@"spinner3-stat"];
    
    [self startSpinners];
    
    no = [CCSprite spriteWithImageNamed:@"no.png"];
    no.anchorPoint = ccp(.5,.5);
    no.position = ccp(self.contentSize.width / 2, 55);
    no.scale = 1.2;
    no.opacity = 0;
    [self addChild:no z:eCommandLayer];
    
    [self page0];
    // [self page8]; // TEST: remove when done!
}

- (void) startSpinners {
    [spinner2 runAction:[CCActionRepeatForever actionWithAction:[CCActionRotateBy actionWithDuration:.05 angle:-14]]];
    [spinner3 runAction:[CCActionRepeatForever actionWithAction:[CCActionRotateBy actionWithDuration:.05 angle:14]]];
}

- (void) initDots {
    
    // This is displayed after the first page is rendered!
    
    if (dots != nil) {
        return;
    }
    
    dots = [[NSMutableArray alloc] init];
    
    float startx = 85;
    for (int i = 0; i < NUM_PAGES; ++i) {
        CCSprite *dot = [CCSprite spriteWithImageNamed:@"dot.png"];
        dot.anchorPoint = ccp(.5,.5);
        dot.opacity = .2;
        dot.position = ccp(startx += 15, 17);
        [self addChild:dot z:eCommandLayer];
        [dots addObject:dot];
    }
    
    ((CCSprite *) [dots objectAtIndex:0]).opacity = .8;
}

- (void) setDot {
    for (int i = 0; i < NUM_PAGES; ++i) {
        CCSprite *dot = [dots objectAtIndex:i];
        dot.opacity = i == currentPageNumber ? .8 : .2;
    }
}

- (void) showSpinners {
    
    spinner1.visible = YES;
    [spinner1 runAction:[CCActionFadeIn actionWithDuration:.05]];
    
    spinner2.visible = YES;
    [spinner2 runAction:[CCActionFadeIn actionWithDuration:.05]];
    
    spinner3.visible = YES;
    [spinner3 runAction:[CCActionFadeIn actionWithDuration:.05]];
}

- (void) hideSpinners {
    [spinner1 runAction:[CCActionSequence actions:[CCActionFadeOut actionWithDuration:.05], [CCActionCallBlock actionWithBlock:^{ spinner1.visible = NO; }], nil]];
    [spinner2 runAction:[CCActionSequence actions:[CCActionFadeOut actionWithDuration:.05], [CCActionCallBlock actionWithBlock:^{ spinner2.visible = NO; }], nil]];
    [spinner3 runAction:[CCActionSequence actions:[CCActionFadeOut actionWithDuration:.05], [CCActionCallBlock actionWithBlock:^{ spinner3.visible = NO; }], nil]];
}

// -----------------------------------------------------------------------

#pragma mark - Page Flip

- (CCNode *) setPageFlip:(int)num {
    
    if (num != 0) {
        [self hidePagePointers];
    }
    
    highestPage = highestPage < num ? num : highestPage;
    
    CCNode *page =  nil;
    if ([pages count] >= num + 1) {
        page = currentPage = [pages objectAtIndex:num];
        [page runAction:[CCActionMoveTo actionWithDuration:SWITCH_SPEED position:ccp(0,0)]];
        [self updatePagePointers:currentPageNumber];
        canSwipe = YES;
        return nil;
    } else {
        page =  currentPage = [CCNodeColor nodeWithColor:[CCColor colorWithRed:.9 green:.9 blue:.9]];
        [pages addObject:page];
        page.anchorPoint = ccp(0,0);
        page.position = ccp(0,0);
        [self addChild: page];
        canSwipe = NO;
        if (currentPageNumber != 0) {
            [self showSpinners];
        }
    }
    
    return page;
}

// -----------------------------------------------------------------------

#pragma mark - Page 0

- (void) page0 {
    
    currentPageNumber = 0;
    CCNode *page =  [self setPageFlip:currentPageNumber];
    if (!page) {
        return;
    }
    
    float top = IS_IPHONE5X ? 418 : 380;
    
    CCLabelTTF *l0  = [self labelWithString:@"Swipe down to exit Tutorial!" fontSize:20 positon:ccp(0,0)];
    CCLabelTTF *l1  = [self labelWithString:@"This" fontSize:18 positon:ccp(LEFT_MAGIN,top)];
    CCLabelTTF *l2  = [self labelWithString:@"is a segment..." fontSize:18 positon:ccp(100,top)];
    CCLabelTTF *l3  = [self labelWithString:@"Segments" fontSize:18 positon:ccp(LEFT_MAGIN,top-=VERTICAL_SPACING)];
    CCLabelTTF *l4  = [self labelWithString:@"can" fontSize:18 positon:ccp(SPACE_WIDTH + l3.position.x + l3.contentSize.width,top)];
    CCLabelTTF *l5  = [self labelWithString:@"be" fontSize:18 positon:ccp(SPACE_WIDTH + l4.position.x + l4.contentSize.width,top)];
    CCLabelTTF *l6  = [self labelWithString:@"one of" fontSize:18 positon:ccp(SPACE_WIDTH + l5.position.x + l5.contentSize.width,top)];
    CCLabelTTF *l7  = [self labelWithString:@"three" fontSize:24 positon:ccp(140,(top-=VERTICAL_SPACING) - 2) color:[CCColor blueColor]];
    CCLabelTTF *l8  = [self labelWithString:@"colors..." fontSize:18 positon:ccp(SPACE_WIDTH + l7.position.x + l7.contentSize.width,top)];
    
    CCSprite *red  = [CCSprite spriteWithImageNamed:@"red-segment.png"];
    red.anchorPoint = ccp(0,0);
    [page addChild:red z:eGamePieceLayer name:@"red-page1"];
    red.position = ccp(8 + l1.position.x + l1.contentSize.width,600);
    
    [page runAction:[CCActionSequence actions:
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          l0.anchorPoint = ccp(.5,.5);
                          l0.position = ccp (self.contentSize.width/2.0, self.contentSize.height / 2 + 50);
                          [page addChild:l0];
                      }],
                     [CCActionCallBlock actionWithBlock:^{ [l0 runAction:[CCActionFadeOut actionWithDuration:3]]; }],
                     [self delayWithDuration:3],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self showSpinners]; }],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l1]; }],  // This
                     [CCActionCallBlock actionWithBlock:^{ [red runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:1 position:ccp(8 + l1.position.x + l1.contentSize.width, l1.position.y)]]]; }],
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l2]; }],  // is a segment
                     [self delayWithDuration:1.2],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l3]; }],  // Segments
                     [self delayWithDuration:.7],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l4]; }],  // can
                     [self delayWithDuration:.2],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l5]; }],  // be
                     [self delayWithDuration:.2],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l6]; }],  // one of
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l7 z:eGamePieceLayer name:@"three-page1"]; }],  // three
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l8 z:eGamePieceLayer name:@"color-page1"]; }],  // colors
                     [self delayWithDuration:2],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self addRGBStaxLabelTo:page]; }],  // colors
                     
                     nil]];
}

- (void) addRGBStaxLabelTo:(CCNode *)parent {
    
    CCSprite *redMain = (CCSprite *)[parent getChildByName:@"red-page1" recursively:NO];
    CCLabelTTF *three = (CCLabelTTF *)[parent getChildByName:@"three-page1" recursively:NO];
    CCLabelTTF *color = (CCLabelTTF *)[parent getChildByName:@"color-page1" recursively:NO];
    
    float leftPos     = redMain.position.x;
    float segPos      = leftPos + 50;
    
    float yPos        = IS_IPHONE5X ? 490 : 430;
    
    CCLabelTTF *R     = [self labelWithString:@"R" fontSize:34 positon:ccp(80, yPos) color:[CCColor redColor]];
    CCLabelTTF *G     = [self labelWithString:@"G" fontSize:34 positon:ccp(R.position.x + R.contentSize.width + SPACE_WIDTH, yPos) color:[CCColor colorWithRed:0 green:.7 blue:0]];
    CCLabelTTF *B     = [self labelWithString:@"B" fontSize:34 positon:ccp(G.position.x + G.contentSize.width + SPACE_WIDTH, yPos) color:[CCColor blueColor]];
    CCLabelTTF *R1    = [self labelWithString:@"Red" fontSize:34 positon:ccp(80, 500) color:[CCColor redColor]];
    CCLabelTTF *G1    = [self labelWithString:@"Green" fontSize:34 positon:ccp(R.position.x + R.contentSize.width + SPACE_WIDTH, yPos) color:[CCColor colorWithRed:0 green:.7 blue:0]];
    CCLabelTTF *B1    = [self labelWithString:@"Blue" fontSize:34 positon:ccp(G.position.x + G.contentSize.width + SPACE_WIDTH, yPos) color:[CCColor blueColor]];
    CCLabelTTF *Stax  = [self labelWithString:@"Stax" fontSize:34 positon:ccp(B.position.x + B.contentSize.width + SPACE_WIDTH, yPos)];
    
    yPos = IS_IPHONE5X ? 185 : 150;
    
    CCLabelTTF *l1    = [self labelWithString:@"No, wait..." fontSize:24 positon:ccp(LEFT_MAGIN,yPos) color:[CCColor redColor]];
    CCLabelTTF *l2    = [self labelWithString:@"there is a fourth segment..." fontSize:18 positon:ccp(LEFT_MAGIN + 40,l1.position.y - VERTICAL_SPACING)];
    
    yPos = IS_IPHONE5X ? 72 : 50;
    
    CCLabelTTF *l3    = [self labelWithString:@"Black is special..." fontSize:16 positon:ccp(LEFT_MAGIN,yPos) color:[CCColor redColor]];
    CCLabelTTF *l4    = [self labelWithString:@"it's rare..." fontSize:16 positon:ccp(SPACE_WIDTH + l3.position.x + l3.contentSize.width, l3.position.y) color:[CCColor redColor]];
    
    float blackYPos = IS_IPHONE5X ? 110 : 90;
    
    CCLabelTTF *l5    = [self labelWithString:@"Black" fontSize:24 positon:ccp(segPos, blackYPos) color:[CCColor blackColor]];
    
    R.visible  = NO;
    G.visible  = NO;
    B.visible  = NO;
    R1.visible = NO;
    G1.visible = NO;
    B1.visible = NO;
    
    [parent addChild:R];
    [parent addChild:G];
    [parent addChild:B];
    [parent addChild:R1];
    [parent addChild:G1];
    [parent addChild:B1];
    [parent addChild:Stax];
    
    CCSprite *red  = [CCSprite spriteWithImageNamed:@"red-segment.png"];
    red.anchorPoint = ccp(0,0);
    [parent addChild:red z:eGamePieceLayer name:@"red"];
    red.position = ccp(leftPos,-64);
    
    CCSprite *green  = [CCSprite spriteWithImageNamed:@"green-segment.png"];
    green.anchorPoint = ccp(0,0);
    [parent addChild:green z:eGamePieceLayer name:@"green"];
    green.position = ccp(leftPos,-64);
    
    CCSprite *blue  = [CCSprite spriteWithImageNamed:@"blue-segment.png"];
    blue.anchorPoint = ccp(0,0);
    [parent addChild:blue z:eGamePieceLayer name:@"blue"];
    blue.position = ccp(leftPos,-64);
    
    CCSprite *black  = [CCSprite spriteWithImageNamed:@"black-segment.png"];
    black.anchorPoint = ccp(0,0);
    [parent addChild:black z:eGamePieceLayer name:@"black-segment"];
    black.position = ccp(leftPos,-64);
    
    yPos = IS_IPHONE5X ? 315 : 290;
    
    [parent runAction:[CCActionSequence actions:
                       
                       [CCActionCallBlock actionWithBlock:^{R.visible = YES; G.visible = YES; B.visible = YES; }],
                       [self delayWithDuration:1.2],
                       
                       [CCActionCallBlock actionWithBlock:^{R1.visible = YES; R1.scale = .7; [R1 runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:1 position:ccp(segPos,yPos)]]];}],
                       [self delayWithDuration:.2],
                       [CCActionCallBlock actionWithBlock:^{[red runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:1 position:ccp(leftPos,yPos)]]];}],
                       [self delayWithDuration:.4],
                       
                       [CCActionCallBlock actionWithBlock:^{G1.visible = YES; G1.scale = .7; [G1 runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:1 position:ccp(segPos,yPos-40)]]];}],
                       [self delayWithDuration:.2],
                       [CCActionCallBlock actionWithBlock:^{[green runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:1 position:ccp(leftPos,yPos-40)]]];}],
                       [self delayWithDuration:.4],
                       
                       [CCActionCallBlock actionWithBlock:^{B1.visible = YES; B1.scale = .7; [B1 runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:1 position:ccp(segPos,yPos-80)]]];}],
                       [self delayWithDuration:.2],
                       [CCActionCallBlock actionWithBlock:^{[blue runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:1 position:ccp(leftPos,yPos-80)]]];}],
                       [self delayWithDuration:1.4],
                       
                       [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [parent addChild:l1]; }],  // No, wait...
                       [self delayWithDuration:1],
                       
                       [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [parent addChild:l2]; }],  // there is a fourth segment...
                       [CCActionCallBlock actionWithBlock:^{ [three runAction:[CCActionSequence actions:[CCActionFadeOut actionWithDuration:1],
                                                                               [CCActionCallBlock actionWithBlock:^{ [three setString:@"four"];}],
                                                                               [CCActionCallBlock actionWithBlock:^{ [color runAction:[CCActionMoveTo actionWithDuration:.5 position:ccp(SPACE_WIDTH + three.position.x + three.contentSize.width, color.position.y)]]; }],
                                                                               [CCActionFadeIn actionWithDuration:1], nil]
                                                              ];}],
                       [self delayWithDuration:1],
                       
                       [CCActionCallBlock actionWithBlock:^{[black runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:1 position:ccp(leftPos,blackYPos)]]];}],
                       [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [parent addChild:l5]; }], // can
                       [self delayWithDuration:1.8],
                       
                       [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [parent addChild:l3 z:eGamePieceLayer]; }],  // Black is special...
                       [self delayWithDuration:1.2],
                       
                       [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [parent addChild:l4 z:eGamePieceLayer]; }],  // It's rare...
                       [CCActionCallBlock actionWithBlock:^{ [self initDots]; [self updatePagePointers:currentPageNumber]; }],
                       [self delayWithDuration:2],
                       
                       [CCActionCallBlock actionWithBlock:
                        ^{
                            [l4 runAction:[CCActionSequence actions:
                                           [CCActionFadeOut actionWithDuration:1],
                                           [CCActionCallBlock actionWithBlock:^{ l4.string = @"it hinders..."; }],
                                           [CCActionFadeIn actionWithDuration:2], nil]];
                        }],  // It's rare...
                       
                       [CCActionCallBlock actionWithBlock:^{middlePos = green.position;}],
                       
                       nil
                       ]];
}

// -----------------------------------------------------------------------

#pragma mark - Page 1

- (void) page1 {
    
    currentPageNumber = 1;
    CCNode *page =  [self setPageFlip:currentPageNumber];
    if (!page) {
        return;
    }
    
    float top = IS_IPHONE5X ? 410 : 350;
    
    CCLabelTTF *l1   = [self labelWithString:@"Three segments..." fontSize:18 positon:ccp(LEFT_MAGIN,top)];
    CCLabelTTF *l2   = [self labelWithString:@"make a" fontSize:18 positon:ccp(LEFT_MAGIN + 120, top-=VERTICAL_SPACING)];
    CCLabelTTF *l3   = [self labelWithString:@"Block" fontSize:22 positon:ccp(l2.position.x + l2.contentSize.width + SPACE_WIDTH, top) color:[CCColor blueColor]];
    CCLabelTTF *l4   = [self labelWithString:@"Tap anywhere to rotate a block..." fontSize:16 positon:ccp(LEFT_MAGIN,top-=VERTICAL_SPACING)];
    CCLabelTTF *l5   = [self labelWithString:@"Drag left to move left..." fontSize:16 positon:ccp(LEFT_MAGIN,top-=VERTICAL_SPACING)];
    CCLabelTTF *l6   = [self labelWithString:@"Drag right to move right..." fontSize:16 positon:ccp(LEFT_MAGIN,top-=VERTICAL_SPACING)];
    CCLabelTTF *l7   = [self labelWithString:@"Swipe straight up to shoot into the grid..." fontSize:16 positon:ccp(LEFT_MAGIN,top-=VERTICAL_SPACING)];
    CCLabelTTF *l8   = [self labelWithString:@"You have" fontSize:16 positon:ccp(LEFT_MAGIN,top-=VERTICAL_SPACING)];
    CCLabelTTF *l9   = [self labelWithString:[NSString stringWithFormat:@"%.0f seconds", INITIAL_SHOTTIME_LIMIT] fontSize:16 positon:ccp(l8.position.x + l8.contentSize.width + SPACE_WIDTH,top) color:[CCColor redColor]];
    CCLabelTTF *l10  = [self labelWithString:@"to rotate, move, and shoot!" fontSize:16 positon:ccp(LEFT_MAGIN + 40,top-=VERTICAL_SPACING)];
    CCLabelTTF *l11  = [self labelWithString:@"But it gets faster!" fontSize:16 positon:ccp(LEFT_MAGIN,top-=VERTICAL_SPACING)];
    CCLabelTTF *l12  = [self labelWithString:@"Much faster!" fontSize:20 positon:ccp(LEFT_MAGIN + 140,top-=VERTICAL_SPACING * 2) color:[CCColor redColor]];
    CCLabelTTF *l13  = [self labelWithString:@"----------" fontSize:22 positon:ccp(l9.position.x,l9.position.y - 3) color:[CCColor redColor]];
    
    NSArray *move = @[l1,l2,l3,l4,l5,l6,l7];
    
    float initX = self.contentSize.width / 2.0 - GAMEPIECE_SIZE / 2.0;
    float initY = IS_IPHONE5X ? 200 : 170;
    
    CCSprite *red  = [CCSprite spriteWithImageNamed:@"red-segment.png"];
    red.anchorPoint = ccp(.5,.5);
    [page addChild:red z:eGamePieceLayer name:@"red-p1"];
    red.position = ccp(initX,initY);
    red.scale = 0;
    
    CCSprite *green  = [CCSprite spriteWithImageNamed:@"green-segment.png"];
    green.anchorPoint = ccp(.5,.5);
    [page addChild:green z:eGamePieceLayer name:@"green-p1"];
    green.position = ccp(initX+=green.contentSize.width,initY);
    green.scale = 0;
    
    CCSprite *blue  = [CCSprite spriteWithImageNamed:@"blue-segment.png"];
    blue.anchorPoint = ccp(.5,.5);
    [page addChild:blue z:eGamePieceLayer name:@"blue-p1"];
    blue.position = ccp(initX+=blue.contentSize.width,initY);
    blue.scale = 0;
    
    CCSprite *tap  = [CCSprite spriteWithImageNamed:@"tap.png"];
    tap.anchorPoint = ccp(.5,.5);
    [page addChild:tap z:eGamePieceLayer name:@"tap-p1"];
    tap.position = ccp(260, 290);
    tap.visible = NO;
    
    [page runAction:[CCActionSequence actions:
                     
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l1]; }],  // Three segments...
                     [self delayWithDuration:.2],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self showBlockRed:red green:green blue:blue]; }],
                     [self delayWithDuration:1.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l2]; }],  // make a
                     [self delayWithDuration:.1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l3]; }],  // Block
                     [self delayWithDuration:1.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l4]; }],  // Tap anywhere...
                     [self delayWithDuration:.3],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self rotateBlockRed:red green:green blue:blue tap:tap]; }],
                     [self delayWithDuration:3.6],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l5]; }],  // Drag left
                     [self delayWithDuration:.6],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self dragLeftRed:red green:green blue:blue tap:tap]; }],
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l6]; }],  // Drag right
                     [self delayWithDuration:.2],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self dragRightRed:red green:green blue:blue tap:tap]; }],
                     [self delayWithDuration:1.8],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l7]; }],  // Swipe up
                     [self delayWithDuration:1.2],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self swipeUpRed:red green:green blue:blue tap:tap]; }],
                     [self delayWithDuration:.5],
                     
                     [CCActionCallBlock actionWithBlock:^{middlePos = green.position;}],
                     [CCActionCallBlock actionWithBlock:^{ [self drawGridWithParent:page middle:middlePos rows:4 pid:currentPageNumber];}],
                     [CCActionCallBlock actionWithBlock:^{[self moveNodes:move yBy:-40 duration:.5];}],
                     [self delayWithDuration:2.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ l8.position = ccp(l8.position.x, l8.position.y - 40); [Sounds curser]; [page addChild:l8]; }],  // You have
                     [self delayWithDuration:.8],
                     
                     [CCActionCallBlock actionWithBlock:^{ l9.position = ccp(l9.position.x, l8.position.y); [Sounds curser]; [page addChild:l9]; }],  // time
                     [self delayWithDuration:1.2],
                     
                     [CCActionCallBlock actionWithBlock:^{ l10.position = ccp(l10.position.x, l10.position.y - 40); [Sounds curser]; [page addChild:l10]; }],  // to do this
                     [self delayWithDuration:2],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [Sounds curser];
                          l9.string = [NSString stringWithFormat:@"%.0f seconds, initially,", INITIAL_SHOTTIME_LIMIT];
                          l11.position = ccp(l11.position.x, l11.position.y - 40);
                          [page addChild:l11];
                          l13.position = ccp(l13.position.x, l13.position.y - 40);
                          [page addChild:l13 z:eBackgroundLayer];
                      }
                      ],  // Gets faster
                     [self delayWithDuration:1.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ l12.position = ccp(l12.position.x, l12.position.y - 40); [Sounds curser]; [page addChild:l12]; }],  // Much faster
                     
                     [CCActionCallBlock actionWithBlock:^{ [self updatePagePointers:currentPageNumber]; }],
                     
                     nil
                     ]];
}

- (void) swipeUpRed:(CCSprite *)red green:(CCSprite *)green blue:(CCSprite *)blue tap:(CCSprite *)tap {
    
    tap.position = ccp(green.position.x, green.position.y + 40);
    tap.visible = YES;
    tap.opacity = 1;
    tap.scale   = 1.5;
    
    CCActionMoveTo *swipe    = [CCActionMoveTo actionWithDuration:.15 position:ccp(tap.position.x, tap.position.y + 200)];
    CCActionCallBlock *hide  = [CCActionFadeOut actionWithDuration:.8];
    CCActionCallBlock *shoot = [CCActionCallBlock actionWithBlock:^{[self shootUpRed:red green:green blue:blue];}];
    [tap runAction:[CCActionSequence actions:swipe, shoot, hide, nil]];
    
}

- (void) shootUpRed:(CCSprite *)red green:(CCSprite *)green blue:(CCSprite *)blue {
    
    float distance = IS_IPHONE5X ? 340 : 280;
    
    [red runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(red.position.x, red.position.y + distance)]];
    [green runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(green.position.x, green.position.y + distance)]];
    [blue runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(blue.position.x, blue.position.y + distance)]];
}

- (void) dragLeftRed:(CCSprite *)red green:(CCSprite *)green blue:(CCSprite *)blue tap:(CCSprite *)tap {
    
    tap.position = ccp(green.position.x, green.position.y + 50);
    tap.visible = YES;
    tap.opacity = 1;
    tap.scale   = 1.5;
    
    float distance = 100;
    
    [tap runAction:[CCActionMoveTo actionWithDuration:.5 position:ccp(tap.position.x - distance, tap.position.y)]];
    [red runAction:[CCActionMoveTo actionWithDuration:.5 position:ccp(red.position.x - distance, red.position.y)]];
    [green runAction:[CCActionMoveTo actionWithDuration:.5 position:ccp(green.position.x - distance, green.position.y)]];
    [blue runAction:[CCActionSequence actions:[CCActionMoveTo actionWithDuration:.5 position:ccp(blue.position.x - distance, blue.position.y)], [CCActionCallBlock actionWithBlock:^{tap.visible = NO;}], nil]];
}

- (void) dragRightRed:(CCSprite *)red green:(CCSprite *)green blue:(CCSprite *)blue tap:(CCSprite *)tap {
    
    tap.position = ccp(green.position.x, green.position.y + 50);
    tap.visible = YES;
    tap.opacity = 1;
    tap.scale   = 1.5;
    
    float distance = 100;
    
    [tap runAction:[CCActionMoveTo actionWithDuration:.5 position:ccp(tap.position.x + distance, tap.position.y)]];
    [red runAction:[CCActionMoveTo actionWithDuration:.5 position:ccp(red.position.x+ distance, red.position.y)]];
    [green runAction:[CCActionMoveTo actionWithDuration:.5 position:ccp(green.position.x + distance, green.position.y)]];
    [blue runAction:[CCActionSequence actions:[CCActionMoveTo actionWithDuration:.5 position:ccp(blue.position.x + distance, blue.position.y)], [CCActionCallBlock actionWithBlock:^{tap.visible = NO;}], nil]];
}

- (void) showBlockRed:(CCSprite *)red green:(CCSprite *)green blue:(CCSprite *)blue {
    [red runAction:[CCActionEaseElasticOut actionWithAction:[CCActionScaleTo actionWithDuration:.6 scale:1]]];
    [green runAction:[CCActionEaseElasticOut actionWithAction:[CCActionScaleTo actionWithDuration:.6 scale:1]]];
    [blue runAction:[CCActionEaseElasticOut actionWithAction:[CCActionScaleTo actionWithDuration:.6 scale:1]]];
}

- (void) rotateBlockRed:(CCSprite *)red green:(CCSprite *)green blue:(CCSprite *)blue tap:(CCSprite *)tap {
    
    CCActionScaleTo   *scale1  = [CCActionScaleTo actionWithDuration:.2 scale:1.5];
    CCActionScaleTo   *scale2  = [CCActionScaleTo actionWithDuration:.2 scale:1.5];
    CCActionFadeOut   *fade    = [CCActionFadeOut actionWithDuration:.2];
    CCActionCallBlock *show    = [CCActionCallBlock actionWithBlock:^{tap.visible = YES; tap.opacity = 0; tap.scale = .1;}];
    CCActionCallBlock *reset   = [CCActionCallBlock actionWithBlock:^{tap.visible = NO;  tap.position = ccp(80,210);}];
    CCActionCallBlock *rotate1 = [CCActionCallBlock actionWithBlock:^{[self rotate1BlockRed:red green:green blue:blue];}];
    CCActionCallBlock *rotate2 = [CCActionCallBlock actionWithBlock:^{[self rotate2BlockRed:red green:green blue:blue];}];
    CCActionDelay     *delay   = [CCActionDelay actionWithDuration:1];
    [tap runAction:[CCActionSequence actions:show, scale1, fade, rotate1, reset, delay, show, scale2, fade, rotate2, nil]];
    
}

- (void) rotate1BlockRed:(CCSprite *)red green:(CCSprite *)green blue:(CCSprite *)blue {
    [red runAction:[CCActionMoveTo actionWithDuration:.1 position:ccp(green.position.x,green.position.y + green.contentSize.height)]];
    [blue runAction:[CCActionMoveTo actionWithDuration:.1 position:ccp(green.position.x,green.position.y - green.contentSize.height)]];
}

- (void) rotate2BlockRed:(CCSprite *)red green:(CCSprite *)green blue:(CCSprite *)blue {
    [red runAction:[CCActionMoveTo actionWithDuration:.1 position:ccp(green.position.x + green.contentSize.width,green.position.y)]];
    [blue runAction:[CCActionMoveTo actionWithDuration:.1 position:ccp(green.position.x - green.contentSize.width,green.position.y)]];
}


// -----------------------------------------------------------------------

#pragma mark - Page 2

- (void) page2 {
    
    currentPageNumber = 2;
    CCNode *page =  [self setPageFlip:currentPageNumber];
    if (!page) {
        return;
    }
    
    float top = IS_IPHONE5X ? 500 : 445;
    
    CCLabelTTF *l1 = [self labelWithString:@"This..." fontSize:20 positon:ccp(LEFT_MAGIN,top) color:[CCColor blueColor]];
    CCLabelTTF *l2 = [self labelWithString:@"is the GRID..." fontSize:20 positon:ccp(l1.position.x + l1.contentSize.width + SPACE_WIDTH,top) color:[CCColor blueColor]];
    
    top = IS_IPHONE5X ? 470 : 420;
    
    CGPoint gridRefLocation = ccp(page.contentSize.width/2, top);
    [page runAction:[CCActionSequence actions:
                     
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l1]; }],
                     [self delayWithDuration:.8],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self drawGridWithParent:page middle:gridRefLocation rows:12 pid:currentPageNumber fade:NO]; }],  // TEST
                     [self delayWithDuration:1.5],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          l1.string = @"This";
                          [Sounds curser];
                          [page addChild:l2];
                          [l2 runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(l1.position.x + l1.contentSize.width + SPACE_WIDTH, l2.position.y)]];
                      }],
                     [self delayWithDuration:2.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self fillGridToParent:page]; }],
                     
                     nil
                     ]];
}

- (void) fillGridToParent:(CCNode *)parent {
    
    float dropInc = [self randomFillSegmentsToParent:parent numberOfRows:6 pid:currentPageNumber];
    
    float top = IS_IPHONE5X ? 240 : 190;
    
    CCLabelTTF *l1 = [self labelWithString:@"Shoot as many blocks as possible into\nthe grid..." fontSize:16 positon:ccp(LEFT_MAGIN,top) color:[CCColor blueColor]];
    CCLabelTTF *l2 = [self labelWithString:@"Go off the bottom and it's..." fontSize:16 positon:ccp(LEFT_MAGIN,top - 50) color:[CCColor blueColor]];
    CCLabelTTF *l3 = [self labelWithString:@"Game Over!" fontSize:22 positon:ccp(LEFT_MAGIN + 135,top - 80) color:[CCColor redColor]];
    
    [parent runAction:[CCActionSequence actions:
                       
                       [CCActionCallBlock actionWithBlock:
                        ^{
                            float top = 0.0;
                            for (int row = 7; row < 12; ++row) {
                                for (int col = 0; col < 9; ++col) {
                                    CCSprite *s = (CCSprite *) [parent getChildByName:[NSString stringWithFormat:@"grid-%d-%d-%d", row, col, currentPageNumber] recursively:NO];
                                    [s runAction:[CCActionFadeTo actionWithDuration:dropInc opacity:.25]];
                                    if (!top) {
                                        top = s.position.y;
                                    }
                                }
                            }
                        }],
                       [self delayWithDuration:2],
                       
                       [CCActionCallBlock actionWithBlock: // Shoot
                        ^{
                            [Sounds curser];
                            l1.anchorPoint = ccp(0,1);
                            l1.opacity = 0;
                            [parent addChild:l1];
                            [l1 runAction:[CCActionFadeIn actionWithDuration:.8]];
                            
                        }],
                       [self delayWithDuration:2.5],
                       
                       [CCActionCallBlock actionWithBlock: // Go off
                        ^{
                            [Sounds curser];
                            l2.anchorPoint = ccp(0,1);
                            l2.opacity = 0;
                            [parent addChild:l2];
                            [l2 runAction:[CCActionFadeIn actionWithDuration:.8]];
                            for (int col = 0; col < 9; ++col) {
                                CCSprite *s = (CCSprite *) [parent getChildByName:[NSString stringWithFormat:@"grid-%d-%d-%d", 11, col, currentPageNumber] recursively:NO];
                                [s runAction:[CCActionSequence actions:[CCActionDelay actionWithDuration:.4], [CCActionFadeIn actionWithDuration:.1], nil]];
                            }
                        }],
                       [self delayWithDuration:1.5],
                       
                       [CCActionCallBlock actionWithBlock: // Game over
                        ^{
                            [Sounds curser];
                            l3.anchorPoint = ccp(0,1);
                            l3.opacity = 0;
                            [parent addChild:l3];
                            [l3 runAction:[CCActionFadeIn actionWithDuration:.8]];
                        }],
                       [self delayWithDuration:1],
                       
                       [CCActionCallBlock actionWithBlock:^{ [self updatePagePointers:currentPageNumber]; }],
                       
                       nil
                       ]];
}

// -----------------------------------------------------------------------

#pragma mark - Page 3

- (void) page3 {
    
    currentPageNumber = 3;
    CCNode *page =  [self setPageFlip:currentPageNumber];
    if (!page) {
        return;
    }
    
    float top = IS_IPHONE5X ? 460 : 410;
    
    CCLabelTTF *l1 = [self labelWithString:@"Create patterns of like-colored\n    segments to remove from the grid." fontSize:16 positon:ccp(LEFT_MAGIN,top)];
    CCLabelTTF *l2 = [self labelWithString:@"Line of 4 or more" fontSize:16 positon:ccp(LEFT_MAGIN + 15,top)];
    CCLabelTTF *l3 = [self labelWithString:@"2x2" fontSize:16 positon:ccp(LEFT_MAGIN + 15,top)];
    CCLabelTTF *l4 = [self labelWithString:@"2x3" fontSize:16 positon:ccp(LEFT_MAGIN + 15,top)];
    CCLabelTTF *l5 = [self labelWithString:@"3x3" fontSize:16 positon:ccp(LEFT_MAGIN + 15,top)];
    CCLabelTTF *l6 = [self labelWithString:@"Combinations" fontSize:16 positon:ccp(LEFT_MAGIN + 15,top)];
    CCLabelTTF *l7 = [self labelWithString:@"Etc..." fontSize:14 positon:ccp(LEFT_MAGIN + 15,top) color:[CCColor redColor]];
    
    [page runAction:[CCActionSequence actions:
                     
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l1]; }],  // Three segments...
                     [self delayWithDuration:1.5],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [Sounds curser];
                          NSArray *array = [self linePatternForParent:page image:@"red-segment.png"];
                          [self showNodes:array duration:1];
                          l2.position = ccp(LEFT_MAGIN, ((CCSprite *) [array objectAtIndex:0]).position.y - 10);
                          [page addChild:l2 z:eGamePieceLayer];
                      }],  // Line pattern
                     [self delayWithDuration:.8],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [Sounds curser];
                          NSArray *array = [self _2x2PatternForParent:page image:@"green-segment.png"];
                          [self showNodes:array duration:1];
                          l3.position = ccp(LEFT_MAGIN, ((CCSprite *) [array objectAtIndex:0]).position.y - 10);
                          [page addChild:l3 z:eGamePieceLayer];
                      }],  // 2x2
                     [self delayWithDuration:.8],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [Sounds curser];
                          NSArray *array = [self _2x3PatternForParent:page image:@"blue-segment.png"];
                          [self showNodes:array duration:1];
                          l4.position = ccp(LEFT_MAGIN, ((CCSprite *) [array objectAtIndex:0]).position.y - 10);
                          [page addChild:l4 z:eGamePieceLayer];
                      }],  // 2x3
                     [self delayWithDuration:.8],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [Sounds curser];
                          NSArray *array = [self _3x3PatternForParent:page image:@"red-segment.png"];
                          [self showNodes:array duration:1];
                          l5.position = ccp(LEFT_MAGIN, ((CCSprite *) [array objectAtIndex:0]).position.y - 10);
                          [page addChild:l5 z:eGamePieceLayer];
                      }],  // 3x3
                     [self delayWithDuration:.8],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [Sounds curser];
                          NSArray *array = [self comboPatternForParent:page pattern:3 image:@"blue-segment.png"];
                          [self showNodes:array duration:1];
                          l6.position = ccp(LEFT_MAGIN, ((CCSprite *) [array objectAtIndex:0]).position.y - 10);
                          [page addChild:l6 z:eGamePieceLayer];
                      }],  // combo 3
                     [self delayWithDuration:1.2],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [self hideNodes:prevComboArray duration:.8];
                          NSArray *array = [self comboPatternForParent:page pattern:2 image:@"green-segment.png"];
                          [self showNodes:array duration:1];
                      }],  // combo 2
                     [self delayWithDuration:1.2],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [self hideNodes:prevComboArray duration:.8];
                          NSArray *array = [self comboPatternForParent:page pattern:1 image:@"red-segment.png"];
                          [self showNodes:array duration:1];
                          l7.position = ccp(l7.position.x, l6.position.y - VERTICAL_SPACING);
                          [page addChild:l7 z:eGamePieceLayer];
                          
                      }],  // combo 1
                     
                     [CCActionCallBlock actionWithBlock:^{ [self updatePagePointers:currentPageNumber]; }],
                     
                     nil
                     ]];
    
}

- (void) showNodes:(NSArray *)nodes duration:(float)duration {
    for (CCNode *node in nodes) {
        [node runAction:[CCActionFadeIn actionWithDuration:duration]];
    }
}

- (void) hideNodes:(NSArray *)nodes duration:(float)duration {
    
    if (!nodes) {
        return;
    }
    
    for (CCNode *node in nodes) {
        [node runAction:[CCActionSequence actions:[CCActionFadeOut actionWithDuration:duration], [CCActionRemove action], nil]];
    }
    
    prevComboArray = nil;
}

- (NSMutableArray *) comboPatternForParent:(CCNode *)parent pattern:(int)pattern image:(NSString *)image {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [array addObjectsFromArray:[self linePatternForParent:parent image:image x:190 y:IS_IPHONE5X ? 144 : 95]];
    
    CCSprite *ref = nil, *s1 = nil;
    switch (pattern) {
        case 1:
            ref = [array objectAtIndex:0];
            break;
        case 2:
            ref = [array objectAtIndex:1];
            break;
        case 3:
            ref = [array objectAtIndex:2];
            break;
        default:
            break;
    }
    
    s1 = [CCSprite spriteWithImageNamed:image];
    s1.anchorPoint = ccp(.5,.5);
    s1.scale = ref.scale;
    s1.position = ccp(ref.position.x, ref.position.y - s1.contentSize.height * ref.scale);
    [parent addChild:s1 z:eGamePieceLayer];
    [array addObject:s1];
    
    s1 = [CCSprite spriteWithImageNamed:image];
    s1.anchorPoint = ccp(.5,.5);
    s1.scale = ref.scale;
    s1.position = ccp(ref.position.x + s1.contentSize.width * s1.scale, ref.position.y - s1.contentSize.height * ref.scale);
    [parent addChild:s1 z:eGamePieceLayer];
    [array addObject:s1];
    
    prevComboArray = array;
    
    return array;
}

- (NSMutableArray *) linePatternForParent:(CCNode *)parent image:(NSString *)image {
    return [self linePatternForParent:parent image:image x:190 y:IS_IPHONE5X ? 410 : 368];
}

- (NSMutableArray *) linePatternForParent:(CCNode *)parent image:(NSString *)image x:(float)x y:(float)y {
    
    CCNode *prev;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (int i = 0; i < 4; ++i) {
        CCSprite *s = [CCSprite spriteWithImageNamed:image];
        s.scale = .75;
        s.anchorPoint = ccp(.5,.5);
        s.position = ccp(!prev ? x : prev.position.x + s.contentSize.width * .75, !prev ? y : prev.position.y);
        [parent addChild:s z:eGamePieceLayer];
        [array addObject:s];
        prev = s;
    }
    
    return array;
}

- (NSMutableArray *) _2x2PatternForParent:(CCNode *)parent image:(NSString *)image {
    return [self _2x2PatternForParent:parent x:190 y:IS_IPHONE5X ? 368 : 325 image:image];
}

- (NSMutableArray *) _2x2PatternForParent:(CCNode *)parent x:(float)x y:(float)y  image:(NSString *)image {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    CCSprite *s1 = [CCSprite spriteWithImageNamed:image];
    s1.scale = .75;
    s1.anchorPoint = ccp(.5,.5);
    s1.position = ccp(x,y);
    [parent addChild:s1 z:eGamePieceLayer];
    [array addObject:s1];
    
    CCSprite *s2 = [CCSprite spriteWithImageNamed:image];
    s2.scale = .75;
    s2.anchorPoint = ccp(.5,.5);
    s2.position = ccp(x + s1.contentSize.width * .75,y);
    [parent addChild:s2 z:eGamePieceLayer];
    [array addObject:s2];
    
    CCSprite *s3 = [CCSprite spriteWithImageNamed:image];
    s3.scale = .75;
    s3.anchorPoint = ccp(.5,.5);
    s3.position = ccp(x,y - s1.contentSize.height * .75);
    [parent addChild:s3 z:eGamePieceLayer];
    [array addObject:s3];
    
    CCSprite *s4 = [CCSprite spriteWithImageNamed:image];
    s4.scale = .75;
    s4.anchorPoint = ccp(.5,.5);
    s4.position = ccp(x + s1.contentSize.width *.74 ,y - s1.contentSize.height * .75);
    [parent addChild:s4 z:eGamePieceLayer];
    [array addObject:s4];
    
    return array;
}

- (NSMutableArray *) _2x3PatternForParent:(CCNode *)parent  image:(NSString *)image {
    return [self _2x3PatternForParent:parent x:190 y:IS_IPHONE5X ? 302 : 255 image:image];
}

- (NSMutableArray *) _2x3PatternForParent:(CCNode *)parent x:(float)x y:(float)y image:(NSString *)image {
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    [array addObjectsFromArray:[self _2x2PatternForParent:parent x:x y:y image:image]];
    
    CCSprite *s5 = [CCSprite spriteWithImageNamed:image];
    s5.scale = .75;
    s5.anchorPoint = ccp(.5,.5);
    s5.position = ccp(x + 2 * (s5.contentSize.width *.74),y);
    [parent addChild:s5 z:eGamePieceLayer];
    [array addObject:s5];
    
    CCSprite *s6 = [CCSprite spriteWithImageNamed:image];
    s6.scale = .75;
    s6.anchorPoint = ccp(.5,.5);
    s6.position = ccp(x + 2 * (s5.contentSize.width *.74),y - s5.contentSize.height * .75);
    [parent addChild:s6 z:eGamePieceLayer];
    [array addObject:s6];
    
    return array;
}

- (NSMutableArray *) _3x3PatternForParent:(CCNode *)parent  image:(NSString *)image {
    
    float x = 190;
    float y = IS_IPHONE5X ? 235 : 188;
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    [array addObjectsFromArray:[self _2x3PatternForParent:parent x:x y:y image:image]];
    
    CCSprite *s7 = [CCSprite spriteWithImageNamed:image];
    s7.scale = .75;
    s7.anchorPoint = ccp(.5,.5);
    s7.position = ccp(x,y - 2 * (s7.contentSize.height * .75));
    [parent addChild:s7 z:eGamePieceLayer];
    [array addObject:s7];
    
    CCSprite *s8 = [CCSprite spriteWithImageNamed:image];
    s8.scale = .75;
    s8.anchorPoint = ccp(.5,.5);
    s8.position = ccp(x + s8.contentSize.width *.74,y - 2 * (s8.contentSize.height * .75));
    [parent addChild:s8 z:eGamePieceLayer];
    [array addObject:s8];
    
    CCSprite *s9 = [CCSprite spriteWithImageNamed:image];
    s9.scale = .75;
    s9.anchorPoint = ccp(.5,.5);
    s9.position = ccp(x + 2 * (s9.contentSize.width *.74),y - 2 * (s9.contentSize.height * .75));
    [parent addChild:s9 z:eGamePieceLayer];
    [array addObject:s9];
    
    return array;
}


// -----------------------------------------------------------------------

#pragma mark - Page 4

- (void) page4 {
    
    currentPageNumber = 4;
    CCNode *page =  [self setPageFlip:currentPageNumber];
    if (!page) {
        return;
    }
    
    float top = IS_IPHONE5X ? 500 : 445;
    
    CCLabelTTF *l1 = [self labelWithString:@"Bombs" fontSize:22 positon:ccp(LEFT_MAGIN,top) color:[CCColor blueColor]];
    
    top = IS_IPHONE5X ? 450 : 420;
    
    float bombPosX = 275.0;
    NSMutableArray *lArray = [[NSMutableArray alloc] init];
    NSMutableArray *bArray = [[NSMutableArray alloc] init];
    [page runAction:[CCActionSequence actions:
                     
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l1]; }],  // Bombs
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self addBombToParent:page type:@"black" pos:ccp(bombPosX, top) row:0 labels:lArray bombs:bArray]; }],  // Black bomb...
                     [self delayWithDuration:.1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self addBombToParent:page type:@"red" pos:ccp(bombPosX, top) row:1 labels:lArray bombs:bArray]; }],  // Red bomb...
                     [self delayWithDuration:.1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self addBombToParent:page type:@"green" pos:ccp(bombPosX, top) row:2 labels:lArray bombs:bArray]; }],  // Green bomb...
                     [self delayWithDuration:.1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self addBombToParent:page type:@"blue" pos:ccp(bombPosX, top) row:3 labels:lArray bombs:bArray]; }],  // Blue bomb...
                     [self delayWithDuration:.1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self addBombToParent:page type:@"time" pos:ccp(bombPosX, top) row:4 labels:lArray bombs:bArray]; }],  // Blue bomb...
                     [self delayWithDuration:.8],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [self showBombDescForParent:page labels:lArray bombs:bArray forIndex:0 moveDown:-30
                                                 desc:
                           @"  Black bombs are used for precision.\n"
                           @"  Drag from the arsenal to the grid\n"
                           @"  to activate..."
                           ];
                      }],  // Black desc
                     [self delayWithDuration:.6],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [self showBombDescForParent:page labels:lArray bombs:bArray forIndex:1 moveDown:-30
                                                 desc:
                           @"  Red bombs destroy 15 red segments\n"
                           @"  from the top. Tap in the arsenal to activate..."
                           ];
                      }],  // Black desc
                     [self delayWithDuration:.6],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [self showBombDescForParent:page labels:lArray bombs:bArray forIndex:2 moveDown:-30
                                                 desc:
                           @"  Green bombs destroy 15 green segments\n"
                           @"  from the top. Tap in the arsenal to activate..."
                           ];
                      }],  // Black desc
                     [self delayWithDuration:.6],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [self showBombDescForParent:page labels:lArray bombs:bArray forIndex:3 moveDown:-30
                                                 desc:
                           @"  Blue bombs destroy 15 blue segments\n"
                           @"  from the top. Tap in the arsenal to activate..."
                           ];
                      }],  // Black desc
                     [self delayWithDuration:.6],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [self showBombDescForParent:page labels:lArray bombs:bArray forIndex:4 moveDown:-30
                                                 desc:
                           @"  Time bombs reset the shot clock to 6 seconds.\n"
                           @"  Tap in the arsenal to activate..."
                           ];
                      }],  // Black desc
                     
                     [CCActionCallBlock actionWithBlock:^{ [self updatePagePointers:currentPageNumber]; }],
                     
                     nil
                     ]];
}

- (void) addBombToParent:(CCNode *)parent type:(NSString *)type pos:(CGPoint)position row:(int)row labels:(NSMutableArray *)labels bombs:(NSMutableArray *)bombs {
    
    [Sounds curser];
    
    CCSprite *bomb = [CCSprite spriteWithImageNamed:[NSString stringWithFormat:@"%@-bomb.png", type]];
    bomb.anchorPoint = ccp(.5,.5);
    bomb.position = ccp(position.x, position.y  - (50 * row));
    bomb.scale = 0;
    
    CCLabelTTF *label = [self labelWithString:type fontSize:16 positon:ccp(LEFT_MAGIN,bomb.position.y - 10)];
    [parent addChild:bomb z:eGamePieceLayer name:[NSString stringWithFormat:@"%@-bomb", type]];
    [parent addChild:label z:eGamePieceLayer];
    
    [labels addObject:label];
    [bombs addObject:bomb];
    
    [bomb runAction:[CCActionEaseBackOut actionWithAction:[CCActionScaleTo actionWithDuration:.3 scale:1.6]]];
}

- (void) showBombDescForParent:(CCNode *)parent labels:(NSMutableArray *)labels bombs:(NSMutableArray *) bombs forIndex:(int)index moveDown:(float)down desc:(NSString *)desc {
    
    [Sounds curser];
    
    CCLabelTTF *label = [labels objectAtIndex:index];
    CCLabelTTF *description = [self labelWithString:desc fontSize:14 positon:ccp(LEFT_MAGIN,label.position.y - 10)];
    description.anchorPoint = ccp(0,1);
    
    NSMutableArray *move = [[NSMutableArray alloc] init];
    
    int count = (unsigned) [bombs count];
    for (int i = index + 1; i < count; ++i) {
        [move addObject:[bombs objectAtIndex:i]];
        [move addObject:[labels objectAtIndex:i]];
    }
    
    [self moveNodes:move yBy:down duration:.5];
    [parent addChild:description z:eBackgroundLayer];
}

// -----------------------------------------------------------------------

#pragma mark - Page 5

- (void) page5 {
    
    currentPageNumber = 5;
    CCNode *page =  [self setPageFlip:currentPageNumber];
    if (!page) {
        return;
    }
    
    float top = IS_IPHONE5X ? 500 : 430;
    
    CCLabelTTF *l1 = [self labelWithString:@"Black & Time bombs appear in random\nsegments on the grid..." fontSize:16 positon:ccp(LEFT_MAGIN,top) color:[CCColor blackColor]];
    CCLabelTTF *l2 = [self labelWithString:@"Remove the segments to acquire the bombs." fontSize:14 positon:ccp(LEFT_MAGIN,top-=210) color:[CCColor blackColor]];
    
    top = IS_IPHONE5X ? 470 : 400;
    
    [page runAction:[CCActionSequence actions:
                     
                     [self delayWithDuration:.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l1]; }],  // Bombs will appear
                     [self delayWithDuration:.4],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self drawGridWithParent:page middle:ccp(page.contentSize.width/2, top) rows:5 pid:currentPageNumber fade:YES]; }],  // Bombs will appear
                     [self delayWithDuration:1.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self addBombsToParent:page]; }],  // Bombs will appear
                     [self delayWithDuration:1.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l2]; }],  // Remove black
                     [self delayWithDuration:1.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self showColorBombDescToParent:page]; }],  // Color bombs
                     
                     nil
                     ]];
}

- (void) showColorBombDescToParent:(CCNode *)parent {
    
    float top = IS_IPHONE5X ? 250 : 185;
    
    CCLabelTTF *l1 = [self labelWithString:@"Color bombs:" fontSize:16 positon:ccp(LEFT_MAGIN,top) color:[CCColor blueColor]];
    CCLabelTTF *l2 = [self labelWithString:@"are acquired when" fontSize:16 positon:ccp(LEFT_MAGIN,top -= (VERTICAL_SPACING + 12)) color:[CCColor blueColor]];
    CCLabelTTF *l3 = [self labelWithString:@"9" fontSize:32 positon:ccp(0,0) color:[CCColor redColor]];
    CCLabelTTF *l4 = [self labelWithString:@"or more" fontSize:16 positon:ccp(0,0) color:[CCColor redColor]];
    CCLabelTTF *l5 = [self labelWithString:@"of like-colored segments are removed..." fontSize:14 positon:ccp(0,0) color:[CCColor blueColor]];
    CCLabelTTF *l6 = [self labelWithString:@"at once..." fontSize:16 positon:ccp(0,0) color:[CCColor redColor]];
    CCLabelTTF *l7 = [self labelWithString:@"Acquired bombs are added to your arsenal..." fontSize:14 positon:ccp(0,0) color:[CCColor blueColor]];
    
    [parent runAction:[CCActionSequence actions:
                       
                       [self delayWithDuration:.4],
                       
                       [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [parent addChild:l1]; }],
                       [self delayWithDuration:.4],
                       
                       [CCActionCallBlock actionWithBlock:
                        ^{
                            
                            float startX = l1.position.x + l1.contentSize.width + 20;
                            for (NSString *name in @[@"red-bomb.png",@"green-bomb.png",@"blue-bomb.png"]) {
                                CCSprite *bomb = [CCSprite spriteWithImageNamed:name];
                                bomb.anchorPoint = ccp(0,0);
                                bomb.position = ccp(startX,l1.position.y - 5);
                                bomb.scale = 0;
                                [parent addChild:bomb];
                                [bomb runAction:[CCActionEaseBackOut actionWithAction:[CCActionScaleTo actionWithDuration:.2 scale:1.5]]];
                                startX += 60;
                            }
                        }],
                       
                       [self delayWithDuration:1],
                       
                       [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [parent addChild:l2]; }], // are acquired
                       [self delayWithDuration:.5],
                       
                       [CCActionCallBlock actionWithBlock: // 9
                        ^{
                            [Sounds curser];
                            l3.anchorPoint = ccp(.5,.5);
                            l3.position = ccp(l2.position.x + l2.contentSize.width + 25, l2.position.y + 8);
                            [parent addChild:l3];
                        }],
                       [self delayWithDuration:1.3],
                       
                       [CCActionCallBlock actionWithBlock: // or more
                        ^{
                            [Sounds curser];
                            l4.position = ccp(l3.position.x + l3.contentSize.width, l2.position.y);
                            [parent addChild:l4];
                        }],
                       [self delayWithDuration:.8],
                       
                       [CCActionCallBlock actionWithBlock: // of the corresponding
                        ^{
                            [Sounds curser];
                            l5.position = ccp(LEFT_MAGIN, l2.position.y - VERTICAL_SPACING);
                            [parent addChild:l5];
                        }],
                       [self delayWithDuration:1.3],
                       
                       [CCActionCallBlock actionWithBlock: // at once
                        ^{
                            [Sounds curser];
                            l6.position = ccp(LEFT_MAGIN, l5.position.y - VERTICAL_SPACING);
                            [parent addChild:l6];
                        }],
                       [self delayWithDuration:1.5],
                       
                       [CCActionCallBlock actionWithBlock: // added to arsenal
                        ^{
                            [Sounds curser];
                            l7.position = ccp(LEFT_MAGIN, l6.position.y - (VERTICAL_SPACING + 10));
                            [parent addChild:l7];
                        }],
                       
                       [CCActionCallBlock actionWithBlock:^{ [self updatePagePointers:currentPageNumber]; }],
                       
                       nil
                       ]];
}

- (void) addBombsToParent:(CCNode *)parent {
    
    float delay = [self randomFillSegmentsToParent:parent numberOfRows:5 pid:currentPageNumber];
    
    [parent runAction:[CCActionSequence actions:[CCActionDelay actionWithDuration:delay],
                       [CCActionCallBlock actionWithBlock:
                        ^{
                            int added = 0;
                            for (int row = 0; row < 5; ++row) {
                                for (int col = 0; col < 9; ++col) {
                                    CCSprite *s = (CCSprite *) [parent getChildByName:[NSString stringWithFormat:@"seg-%d-%d-%d", row, col, currentPageNumber] recursively:NO];
                                    if ((s && arc4random_uniform(10) > 7) || (!added && row > 4 && col > 6)) {
                                        CCSprite *b = added % 2 ? [CCSprite spriteWithImageNamed:@"time-bomb.png"] : [CCSprite spriteWithImageNamed:@"black-bomb.png"];
                                        b.anchorPoint = ccp(.5,.5);
                                        b.position = ccp(s.position.x, s.position.y);
                                        b.opacity = .8;
                                        b.scale = 1.1;
                                        [parent addChild:b z:eGamePieceLayer];
                                        ++added;
                                    }
                                }
                            }
                        }], nil]];
}

// -----------------------------------------------------------------------

#pragma mark - Page 6

- (void) page6 {
    
    currentPageNumber = 6;
    CCNode *page =  [self setPageFlip:currentPageNumber];
    if (!page) {
        return;
    }
    
    float top = IS_IPHONE5X ? 500 : 445;
    
    CCLabelTTF *l1 = [self labelWithString:@"The Arsenal..." fontSize:22 positon:ccp(LEFT_MAGIN,top) color:[CCColor blackColor]];
    CCLabelTTF *l2 = [self labelWithString:@"              contains your bomb collection." fontSize:16 positon:ccp(LEFT_MAGIN,top-=VERTICAL_SPACING) color:[CCColor blackColor]];
    CCLabelTTF *l3 = [self labelWithString:@"Swipe down to reveal your arsenal..." fontSize:16 positon:ccp(LEFT_MAGIN,top-=(VERTICAL_SPACING+5)) color:[CCColor blackColor]];
    CCLabelTTF *l4 = [self labelWithString:@"Remember, tap color/time bombs to activate." fontSize:14 positon:ccp(LEFT_MAGIN,top-=310) color:[CCColor redColor]];
    CCLabelTTF *l5 = [self labelWithString:@"Drag black bombs directly to the grid for\npin-point accuracy..." fontSize:14 positon:ccp(LEFT_MAGIN,top-=(VERTICAL_SPACING+10)) color:[CCColor redColor]];
    
    top = IS_IPHONE5X ? 450 : 420;
    
    [page runAction:[CCActionSequence actions:
                     
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l1]; }],  // The Arenal
                     [self delayWithDuration:.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l2]; }],  // Contains
                     [self delayWithDuration:1.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l3]; }],  // Swipe down
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self showArsenalForParent:page]; }],
                     [self delayWithDuration:2],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l4]; }],  // Remember...
                     [self delayWithDuration:2],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l5]; }],  // Drag...
                     
                     [CCActionCallBlock actionWithBlock:^{ [self updatePagePointers:currentPageNumber]; }],
                     
                     nil
                     ]];
}

- (void) showArsenalForParent:(CCNode *)parent {
    
    [parent runAction:[CCActionSequence actions:
                       
                       [CCActionCallBlock actionWithBlock:
                        ^ {
                            CCSprite *tap  = [CCSprite spriteWithImageNamed:@"tap.png"];
                            tap.position = ccp(parent.contentSize.width / 2, 240);
                            tap.visible = YES;
                            tap.opacity = 1;
                            tap.scale   = 1.5;
                            [parent addChild:tap z:eCommandLayer];
                            
                            CCActionMoveTo *swipe    = [CCActionMoveTo actionWithDuration:.15 position:ccp(tap.position.x, tap.position.y - 140)];
                            CCActionCallBlock *hide  = [CCActionFadeOut actionWithDuration:.8];
                            CCActionCallBlock *shoot = [CCActionCallBlock actionWithBlock:^{ [self revealArsenalForParent:parent]; }];
                            [tap runAction:[CCActionSequence actions:swipe, shoot, hide, nil]];
                        }],
                       
                       nil
                       ]];
    
}

- (void) revealArsenalForParent:(CCNode *)parent {
    
    float top = IS_IPHONE5X ? 180 : 130;
    
    CCSprite *s = [CCSprite spriteWithImageNamed:@"arsenal.png"];
    s.anchorPoint = ccp(.5,0);
    s.position = ccp(parent.contentSize.width/2, -s.contentSize.height);
    [parent addChild:s];
    
    [s runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(s.position.x, top)]];
}

// -----------------------------------------------------------------------

#pragma mark - Page 7

- (void) page7 {
    
    currentPageNumber = 7;
    CCNode *page =  [self setPageFlip:currentPageNumber];
    if (!page) {
        return;
    }
    
    float top = IS_IPHONE5X ? 500 : 445;
    
    CCLabelTTF *l1 = [self labelWithString:@"Special Pieces" fontSize:22 positon:ccp(LEFT_MAGIN,top) color:[CCColor blueColor]];
    CCLabelTTF *l2 = [self labelWithString:@"- bonus stars" fontSize:16 positon:ccp(LEFT_MAGIN,top) color:[CCColor blackColor]];
    CCLabelTTF *l3 = [self labelWithString:@"- black segments" fontSize:16 positon:ccp(LEFT_MAGIN,top) color:[CCColor blackColor]];
    
    NSArray *msg = @[
                     @"Bonus stars appear at random on the grid.",
                     @"Each are worth 250 points.",
                     @"",
                     @"Black segments are like other segments...",
                     @"Except they're rare... they can get in the way.",
                     @"They are also removed when they reach the",
                     @"top of the grid!",
                     ];
    
    top = IS_IPHONE5X ? 450 : 420;
    
    CCSprite *black = [CCSprite spriteWithImageNamed:@"black-segment.png"];
    CCSprite *star  = [CCSprite spriteWithImageNamed:@"star.png"];
    
    [page runAction:[CCActionSequence actions:
                     
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:^{ [Sounds curser]; [page addChild:l1]; }],  // The Arenal
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          star.anchorPoint = ccp(.5,.5);
                          star.position = ccp(50, l1.position.y - 40);
                          star.scale = 0;
                          [page addChild:star];
                          [star runAction:[CCActionScaleTo actionWithDuration:.2 scale:1.6]];
                          
                          black.anchorPoint = ccp(.5,.5);
                          black.position = ccp(50, star.position.y - 40);
                          black.scale = 0;
                          [page addChild:black];
                          [black runAction:[CCActionScaleTo actionWithDuration:.2 scale:1]];
                          
                      }],  // Special
                     [self delayWithDuration:.3],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [Sounds curser];
                          l2.anchorPoint = ccp(0,.5);
                          l2.position = ccp(100, star.position.y);
                          [page addChild:l2];
                          l3.anchorPoint = ccp(0,.5);
                          l3.position = ccp(100, black.position.y);
                          [page addChild:l3];
                      }],
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [Sounds curser];
                          float delay = .2;
                          float mtop = black.position.y - (IS_IPHONE5X ? 60 : 40);
                          for (NSString *m in msg) {
                              CCLabelTTF *l = [CCLabelTTF labelWithString:m fontName:@"Comfortaa-Bold.ttf" fontSize:14];
                              l.color = [CCColor blackColor];
                              l.anchorPoint = ccp(0,0);
                              l.position = ccp(LEFT_MAGIN, mtop-=20);
                              l.opacity = 0;
                              [page addChild:l];
                              [l runAction:[CCActionFadeIn actionWithDuration:delay+=.8]];
                          }
                      }],  // bonus
                     [self delayWithDuration:1.5],
                     
                     [CCActionCallBlock actionWithBlock:^{ [self updatePagePointers:currentPageNumber]; }],
                     
                     nil
                     ]];
}

// -----------------------------------------------------------------------

#pragma mark - Page 8

- (void) page8 {
    
    currentPageNumber = 8;
    CCNode *page =  [self setPageFlip:currentPageNumber];
    if (!page) {
        return;
    }
    
    float top = IS_IPHONE5X ? 480 : 415;
    
    CCButton *playButton = [CCButton buttonWithTitle:@"Start Game" fontName:@"cyberspace.ttf" fontSize:18];
    playButton.position = ccp(page.contentSize.width / 2, top-310);
    playButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
    [playButton setTarget:self selector:@selector(startGame)];
    
    CCButton *exitButton = [CCButton buttonWithTitle:@"Exit Tutorial" fontName:@"cyberspace.ttf" fontSize:18];
    exitButton.position = ccp(page.contentSize.width / 2, top-310);
    exitButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
    [exitButton setTarget:self selector:@selector(close)];
    
    CCButton *closeButton = [CCButton buttonWithTitle:@"Close" fontName:@"cyberspace.ttf" fontSize:14];
    closeButton.anchorPoint = ccp(1,0);
    closeButton.position = ccp(self.contentSize.width -15, -20);
    closeButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
    [closeButton setTarget:self selector:@selector(close)];
    
    CCSprite *gbox = [CCSprite spriteWithImageNamed:@"gbox.png"];
    [page runAction:[CCActionSequence actions:
                     
                     [self delayWithDuration:1],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          CCSprite *rbox = [CCSprite spriteWithImageNamed:@"rbox.png"];
                          CCSprite *bbox = [CCSprite spriteWithImageNamed:@"bbox.png"];
                          
                          gbox.position  = ccp(page.contentSize.width / 2, 0);
                          rbox.position  = ccp(gbox.position.x - 60, 0);
                          bbox.position  = ccp(gbox.position.x + 60, 0);
                          
                          [page addChild:rbox];
                          [page addChild:gbox];
                          [page addChild:bbox];
                          
                          rbox.rotation = -20;
                          gbox.rotation = 10;
                          bbox.rotation = -10;
                          
                          float duration = .8;
                          
                          [gbox runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:duration += .5 position:ccp(self.contentSize.width / 2, self.contentSize.height / 2 + 60)]]];
                          [rbox runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:duration += .5 position:ccp(gbox.position.x - 60, self.contentSize.height / 2 + 56)]]];
                          [bbox runAction:[CCActionEaseElasticOut actionWithAction:[CCActionMoveTo actionWithDuration:duration += .5 position:ccp(gbox.position.x + 60, self.contentSize.height / 2 + 55)]]];
                      }
                      ],
                     [self delayWithDuration:1.5],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          CCLabelTTF *stax = [CCLabelTTF labelWithString:@"Stax" fontName:@"cyberspace.ttf" fontSize:18];
                          stax.color = [CCColor colorWithRed:.75 green:.75 blue:.9];
                          stax.anchorPoint = ccp(.5,.5);
                          stax.scaleX = 2;
                          stax.position = ccp(gbox.position.x, gbox.position.y - 58);
                          [page addChild:stax];
                          
                          HAS_RAN = NO;
                      }],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          if ([[GameState sharedState] savedGame]) {
                              [page addChild:exitButton];
                          } else {
                              [page addChild:playButton];
                          }
                      }],
                     
                     [CCActionCallBlock actionWithBlock:
                      ^{
                          [page addChild:closeButton];
                          [closeButton runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(self.contentSize.width -15, 10)]];
                      }],
                     [CCActionCallBlock actionWithBlock:^{ [self updatePagePointers:currentPageNumber]; }],
                     
                     nil
                     ]];
}

- (void) startGame {
    HAS_RAN = YES;
    CCActionCallBlock *sound = [CCActionCallBlock actionWithBlock:^{ [Sounds stopBackgroundMusic]; [Sounds startGame]; }];
    CCActionCallBlock *start = [CCActionCallBlock actionWithBlock:^{ [self doStartGame]; }];
    [self runAction:[CCActionSequence actions:sound, start, nil]];
}

- (void) doStartGame {
    [[CCDirector sharedDirector] replaceScene:[GameBoardScene scene] withTransition:[CCTransition transitionFadeWithDuration:1.0f]];
}

- (void) close {
    [self closeSequence];
}

- (void) closeSequence {
    CCActionCallBlock *trans = [CCActionCallBlock actionWithBlock:^ {
        if (HAS_RAN) {
            [[CCDirector sharedDirector] replaceScene:[IntroScene scene] withTransition:[CCTransition transitionCrossFadeWithDuration:1.5]];
        } else {
            [[CCDirector sharedDirector] replaceScene:[IntroScene scene] withTransition:[CCTransition transitionCrossFadeWithDuration:.01]];
        }
    }];
    [Sounds panelClose];
    [self runAction:[CCActionSequence actions:trans, nil]];
}

// -----------------------------------------------------------------------

#pragma mark - Actions

- (CCActionDelay *) delayWithDuration:(float)duration {
    return [CCActionDelay actionWithDuration:duration];
}

- (CCLabelTTF *) labelWithString:(NSString *)str fontSize:(float)size positon:(CGPoint)pos {
    return [self labelWithString:str fontSize:size positon:pos color:[CCColor blackColor]];
}

- (CCLabelTTF *) labelWithString:(NSString *)str fontSize:(float)size positon:(CGPoint)pos color:(CCColor *)color {
    CCLabelTTF *label = [CCLabelTTF labelWithString:str fontName:@"Comfortaa-Bold.ttf" fontSize:size];
    label.anchorPoint = ccp(0,0);
    label.position    = pos;
    label.color       = color;
    return label;
}

- (void) showNextPage:(int)pageNum {
    
    if (!nextButton) {
        nextButton = [CCButton buttonWithTitle:@"Next" fontName:@"cyberspace.ttf" fontSize:14];
        [self addChild:nextButton z:eCommandLayer name:@"continue"];
        nextButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
        nextButton.anchorPoint = ccp(1,0);
        nextButton.position = ccp(self.contentSize.width - 15, -20);
    }
    [nextButton runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(self.contentSize.width - 15, 10)]];
    [nextButton setTarget:self selector:@selector(nextPage)];
}

- (void) showPreviousPage:(int)pageNum {
    
    if (pageNum < 0) {
        return;
    }
    
    if (!prevButton) {
        prevButton = [CCButton buttonWithTitle:@"Back" fontName:@"cyberspace.ttf" fontSize:14];
        [self addChild:prevButton z:eCommandLayer name:@"back"];
        prevButton.color = [CCColor colorWithRed:.6 green:.6 blue:.6];
        prevButton.anchorPoint = ccp(0,0);
        prevButton.position = ccp(15,-20);
    }
    [prevButton runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(15, 10)]];
    [prevButton setTarget:self selector:@selector(prevPage)];
}

- (void) hidePagePointers {
    
    if (currentPageNumber >= [pages count]) {
        [nextButton runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(nextButton.position.x, -20)]];
        [prevButton runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(prevButton.position.x, -20)]];
    }
}

- (void) updatePagePointers:(int)cpage {
    
    if (cpage + 1 < NUM_PAGES) {
        [self showNextPage:currentPageNumber];
        canSwipe = YES;
    }
    
    if (cpage > 0) {
        [self showPreviousPage:currentPageNumber];
        canSwipe = YES;
    }
    
    [self hideSpinners];
}

- (void) nextPage {
    
    if (currentPageNumber == NUM_PAGES - 1) {
        return;
    }
    
    ++currentPageNumber;
    
    // [currentPage runAction:[CCActionMoveTo actionWithDuration:SWITCH_SPEED position:ccp(-self.contentSize.width, 0)]];
    [self removeNodeFromScene:currentPage];
    
    if (currentPageNumber < [pages count]) {
        [self addChild:[pages objectAtIndex:currentPageNumber]];
    }
    
    if (currentPageNumber >= NUM_PAGES - 1) {
        [nextButton runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(nextButton.position.x, -20)]];
    }
    [Sounds panelOpen];
    SuppressPerformSelectorLeakWarning([self performSelector:NSSelectorFromString([NSString stringWithFormat:@"page%d", currentPageNumber]) withObject:self]);
    [self setDot];
}

- (void) prevPage {
    
    if (currentPageNumber == 0) {
        return;
    }
    
    --currentPageNumber;
    
    // [currentPage runAction:[CCActionMoveTo actionWithDuration:SWITCH_SPEED position:ccp(self.contentSize.width, 0)]];
    [self removeNodeFromScene:currentPage];
    
    if (currentPageNumber >= 0) {
        [self addChild:[pages objectAtIndex:currentPageNumber]];
    }
    
    if (currentPageNumber <= 0) {
        [prevButton runAction:[CCActionMoveTo actionWithDuration:.2 position:ccp(20, -20)]];
    }
    [Sounds panelOpen];
    SuppressPerformSelectorLeakWarning([self performSelector:NSSelectorFromString([NSString stringWithFormat:@"page%d", currentPageNumber]) withObject:self]);
    [self setDot];
}

- (void) removeNodeFromScene:(CCNode *)node {
    [self removeChild:node cleanup:NO];
}

// -----------------------------------------------------------------------

#pragma mark - Utils

- (void) drawGridWithParent:(CCNode *)parent middle:(CGPoint)mid rows:(int)rows pid:(int)pid {
    [self drawGridWithParent:parent middle:mid rows:rows pid:pid fade:YES];
}

- (void) drawGridWithParent:(CCNode *)parent middle:(CGPoint)mid rows:(int)rows pid:(int)pid fade:(BOOL)fade {
    
    float startX  = parent.contentSize.width / 2 - 4 * (GAMEPIECE_SIZE / 2), x = startX;
    float startY  = mid.y, y = startY;
    float fadeDec = 1 / (float) rows + .01;
    float opacity = 1;
    
    for (int row = 0; row < rows; ++row) {
        for (int col = 0; col < GAMEBOARD_COLUMNS; ++col) {
            CCSprite *s = [CCSprite spriteWithImageNamed:@"grid.png"];
            s.anchorPoint = ccp(.5,.5);
            s.opacity = opacity;
            [parent addChild:s z:eBackgroundLayer name:[NSString stringWithFormat:@"grid-%d-%d-%d", row, col, pid]];
            s.position = ccp(x, y);
            x += (GAMEPIECE_SIZE / 2);
        }
        opacity -= fade ? fadeDec : 0.0;
        x = startX;
        y -= (GAMEPIECE_SIZE / 2);
    }
}

- (float) randomFillSegmentsToParent:(CCNode *)parent numberOfRows:(int)nrows pid:(int)pid {
    
    int type = 0, last = 0;
    float dropInc = .02;
    for (int col = 0; col < 9; ++col) {
        int rows = arc4random_uniform(nrows) + 1;
        if (rows == last) {
            rows += 1;
        }
        last = rows;
        for (int row = 0; row < rows; ++row) {
            CCSprite *s = (CCSprite *) [parent getChildByName:[NSString stringWithFormat:@"grid-%d-%d-%d", row, col, currentPageNumber] recursively:NO], *seg = nil;
            if (s.position.y <= 0) {
                continue; // Hack for some sort of timing bug!
            }
            switch (type++) {
                case 0:
                    seg = [CCSprite spriteWithImageNamed:@"red-segment.png"];
                    seg.anchorPoint = ccp(.5,.5);
                    seg.position = ccp(s.position.x, 30);
                    break;
                case 1:
                    seg = [CCSprite spriteWithImageNamed:@"green-segment.png"];
                    seg.anchorPoint = ccp(.5,.5);
                    seg.position = ccp(s.position.x, 30);
                    break;
                case 2:
                    seg = [CCSprite spriteWithImageNamed:@"blue-segment.png"];
                    seg.anchorPoint = ccp(.5,.5);
                    seg.position = ccp(s.position.x, 30);
                    break;
                default:
                    break;
            }
            [seg runAction:[CCActionMoveTo actionWithDuration:dropInc position:ccp(s.position.x, s.position.y)]];
            dropInc += .03;
            [parent addChild:seg z:eGamePieceLayer name:[NSString stringWithFormat:@"seg-%d-%d-%d", row, col, pid]];
            if (type > 2) {
                type = 0;
            }
        }
    }
    
    return dropInc;
}

- (void) moveNodes:(NSArray *)nodes yBy:(float)y duration:(float)duration {
    
    for (CCNode *node in nodes) {
        [node runAction:[CCActionMoveTo actionWithDuration:duration position:ccp(node.position.x, node.position.y + y)]];
    }
}

// -----------------------------------------------------------------------

#pragma mark - Transition handlers

- (void) onEnterTransitionDidFinish {
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Tutorial.plist"];
    [self addChild:[CCSpriteBatchNode batchNodeWithFile:@"Tutorial.pvr.ccz"]];
    
    swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(close)];
    swipeDown.direction = UISwipeGestureRecognizerDirectionDown;
    [[[CCDirector sharedDirector] view] addGestureRecognizer:swipeDown];
    
    swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft)];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [[[CCDirector sharedDirector] view] addGestureRecognizer:swipeLeft];
    
    swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight)];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [[[CCDirector sharedDirector] view] addGestureRecognizer:swipeRight];
    
    [self renderScene];
}

- (void) onExitTransitionDidStart {
    [[[CCDirector sharedDirector] view] removeGestureRecognizer:swipeDown];
    [[[CCDirector sharedDirector] view] removeGestureRecognizer:swipeLeft];
    [[[CCDirector sharedDirector] view] removeGestureRecognizer:swipeRight];
}

- (void) onExit {
    DLog(@"Tutorial exited!");
}

// -----------------------------------------------------------------------

#pragma mark - Swipe handlers...

- (void) swipeLeft {
    
    if (!canSwipe) {
        [self flashNo];
        return;
    }
    
    if (currentPageNumber == NUM_PAGES - 1) {
        [self close];
    }
    
    [self nextPage];
}

- (void) swipeRight {
    
    if (!canSwipe) {
        [self flashNo];
        return;
    }
    
    [self prevPage];
}

- (void) flashNo {
    
    no.scale = 0;
    no.opacity = 1;
    
    [no runAction:[CCActionSequence actions:
                   [CCActionScaleTo actionWithDuration:.1 scale:1],
                   [CCActionFadeOut actionWithDuration:.8],
                   nil
                   ]];
}

// -----------------------------------------------------------------------

#pragma mark - Destructor

- (void) dealloc {
    
    DLog(@"Dealloc'ed Tutorial");
    
    swipeDown  = nil;
    swipeLeft  = nil;
    swipeRight = nil;
    
    for (CCNode *node in pages) {
        [node removeAllChildren];
    }
    
    [pages removeAllObjects];
    [self removeAllChildren];
    [[CCSpriteFrameCache sharedSpriteFrameCache] removeSpriteFramesFromFile:@"Tutorial.plist"];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
}

@end
