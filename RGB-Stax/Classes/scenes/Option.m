//
//  Option.m
//  RGB-Stax
//
//  Created by Allan De Leon on 7/22/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//

#import "Option.h"

@implementation Option

const float OPTION_TOP = 460;
const float OPTION_DEC = 40;

- (id) initWithName:(NSString *)name label:(NSString *)label booleanValue:(BOOL)value selector:(SEL)selector {

    if (!(self = [super init])) {
        return nil;
    }
    
    self.name         = name;
    self.label        = label;
    self.booleanValue = value;
    self.handler      = selector;
    
    return self;
}

@end
