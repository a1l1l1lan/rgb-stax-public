//
//  SceneUtil.m
//  RGB-Stax
//
//  Created by Allan De Leon on 6/20/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "SceneUtil.h"

@implementation SceneUtil

// -----------------------------------------------------------------------

#pragma mark - Retina detection

+ (BOOL) isRetina {
    return ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] && ([UIScreen mainScreen].scale == 2.0));
}

@end
