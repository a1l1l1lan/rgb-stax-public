//
//  AppDelegate.m
//  RGB-Stax
//
//  Created by Allan De Leon on 5/20/14.
//  Copyright Allan De Leon 2014. All rights reserved.
//
// -----------------------------------------------------------------------

#import "AppDelegate.h"
#import "logic/GameState.h"
#import "scenes/IntroScene.h"

BOOL IS_IPHONE5X = NO;

@implementation AppDelegate

- (BOOL) application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
	[self setupCocos2dWithOptions:@{
                                    
        // Show the FPS and draw call label.
        CCSetupShowDebugStats: @(NO),
                                    
        // More examples of options you might want to fiddle with:
        // (See CCAppDelegate.h for more information)
                                    
        // Use a 16 bit color buffer:
        //	CCSetupPixelFormat: kEAGLColorFormatRGB565,
                                    
        // Use a simplified coordinate system that is shared across devices.
        //	CCSetupScreenMode: CCScreenModeFixed,
                                    
        // Run in portrait mode.
        CCSetupScreenOrientation: CCScreenOrientationPortrait,
                                    
        // Run at a reduced framerate.
        //	CCSetupAnimationInterval: @(1.0/30.0),
                                    
        // Run the fixed timestep extra fast.
        //	CCSetupFixedUpdateInterval: @(1.0/180.0),
                                    
        // Make iPad's act like they run at a 2x content scale. (iPad retina 4x)
        //	CCSetupTabletScale2X: @(YES),
        
    }];
	
	return YES;
}

-(CCScene *) startScene {
    IS_IPHONE5X = [[CCDirector sharedDirector] viewSizeInPixels].height == 1136 ? YES: NO;
	return [IntroScene scene];
}

//- (void) applicationWillTerminate:(UIApplication *)application {
//    [super applicationWillTerminate:application];
//}

//- (void) applicationWillResignActive:(UIApplication *)application {
//    [super applicationWillResignActive:application];
//}

- (void) applicationDidEnterBackground:(UIApplication *)application {
    [super applicationDidEnterBackground:application];
    [[GameState sharedState] saveGameState];
}

@end
