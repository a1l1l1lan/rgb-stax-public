//
//  main.m
//  RGB-Stax
//
//  Created by Allan De Leon on 5/20/14.
//  Copyright Allan De Leon 2014. All rights reserved.
//

#import <UIKit/UIKit.h>

int main(int argc, char *argv[]) {
    
    @autoreleasepool {
        int retVal = UIApplicationMain(argc, argv, nil, @"AppDelegate");
        return retVal;
    }
}
