//
//  Constants.h
//  RGB-Stax
//
//  Created by Allan De Leon on 7/21/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import <Foundation/Foundation.h>

#ifndef CONSTANTS_H
#define CONSTANTS_H
#define GC_LEADERBOARD_ID @"com.softlycoded.RGB_Stax.HighScores"
#endif

@interface Constants : NSObject

@end
