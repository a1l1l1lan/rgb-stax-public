//
//  Constants.m
//  RGB-Stax
//
//  Created by Allan De Leon on 7/21/14.
//  Copyright (c) 2014 Allan De Leon. All rights reserved.
//
// -----------------------------------------------------------------------

#import "Constants.h"

const int GAMEPIECE_SIZE              = 64;   // Size of segment sprites in pixels
const int GAMEBOARD_TOP               = 512;  // Top coordinate of game grid
const int GAMEBOARD_ROWS              = 12;   // Number of rows
const int GAMEBOARD_COLUMNS           = 9;    // Number of columns

const int PREP_TOP                    = 78;   // Y-coordinate of block ready position

const int COLOR_BOMB_THRESHHOLD       = 9;    // Number of segments to eliminate to get color bombs

const float SHOTTIME_INC              = 0.05; // Increment for progress bar
const float INITIAL_SHOTTIME_LIMIT    = 6.0;  // Initial shot time limit in seconds
const float SHOTTIME_DEC              = 0.25; // Amount to decrease shot time by when threshhold is reached
const float SHOTTIME_MIN              = 3.0;  // Minimum shot time in seconds

const unsigned BLACK_THRESHHOLD_INC   = 2500; // Score increments at which black pieces are created
const unsigned BOMB_THRESHHOLD_INC    = 2500; // Score threshhold when bombs are created
const unsigned STAR_THRESHHOLD_INC    =  300; // Score threshhold when bonus stars are created
const unsigned TIMER_THRESHHOLD_INC   = 2500; // Score threshhold when shot time gets decremented (sped up)
const unsigned SLOW_MO_THRESHHOLD_INC = (((INITIAL_SHOTTIME_LIMIT - SHOTTIME_MIN) / SHOTTIME_DEC) - 2) * TIMER_THRESHHOLD_INC;

const int BONUS_POINTS                = 250;  // Bonus points for stars
const int COLOR_BOMB_REMOVES          = 15;   // Number of segments removed when color bombs are used
const int BOMB_WARN_FREQ              = 3;    // Every 3rd shot, give a warning chirp to use bombs

@implementation Constants

@end
